/**
 * Created by osher on 2016/08/12.
 */

import java.io.*;
import java.nio.file.Files;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.io.FileInputStream;

public class FileManager
{
    private Pattern patternWS;
    private Pattern patternNonAscii;
    private Processor processor;

    public FileManager()
    {
        //compile pattern once for efficiency
        patternWS = Pattern.compile("\\s+");
        patternNonAscii = Pattern.compile("[^\\u0000-\\u00FF]+");
        processor = new Processor();

    }

    //read a text file from a directory
    //Parameters:
    //	fileName - name of file without file type
    //  dir - the directory where the file is stored
    public String readTxt(String fileName, String dir)
    {
        String output = "";
        try
        {
            //read text file
            BufferedReader reader = new BufferedReader(new FileReader(dir + fileName + ".txt"));
            output = reader.readLine();
            String line = "";
            //read line by line until no more lines
            while ((line = reader.readLine()) != null)
            {
                output = output + "\n" + line;
            }
            reader.close();
        }
        catch (Exception e)
        {
            System.out.println("Error in readTxt: " + e);
        }
        return output;

    }

    //read text of a docx file
    public String readDocx(String fileName, String dir)
    {
        String output = "";
        try
        {
            FileInputStream fis = new FileInputStream("../" + dir + fileName + ".docx");
            XWPFDocument docx = new XWPFDocument(fis);  //extracts contents of docx
            //list stores paragraphs of docx file
            List<XWPFParagraph> paragraphList = docx.getParagraphs();

            //iterate over paragraph list
            for (XWPFParagraph paragraph : paragraphList)
            {
                output = output + paragraph.getText() + "\n";
            }
            fis.close();

        }
        catch (Exception ee)
        {
            System.out.println("Error in readDocX: " + ee);
        }

        return output;

    }

    //read text of a doc file
    public String readDoc(String fileName, String dir)
    {
        String output = "";
        try
        {
            FileInputStream fis = new FileInputStream("../" + dir + fileName + ".doc");
            HWPFDocument doc = new HWPFDocument(fis);  //extracts contents of doc file
            //WordExtractor extractor = new WordExtractor(doc);   //extracts all text
            output = doc.getDocumentText();
            //output = extractor.getText();
            fis.close();

        }
        catch (Exception ee)
        {
            System.out.println("Error in readDoc: " + ee);
        }

        return output;

    }

    //read text from a pdf file
    public String readPDF(String fileName, String dir, boolean upload)
    {
        String output = "";
        if(upload)
        {
            try
            {
                //python script
                String cmd = "python ../pdfReader.py " + dir + " " + fileName + ".pdf" + " True";
                processor.processCMD(cmd);

                output = readTxt(fileName, "../PDFs/");
                try
                {
                    Files.deleteIfExists(new File("../PDFs/" + fileName + ".txt").toPath());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

            }
            catch (Exception ee)
            {
                System.out.println("Error in readPDF: " + ee);
                output = "error";
            }
        }
        else
        {

            try
            {
                String cmd = "python pdfReader.py " + dir  + " " + fileName + ".pdf" + " False";
                processor.processCMD(cmd);

                output = readTxt(fileName, "PDFs/");
                try
                {
                    Files.deleteIfExists(new File("PDFs/" + fileName + ".txt").toPath());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

            }

            catch (Exception ee)
            {
                System.out.println("Error in readPDF: " + ee);
                output = "error";
            }


        }

        return output;

    }

    //write to a file
    public void writeFile(String fileName, String contents)
    {
        try
        {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(contents);
            writer.close();
        }

        catch (Exception ee)
        {
            System.out.println("Error in writeFile: " + ee);
        }
    }

    /*
    creates a json file. Required to send to salid server
    Parameters:
        fileName - name of file without file type
        contents - String content of of the file
        upload - boolean value stating whether the file is being called by the Upload class
    */
    @SuppressWarnings("unchecked")
    public int createJSON (String fileName, String contents, boolean upload)
    {
        /*
            Json files have to be 4kb or smaller in size as the language identification server (SALID)
            only accepts file sizes of maximum 4kb due to socket limitations.
            The text contents retrieved gets whitespaces cleaned and split into multiple json files
         */
        String removedWS;
        String cleanContents;
        //remove symbols that are unnecessary
        //remove whitespaces - decreases file size.
        Matcher matcher = patternWS.matcher(contents);
        removedWS = matcher.replaceAll(" ");

        matcher = patternNonAscii.matcher(removedWS);
        cleanContents = matcher.replaceAll("");

        
        //split contents into multiple json files if needed
        String parts[];
        int len = cleanContents.length();
        if(len > 3500)
        {
            //number of json files that are needed for the contents
            int numParts = len / 3500;
            if((len % 3500) > 0)
            {
                numParts++;
            }
            //initialise array
            parts = new String[numParts];
            int start =  0;
            int end = 3500;
            for(int i = 0; i < numParts; i++)
            {

                parts[i] = cleanContents.substring(start,end);
                start = end;
                if(i != numParts-2)
                {
                    end += 3500;
                }

                else
                {
                    end = cleanContents.length();
                }

            }

        }

        else
        {
            parts = new String[1];
            parts[0] = cleanContents;
        }

        try
        {
            //loop to create json files for each part of the text
            for (int i = 0; i < parts.length; i++)
            {
                /*
                Creates a json file that will be sent to the language identification server.
                There are 2 fields in the json: (1) text and (2) benchmark.
                text: the text that will be identified
                benchmark: the minimum requirement of confidence for a language to be detected
                 */
                JSONObject file = new JSONObject();
                file.put("text", parts[i]);
                file.put("benchmark", "20");
                FileWriter writer;
                //upload
                if (upload == true)
                {
                    writer = new FileWriter("../json/" + i + fileName  + ".json");
                }

                //web crawler
                else
                {
                    writer = new FileWriter("json/" + i + fileName + ".json");
                }
                writer.write(file.toJSONString());
                writer.close();

            }
        }

        catch (Exception ee)
        {
            System.out.println("Error in createJSON: " + ee);
        }

        return parts.length;

    }

}
