This is the readme for the SALANG IR Honours Project

Project Members:
Andreas von Holy
Osher Shuman
Alon Bresler

Operating System:
Ubuntu

Required packages:
Java 8
	sudo add-apt-repository ppa:webupd8team/java
	sudo apt-get update
	sudo apt-get install oracle-java8-installer
pip
	sudo apt-get install pip

textract
	sudo pip install textract


Note:
Run the language detection server from salang directory: 
	cd LangID\ Index/NCHLT\ South\ African\ Language\ Identifier/linux/
	./salid server

Run Solr server from salang directory: 
	./solr-6.1.0/bin/solr start -cloud -noprompt

Create Solr collection:
	./solr-6.2.0/bin/solr create -c <collection name> -d solr-6.2.0/server/solr/configsets/custom_configs/

To compile Java classes in LangID Index directory from salang directory:
	cd LangID\ Index
	make

To run web crawler:
	cd LangID\ Index
	java -cp .:*:jars/* BasicCrawlController Webpages/ <numberOfCrawlers>


