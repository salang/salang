import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Strip
{
    public static void main (String [] args)
    {

        try
        {
            String output = "";
            //read text file
            BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            output = reader.readLine();
            String line = "";
            //read line by line until no more lines
            while ((line = reader.readLine()) != null)
            {
                output = output + "\n" + line;
            }
            reader.close();

            final long startTime = System.currentTimeMillis();
            Pattern pattern = Pattern.compile("\\s+");
            Matcher matcher = pattern.matcher(output);
            String cleanContents = matcher.replaceAll(" ");
            final long endTime = System.currentTimeMillis();
            System.out.println("Total execution time: " + (endTime - startTime) );

            BufferedWriter writer = new BufferedWriter((new FileWriter("testClean.txt")));
            writer.write(cleanContents);
            writer.close();

            //read text file
            output = "";
            reader = new BufferedReader(new FileReader(args[0]));
            output = reader.readLine();
            line = "";
            //read line by line until no more lines
            while ((line = reader.readLine()) != null)
            {
                output = output + "\n" + line;
            }
            reader.close();

            final long startTime2 = System.currentTimeMillis();
            Matcher matcher2 = pattern.matcher(output);
            String cleanContents2 = matcher2.replaceAll(" ");
            final long endTime2 = System.currentTimeMillis();
            System.out.println("Total execution time 2: " + (endTime2 - startTime2) );

            writer = new BufferedWriter((new FileWriter("testClean2.txt")));
            writer.write(cleanContents2);
            writer.close();

        }
        catch (Exception e)
        {
            System.out.println("Error: " + e);
        }
    }
}
