/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.http.Header;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

import org.apache.tika.parser.ParseContext;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.BodyContentHandler;

/**
 * @author Yasser Ganjisaffar
 */
public class BasicCrawler extends WebCrawler
{

    private static final Pattern FILTER_EXTENSIONS = Pattern.compile(".*\\.(css|js|bmp|gif|jpeg|png|tiff|mid|mp2|mp3|mp4|wav|avi|mov|mpeg|ram|m4v|rm|smil|wmv|swf|wma|zip|rar|gz)$");
    private LangID langID = new LangID();
    private Index index = new Index();
    private FileManager fileManager = new FileManager();

    /**
     * You should implement this function to specify whether the given url
     * should be crawled or not (based on your crawling logic).
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url)
    {

        String href = url.getURL().toLowerCase();
        // Ignore the url if it has an extension that matches our defined set of image extensions.
        if (FILTER_EXTENSIONS.matcher(href).matches())
        {
            return false;
        }

        // Only accept the url if it is in the "www.news24.com" domain and protocol is "http".
        return href.startsWith("http://isizulu.news24.com/");
    }

    /**
     * This function is called when a page is fetched and ready to be processed
     * by your program.
     */
    @Override
    public void visit(Page page)
    {
        int docid = page.getWebURL().getDocid();
        String url = page.getWebURL().getURL();
        String domain = page.getWebURL().getDomain();
        String path = page.getWebURL().getPath();
        String subDomain = page.getWebURL().getSubDomain();
        String parentUrl = page.getWebURL().getParentUrl();
        String anchor = page.getWebURL().getAnchor();

        logger.debug("Docid: {}", docid);
        logger.info("URL: {}", url);
        logger.debug("Domain: '{}'", domain);
        logger.debug("Sub-domain: '{}'", subDomain);
        logger.debug("Path: '{}'", path);
        logger.debug("Parent page: {}", parentUrl);
        logger.debug("Anchor text: {}", anchor);


        if (page.getParseData() instanceof HtmlParseData)
        {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();

            //String text = htmlParseData.getText();
            String html = htmlParseData.getHtml();

            String text = "";
            String metaData = "<meta charset=\"utf-8\" />\n<meta property=\"source_url\" content=\"" + url + "\">";
            String htmlEdit = metaData + "\n" + html;
            fileManager.writeFile("Webpages/wp" + docid + ".html", htmlEdit);
            try
            {
                //detecting the file type
                BodyContentHandler handler = new BodyContentHandler(-1);
                Metadata metadata = new Metadata();
                FileInputStream inputstream = new FileInputStream(new File("Webpages/wp" + docid + ".html"));
                ParseContext parseContext = new ParseContext();

                //Html parser
                HtmlParser htmlParser = new HtmlParser();
                htmlParser.parse(inputstream, handler, metadata,parseContext);
                text = handler.toString();
            }

            catch (Exception e)
            {
                System.out.println("Error with HTML Parser: " + e);
            }


            Set<WebURL> links = htmlParseData.getOutgoingUrls();

            if(text.length() != 0)
            {

                final long startTime = System.currentTimeMillis();  //calculate time spent per web page in milliseconds
                int numParts = fileManager.createJSON("doc" + docid, text, false);  //create a json file(s) for language identification server
                int[] identified = new int[10]; //each number identifies a language. Prevents reindexing of a file for the same language

                for (int i = 0; i < numParts; i++)
                {
                    String lang = langID.identify(i + "doc" + docid, false);

                    //index file in language collection/core
                    switch (lang)
                    {
                        case "English":
                            if (identified[9] == 0)
                            {
                                identified[9] = 1;
                                //index.indexWebpage("wp" + docid + ".html", "Webpages/", "English");
                            }
                            break;
                        case "isiZulu":
                            if (identified[0] == 0)
                            {
                                identified[0] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "isiZulu");
                            }
                            break;
                        case "isiXhosa":
                            if (identified[1] == 0)
                            {
                                identified[1] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "isiXhosa");
                            }
                            break;
                        case "isiNdebele":
                            if (identified[2] == 0)
                            {
                                identified[2] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "isiNdebele");
                            }
                            break;
                        case "Sepedi":
                            if (identified[3] == 0)
                            {
                                identified[3] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "Sesotho_sa_Leboa");
                            }
                            break;
                        case "Sesotho":
                            if (identified[4] == 0)
                            {
                                identified[4] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "Sesotho");
                            }
                            break;
                        case "Setswana":
                            if (identified[5] == 0)
                            {
                                identified[5] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "Setswana");
                            }
                            break;
                        case "Siswati":
                            if (identified[6] == 0)
                            {
                                identified[6] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "siSwati");
                            }
                            break;
                        case "Tshivenda":
                            if (identified[7] == 0)
                            {
                                identified[7] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "Tshivenda");
                            }
                            break;
                        case "Xitsonga":
                            if (identified[8] == 0)
                            {
                                identified[8] = 1;
                                index.indexWebpage("wp" + docid + ".html", "Webpages/", "Xitsonga");
                            }
                            break;
                        default:
                            break;
                    }
                }



                //delete the html file
                try
                {
                    Files.deleteIfExists(new File("Webpages/wp" + docid + ".html").toPath());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }



                final long endTime = System.currentTimeMillis();
                float totalTime = endTime - startTime;
                float timeSecs = totalTime/1000;
                System.out.println("Total execution time: " + timeSecs + " seconds");
            }

            logger.debug("Text length: {}", text.length());
            logger.debug("Html length: {}", html.length());
            logger.debug("Number of outgoing links: {}", links.size());

        }

        Header[] responseHeaders = page.getFetchResponseHeaders();
        if (responseHeaders != null)
        {
            logger.debug("Response headers:");
            for (Header header : responseHeaders)
            {
                logger.debug("\t{}: {}", header.getName(), header.getValue());
            }
        }

        logger.debug("=============");
    }
}
