public class Index
{
    private Processor process;
    public Index()
    {
        process = new Processor();
    }

    public void indexWebpage(String fileName, String dir, String language)
    {
        String cmd = "../solr-6.2.0/bin/post -c " + language + " " + dir + fileName + " -host 137.158.60.198";
        String output = process.processCMD(cmd);
        System.out.println("Index output: " + output);
    }

    public void indexUpload(String fileName, String dir, String language, String id, String title, String description)
	{
        System.out.println(id);
        String Url_ = "/docs?"+id;
	    String cmd = "../../solr-6.2.0/bin/post -c " + language +  " " + dir + fileName + " -host 137.158.60.198" + " -params \"literal.source_url=" + id + "&literal.id=" + Url_ + "&literal.title=" + title + "&literal.description=" + description + "\"";
		String output = process.processCMD(cmd);
		System.out.println("Index output: " + output);
    }

	//arguments required:
    //  filename - name of file to be uploaded
    //  directory - directory of file relative to Index.java
    //  upload - true or false option
    //  language - language collection file belongs to
	public static void main (String [] args)
    {
        Index index = new Index();
        index.indexUpload(args[0], args[1],args[2],args[3],args[4],args[5]); 

    }
}
