/**
 * Created by osher on 2016/08/12.
 */


public class Tester
{
    public static void main (String [] args)
    {
        try
        {
            FileManager fileManager = new FileManager();
            String docx = fileManager.readDocx("testx", "docs/");
            System.out.println(docx);
            System.out.println("=======================================================================================================");
            String pdf = fileManager.readPDF("testpdf", "docs/", false);

            //getting the content of the document
            System.out.println("Contents of the PDF :" + pdf);


        }

        catch (Exception ee)
        {

        }

    }
}
