//upload class
//Description: upload user submitted file from web portal

public class Upload
{
    private FileManager fileManager;
    public Upload()
    {
        fileManager = new FileManager();
    }

    /*
        Read uploaded file and identify language
        Parameter:
            fileName - name of file including file type
            dir - directory to file relative from java class
    */
    public void uploadFile(String fileName, String dir)
    {
        LangID langID = new LangID();   //language identifier
        String output = "";   //text contents of file
        String[] fileNameSplit = fileName.split("\\.");   //split by . for file name and file type

        switch (fileNameSplit[1])
        {
            //Format: text file
            case "txt":
                output = fileManager.readTxt(fileNameSplit[0], dir);
                break;
            //Format: pdf file
            case "pdf":
                output = fileManager.readPDF(fileNameSplit[0], dir, true);
                break;
            //Format: doc (Word) file
            case "doc":
                output = fileManager.readDoc(fileNameSplit[0], dir);
                break;
            //Format: doc (Word) file
            case "docx":
                output = fileManager.readDocx(fileNameSplit[0], dir);
                break;
            default:
                output = "Invalid File Type";
                break;
        }

        //file type uploaded valid and read
        if (!output.equals("Invalid"))
        {
            //create json file to send to language server
            int numParts = fileManager.createJSON(fileNameSplit[0], output, true);
            System.out.println("JSON Created");
            String lang = "";
            Float maxConfidence = new Float(0.0f);
            //identify language of each part of the file
            for (int i = 0; i < numParts; i++)
            {
                String langConfidence = langID.identify(i + fileNameSplit[0], true); //identify language
                String [] results = langConfidence.split(" ");
                Float confidence = Float.parseFloat(results[1]);
                if(confidence > maxConfidence)
                {
                    maxConfidence = confidence;
                    lang = results[0];
                }
            }

            //return Language to website
            switch (lang)
            {
                case "English":
                    fileManager.writeFile("langResult.txt", "English");
                    break;
                case "isiZulu":
                    fileManager.writeFile("langResult.txt", "isiZulu");
                    break;
                case "isiXhosa":
                    fileManager.writeFile("langResult.txt", "isiXhosa");
                    break;
                case "isiNdebele":
                    fileManager.writeFile("langResult.txt", "isiNdebele");
                    break;
                case "Sepedi":
                    fileManager.writeFile("langResult.txt", "Sesotho sa Leboa");
                    break;
                case "Sesotho":
                    fileManager.writeFile("langResult.txt", "Sesotho");
                    break;
                case "Setswana":
                    fileManager.writeFile("langResult.txt", "Setswana");
                    break;
                case "Siswati":
                    fileManager.writeFile("langResult.txt", "siSwati");
                    break;
                case "Tshivenda":
                    fileManager.writeFile("langResult.txt", "Tshivenda");
                    break;
                case "Xitsonga":
                    fileManager.writeFile("langResult.txt", "Xitsonga");
                    break;
                default:
                    fileManager.writeFile("langResult.txt", "Unidentified");
                    break;
            }
        }

        else
        {
            System.out.println("Error in uploadFile: File not valid");
            fileManager.writeFile("langResult.txt", "Incorrect format");
        }


    }

    //Parameters:
    //  fileName - name of file to upload
    //  directory - directory of file
    public static void main(String[] args)
    {
        //calls upload method with arguments
        Upload up = new Upload();
        up.uploadFile(args[0], args[1]);
    }
}

