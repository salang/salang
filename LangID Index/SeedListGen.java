import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;

public class SeedListGen
{
    public SeedListGen(String [] fileNames)
    {
        for (String fileName : fileNames)
        {
            String [] words;
            try
            {
                BufferedReader reader = new BufferedReader(new FileReader(fileName));
                String line = reader.readLine();
                int size = Integer.parseInt(line);
                words = new String[size];
                int count = 0;
                while ((line = reader.readLine()) != null)
                {
                    words[count] = line;
                    count++;
                }

                Random rand = new Random();
                BufferedWriter writer = new BufferedWriter(new FileWriter("WordList.txt", true));
                for (int i = 0; i < 1000; i++)
                {
                    int randomNum = rand.nextInt(words.length);
                    //System.out.println("Count: " + i);
                    //System.out.println(words[randomNum]);
                    writer.write(words[randomNum]);
                    writer.newLine();
                }

                writer.close();

            }
            catch(Exception ee)
            {
                System.out.println("Error with " + fileName + ": " + ee);
            }
        }

    }

    public static void main (String [] args)
    {
        SeedListGen seedListGen = new SeedListGen(args);
    }
}