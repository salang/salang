//langID class
//Identify language of text
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class LangID
{

    Processor proc;
    FileManager fileManager;

    public LangID()
    {

        proc = new Processor();
        fileManager = new FileManager();
    }

    //Identify language of file
	//Parameters:
	//		fileName - file of the document
	public String identify (String fileName, boolean upload)
	{
		try
		{
            if(upload == true)
            {

				System.out.println("fileName: " + fileName);
				//Command: runs perl script which sends json file to language server
    			String cmd = "perl ../BasicLIDWrapper.pl " + "../json/"  + fileName + ".json " + "../results/" + fileName + "Result.txt";
                proc.processCMD(cmd); //process command

    			String outputRaw = fileManager.readTxt(fileName + "Result", "../results/");	//read in result

				//delete the result file
				try
				{
					Files.deleteIfExists(new File("results/" + fileName + "Result.txt").toPath());
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}

    			String splitDir [] = outputRaw.split("\t");	//split directory and result
    			String splitLang [] = splitDir[1].split(",");	//split result into language and confidence

    			int lenL = splitLang[0].length();	//length of language part
                int lenC = splitLang[1].length();	//length of confidence part
				String outputFinal = splitLang[0].substring(14,lenL-1) + " " + splitLang[1].substring(14, lenC);
    			System.out.println("Final output language: " + outputFinal);

				//delete the json file once language is detected
				Files.deleteIfExists(new File("../json/" + fileName + ".json").toPath());
				Files.deleteIfExists(new File("../results/" + fileName + "Result.txt").toPath());
				return outputFinal;
            }

            else
            {
                //Command: runs perl script which sends json file to language server
    			String cmd = "perl BasicLIDWrapper.pl " + "json/"  + fileName + ".json " + "results/" + fileName + "Result.txt";
    			proc.processCMD(cmd); //process command
    			//read result
    			String outputRaw = fileManager.readTxt(fileName + "Result", "results/");

				String splitDir [] = outputRaw.split("\t");	//split directory and result
    			String splitLang [] = splitDir[1].split(",");	//split result into language and confidence
    			int len = splitLang[0].length();
    			String outputFinal = splitLang[0].substring(14,len-1);
                if(outputFinal.equals("rma"))
                {
                    System.out.println("JSON file name: " + fileName + ".json ");
                    //System.exit(0);
                }

    			//System.out.println("Final output language: " + outputFinal);
                try
                {
                    //delete the files once language is detected
                    Files.deleteIfExists(new File("json/" + fileName + ".json").toPath());
                    Files.deleteIfExists(new File("results/" + fileName + "Result.txt").toPath());
                }
                    catch (IOException e)
                {
                    e.printStackTrace();
                }
    			return outputFinal;
            }


		}

		catch(Exception ee)
		{
			System.out.println("Error in identify: " + ee);
			String e = "Language unidentified";
			return e;
		}


	}

}
