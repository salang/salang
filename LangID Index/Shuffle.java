import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Shuffle
{
    public Shuffle()
    {

    }

    public static void main (String [] args)
    {
        ArrayList <String> urls = new ArrayList<>(235589);
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader("SeedList.txt"));
            String line;
            while ((line = reader.readLine()) != null)
            {
                urls.add(line);
            }

            reader.close();
            Collections.shuffle(urls);

            BufferedWriter writer = new BufferedWriter(new FileWriter("SeedList_Shuffle.txt"));
            for(String url : urls)
            {
                writer.write(url);
                writer.newLine();
            }
            writer.close();
        }

        catch(Exception ee)
        {
            System.out.println("Error : " + ee);
        }


    }
}