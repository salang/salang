License

These files are distributed under the Creative Commons Attribution 2.5 South Africa license. 

All files are distributed under the same conditions.
_______________________________________________
License: Creative Commons Attribution 2.5 South Africa
URL: http://creativecommons.org/licenses/by/2.5/za/

Attribute work to: South African Department of Arts and Culture & Centre for Text Technology (CTexT, North-West University, South Africa)

Attribute work to URL: http://www.nwu.ac.za/ctext 
______________________________________________

Requirements:
Microsoft .Net 4.5 (http://www.microsoft.com/en-za/download/details.aspx?id=30653)

Additional resources:
Ude.dll
salid folder with required LID files as specified in the README for salid.exe (./salid/README.txt)
______________________________________________

Input format: Utf8 text file containing running text. 

To Launch the NCHLT-SA-LID, double click on the file. This will bring up the GUI version of the tools.

To run the command line version of LID, please see the README for salid.exe (./salid/README.txt) for details.
