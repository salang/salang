from django.db import models
from authentication.models import Account
from uploads.models import Upload

# the review model for a Review on the portal
class Review(models.Model):
    # person who created the review
    author = models.ForeignKey(Account)
    created_at = models.DateTimeField(auto_now_add=True)
    # upload the review is about
    upload = models.ForeignKey(Upload)
    # if the document is in the correct language
    correctLang = models.BooleanField(default=False)
    # set descriptor as the file
    def __unicode__(self):
        return self.upload.filename
