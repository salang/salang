from rest_framework.serializers import ModelSerializer
from authentication.serializers import AccountSerializer
from uploads.serializers import UploadSerializer
from review.models import Review

# serializer class
class ReviewSerializer(ModelSerializer):
    # need the author and upload of the review
    author = AccountSerializer(read_only=True, required=False)
    upload = UploadSerializer(read_only=True, required=False)

    class Meta:
        model = Review
        fields = ('id', 'author', 'upload', 'created_at', 'correctLang')
        read_only_fields = ('id', 'created_at', 'author')
    # get the author and upload
    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(ReviewSerializer, self).get_validation_exclusions()
        return exclusions + ['author', 'upload']