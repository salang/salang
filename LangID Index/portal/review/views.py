from rest_framework import permissions, viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from uploads.permissions import IsAuthorOfDoc
from review.models import Review
from review.serializers import ReviewSerializer
from uploads.models import Upload
from authentication.views import rewardUser
from authentication.models import Account

####################################
# METHODS FOR THE REVIEW VIEW ######
####################################

# review set for reviews
class ReviewViewSet(viewsets.ModelViewSet):
    # define serializer and query set
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    # get the permission
    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(), )
        return (permissions.IsAuthenticated(), IsAuthorOfDoc(),  )

    def perform_create(self, serializer):
        upload_ = Upload.objects.get(id=self.request.data.get('upload'))
        instance = serializer.save(author=self.request.user, upload=upload_)
        return super(ReviewViewSet, self).perform_create(serializer)

############################################
# METHODS FOR THE ACCOUNT REVIEW VIEW ######
############################################

# get all the reviews for a person
class AccountReviewsViewSet(viewsets.ViewSet):
    queryset = Review.objects.select_related('author').all().order_by('-created_at')
    serializer_class = ReviewSerializer

    def list(self, request, account_username=None):
        queryset = self.queryset.filter(author__username=account_username)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)


####################################
# ACCESSOR METHODS FOR THE REVIEWS #
####################################

# handle indexing and adding the reviewers of a document
@api_view(['GET',])
def postReviewProcess(request):
    # get the data
    reviewId = request.GET.get('id')
    review = Review.objects.get(id=reviewId)
    upload = Upload.objects.get(id=review.upload.id)
    upload.reviewers.no_reviewers += 1
    no_reviewers = upload.reviewers.no_reviewers
    # add in the author as a new reviewer to the document
    if no_reviewers is 1:
        upload.reviewers.reviewer1 = review.author.id
    elif no_reviewers is 2:
        upload.reviewers.reviewer2 = review.author.id
    elif no_reviewers is 3:
        upload.reviewers.reviewer3 = review.author.id
    elif no_reviewers is 4:
        upload.reviewers.reviewer4 = review.author.id
    elif no_reviewers is 5:
        upload.reviewers.reviewer5 = review.author.id
    elif no_reviewers is 6:
        upload.reviewers.reviewer6 = review.author.id

    upload.reviewers.save()
    upload.no_reviews += 1

    # handle a problem being reported
    if not review.correctLang:
        # bad review
        upload.no_bad_reviews += 1
        if upload.no_bad_reviews == 2:
            upload.rejected = True
            upload.approved = False
    else:
        # good review
        upload.no_good_reviews += 1
        if upload.no_good_reviews == 2:
            upload.rejected = False
            upload.approved = True
    upload.save()
    rewardUser(review.author.id, 1)
    return Response({
        'status': 'Review processed',
        'message': 'Success'
    }, status=status.HTTP_202_ACCEPTED)


