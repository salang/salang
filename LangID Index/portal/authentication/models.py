from django.contrib.auth.models import AbstractBaseUser,BaseUserManager
from django.db import models

# manages the account model
class AccountManager(BaseUserManager):
    # normal user
    def create_user(self, email, password=None, **kwargs):
        # raising errors if fields are missing
        if not email:
            raise ValueError('Users must have a valid email address.')

        if not kwargs.get('username'):
            raise ValueError('Users must have a valid username.')

        # follows predefined stuff
        account = self.model(
            email=self.normalize_email(email),
            username=kwargs.get('username'),
            name=kwargs.get('name')
        )
        # create account
        account.set_password(password)
        account.save()

        return account

    # if a super-user needs to be created
    def create_superuser(self, email, password, **kwargs):
        # class normal method
        account = self.create_user(email, password, **kwargs)
        # set special values
        account.is_admin = True
        account.save()
        return account

# the language model to add the languages an account has
class Language(models.Model):
    # language name
    name = models.CharField(max_length=30)
    # language shortening
    short = models.CharField(max_length=20)
    def __unicode__(self):
        return self.name

# the account model for a user on the system
class Account(AbstractBaseUser):
    # unique = true is added because we use the email as a username and thus cant have duplicates
    email = models.EmailField(unique=True)
    # get username, also needs to be unique
    username = models.CharField(max_length=40, unique=True)
    # other fields, blank=true fields allow for optional values here >> do we want this for us?
    name = models.CharField(max_length=80)

    # is admin boolean
    is_admin = models.BooleanField(default=False)

    # timestamps >> auto_now_add means we can't edit created_at after creation, auto_now updates
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # gamification
    points = models.IntegerField(default=0)
    # how many docs they have uploaded
    doc_uploads = models.IntegerField(default=0)
    # how many reviews have been completed
    reviews = models.IntegerField(default=0)
    # how many document views a person has
    views = models.IntegerField(default=0)

    level = models.IntegerField(default=1)  # actual rank
    capMin = models.IntegerField(default=0)  # current rank min
    capMax = models.IntegerField(default=100)  # next rank

    languages = models.ManyToManyField(Language, blank=False)

    #alon and oshers section for the backend
    alonData = models.TextField(default='')
    osherData = models.TextField(default='')

    # the manager for the class
    objects = AccountManager()

    # this sets the email as the username for this entry
    USERNAME_FIELD = 'email'
    # is added because we are overriding fields and needs to be explicitly stated
    REQUIRED_FIELDS = ['username', 'name']

    # change bash representation of object name
    def __unicode__(self):
        return self.email

    # below are both django conversions
    def get_full_name(self):
        names = self.name.split()
        lastEl = len(names)
        return ' '.join([names[0], names[lastEl-1]])

    def get_short_name(self):
        names = self.name.split()
        lastEl = len(names)
        return names[0]







