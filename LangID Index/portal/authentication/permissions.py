from rest_framework import permissions

# if there is a user we return true else we return false
class IsAccountOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, account):
        if request.user:
            return account == request.user
        return False