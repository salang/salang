from django.contrib.auth import update_session_auth_hash
from rest_framework import serializers
from authentication.models import Account, Language

# serializer for the language structure
class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ('id', 'name', 'short')

# serializer for the Account class
class AccountSerializer(serializers.ModelSerializer):
    # we pass required=False as we don't want the password to be updated unless we have a new one,
    # write_only=True allows proper secrecy and encryption in all forms
    password = serializers.CharField(write_only=True, required=False)
    # there for typos
    confirm_password = serializers.CharField(write_only=True, required=False)

    languages = LanguageSerializer(read_only=True, many=True)

    # defines meta data
    class Meta:
        model = Account
        # which parts should be serialized, issuperuser is left out on purpose here
        fields = ('id', 'email', 'username', 'created_at', 'updated_at', 'name', 'password', 'confirm_password',
                  'points', 'doc_uploads', 'reviews', 'languages', 'views', 'level', 'capMax', 'capMin')
        write_only_fields = ('password',)
        # specify them to be read only
        read_only_fields = ('created_at', 'updated_at', 'id', )

        # used for deserialisation
        def create(self, validated_data):
            return Account.objects.create(**validated_data)


        def update(self, instance, validated_data):
            # we update the values if they are present, otherwise we use the old one
            instance.username = validated_data.get('username', instance.username)
            instance.name = validated_data.get('name', instance.name)

            instance.save()

            password = validated_data.get('password', None)
            confirm_password = validated_data.get('confirm_password', None)

            # check if they are the same
            if password and confirm_password and password == confirm_password:
                # securely stores the password
                instance.set_password(password)
                instance.save()
            # we update the users authentication
            update_session_auth_hash(self.context.get('request'), instance)

            return instance


