# REST FRAMEWORK IMPORTS
from rest_framework.renderers import JSONRenderer
from rest_framework import permissions, status
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import list_route, api_view

# DJANGO IMPORTS
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout

# AUTHENTICATION APP IMPORTS
from authentication.models import Account, Language
from authentication.permissions import IsAccountOwner
from authentication.serializers import AccountSerializer, LanguageSerializer

from django.contrib.auth import update_session_auth_hash
from authentication.rankings import getNewLevelCap


# rewards for the gamification
REWARD_VIEW = 5
REWARD_REVIEW = 15
REWARD_UPLOAD = 60




# creates a json response
class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


####################################
######## MANAGING AN ACCOUNT #######
####################################


# change the name of an account
@api_view(['GET', ])
def changeName(request):
    # get the name and change it, then save the entry and return the updated profile
    user = Account.objects.get(id=request.GET.get('id'))
    user.name = request.GET.get('name')
    user.save()
    serializer = AccountSerializer(user)
    return Response(serializer.data)

# checks if a specified account exists
@api_view(['GET', ])
def checkAccount(request):
    # try catch to see if account exists, return result
    try:
        user = Account.objects.get(username=request.GET.get('searchedName'))
    except Account.DoesNotExist:
        return Response({
            'status': 'Not Found',
            'message': 'Account could not be found.'
        }, status=status.HTTP_404_NOT_FOUND)
    else:
        return Response({
            'status': 'User Exists',
            'message': 'Account exists.',
            'id': user.id,
            'views': user.views,
            'doc_uploads': user.doc_uploads,
            'reviews': user.reviews,
            'points': user.points,
            'level': user.level,
            'capMax': user.capMax,
            'capMin': user.capMin
        }, status=status.HTTP_202_ACCEPTED)

# get the language of an account
@api_view(['GET'])
def getLanguages(request):
    # get language and its codes adn return them
    account = Account.objects.get(id=request.GET.get('id'))
    languages = account.languages.all()
    serializer = LanguageSerializer(languages, many=True)
    return Response(serializer.data)



# change the password of an account
@api_view(['POST'])
def changePassword(request):
    # change the password and return the new data
    user = request.data.get('id')
    oldP = request.data.get('passwordOld')
    newP = request.data.get('passwordNew1')
    newP2 = request.data.get('passwordNew2')
    user = Account.objects.get(id=user)
    if user.check_password(oldP):
        if not newP:
            return Response({
                'status': 'Need a valid password',
                'message': 'Need a valid password.'
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        if not newP2:
            return Response({
                'status': 'Please confirm password',
                'message': 'Please confirm password.'
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
        if newP != newP2:
            return Response({
                'status': 'Passwords do not match',
                'message': 'Passwords do not match.'
            }, status=status.HTTP_406_NOT_ACCEPTABLE)
    else:
        return Response({
            'status': 'Old password not valid',
            'message': 'Old password is wrong.'
        }, status=status.HTTP_406_NOT_ACCEPTABLE)

    user.set_password(newP2)
    user.save()
    update_session_auth_hash(request, user)
    return Response({
            'status': 'Password changed',
            'message': 'Password changed'
        }, status=status.HTTP_202_ACCEPTED)

# set the language for an account
@api_view(['POST'])
def setLanguages(request):
    # get the details in the get request
    _amount = request.data.get('no')
    _email = request.data.get('email')
    _langs = request.data.get('languages')
    account = Account.objects.get(email=_email)
    # get languages
    for i in range(0, _amount):
        lang = Language(name=_langs.get(str(i)).get('name'), short=_langs.get(str(i)).get('short'))
        lang.save()
        account.languages.add(lang)
        account.save()
    # return the response
    return Response({
        'status': 'Languages added',
        'message': 'Successfully added ' + str(_amount) + 'languages.'
    }, status=status.HTTP_201_CREATED)

# update the languages of an account
@api_view(['POST'])
def updateLanguages(request):
    _id = request.data.get('id')
    account = Account.objects.get(id=_id)

    # clear old languages
    old_amount = len(account.languages.all())
    for j in range(0, old_amount):
        account.languages.all()[0].delete()

    account.save()

    # add new languages
    _amount = request.data.get('no')
    _langs = request.data.get('languages')

    # find new languages, add them and save the model
    for i in range(0, _amount):
        lang = Language(name=_langs.get(str(i)).get('name'), short=_langs.get(str(i)).get('short'))
        lang.save()
        account.languages.add(lang)
        account.save()

    return Response({
        'status': 'Languages updated',
        'message': 'Successfully updated ' + str(_amount) + 'languages.'
    }, status=status.HTTP_202_ACCEPTED)

# reward a user for completing a gamification task
def rewardUser(id, reward):
    user = Account.objects.get(id=id)
    _points = REWARD_UPLOAD * user.doc_uploads + REWARD_VIEW * user.views + REWARD_REVIEW * user.reviews
    # check if the points are right
    if _points != user.points:
        f = open("hackedAcc.txt", "a")
        f.write("Account: " + str(user.email) + " , " + str(user.points) + " , " + str(_points) + "\n")
        f.close()
        user.points = _points

    # upload
    if reward is 0:
        user.points += REWARD_UPLOAD
        user.doc_uploads += 1
    # reviews
    elif reward is 1:
        user.points += REWARD_REVIEW
        user.reviews += 1
    # view
    elif reward is 2:
        user.points += REWARD_VIEW
        user.views += 1
    user.save()
    checkRank(id)

# check the rank of a user
def checkRank(id):
    user = Account.objects.get(id=id)
    while user.points >= user.capMax:
        # level up
        user.capMin = getNewLevelCap(user.level)
        user.level += 1
        user.capMax = getNewLevelCap(user.level)
        user.save()

####################################
######## DATABASE REQUESTS #########
####################################


# get the newest users on the system
@api_view(['GET'])
def getNewUsers(request):
    number = request.GET.get('no')
    accounts = Account.objects.all().exclude(id=1).order_by('-created_at')[:number]
    serializer = AccountSerializer(accounts, many=True)
    return Response(serializer.data)


####################################
## METHODS FOR THE ACCOUNT VIEW ####
####################################

# ModelViewSet offers an interface for listing, creating, retrieving, updating and destroying objects of a given model.
class AccountViewSet(ModelViewSet):
    # we override the lookupfield from id to username
    lookup_field = 'username'
    # define query set and serializer
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    # getting authentication to perform update and delete
    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)

        if self.request.method == 'POST':
            return (permissions.AllowAny(),)

        return (permissions.IsAuthenticated(), IsAccountOwner(),)

    # handles bad passwords by overriding the password saver and doing it with our create method
    def create(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            Account.objects.create_user(**serializer.validated_data)

            return Response(serializer.validated_data, status=status.HTTP_201_CREATED)

        return Response({
            'status': 'Bad request',
            'message': 'Account could not be created with received data.'
        }, status=status.HTTP_400_BAD_REQUEST)


#####################################
# METHODS FOR THE GAMIFICATION VIEW #
#####################################

class LeaderboardView(ViewSet):
    lookup_field = 'username'
    # define query set and serializer
    queryset = Account.objects.all().exclude(id=1)
    serializer_class = AccountSerializer

    # get all the users sorted by points
    @list_route()
    def get_all_users(self, request):
        queryset = Account.objects.exclude(id=1).order_by('-points')
        serializer = AccountSerializer(queryset, many=True)
        return Response(serializer.data)

    # gets the top users sorted by points
    @list_route()
    def get_top_users(self, request):
        queryset = Account.objects.exclude(id=1).order_by('-points')[:10]
        serializer = AccountSerializer(queryset, many=True)
        return Response(serializer.data)


####################################
# METHODS FOR THE LOGIN VIEW #######
####################################


# views.APIView are made to handle ajax requests
class LoginView(APIView):
    # override the post method
    def post(self, request, format=None):
        #data = json.loads(request.body)

        email = request.data.get('email', None)
        password = request.data.get('password', None)

        # checks for an account with email and returns the account if found, None if it is not found
        account = authenticate(email=email, password=password)

        if account is not None:
            if account.is_active:
                # create a new session for the user
                login(request, account)
                # serialize account details and store it in a json
                serialized = AccountSerializer(account)

                return Response(serialized.data)
            # if account is not active
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Username/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

####################################
# METHODS FOR THE LOGOUT VIEW ######
####################################


class LogoutView(APIView):
    # only auth'd users should be here
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        logout(request)
        # return nothing
        return Response({}, status=status.HTTP_204_NO_CONTENT)





