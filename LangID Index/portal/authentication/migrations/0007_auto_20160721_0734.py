# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-21 07:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0006_auto_20160720_1054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='docfile',
            field=models.FileField(upload_to=b'documents/'),
        ),
    ]
