# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-20 12:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0012_auto_20160915_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='alonData',
            field=models.TextField(default=b''),
        ),
        migrations.AddField(
            model_name='account',
            name='osherData',
            field=models.TextField(default=b''),
        ),
    ]
