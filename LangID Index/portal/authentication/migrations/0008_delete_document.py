# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-27 16:16
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0007_auto_20160721_0734'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Document',
        ),
    ]
