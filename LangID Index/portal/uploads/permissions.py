from rest_framework import permissions

# permission to check who the author is
class IsAuthorOfDoc(permissions.BasePermission):
    def has_object_permission(self, request, view, upload):
        if request.user:
            return upload.author == request.user
        return False