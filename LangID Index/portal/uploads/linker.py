from subprocess import call
from uploads.models import Upload, Reviewers

############################################
# LINKS TO OTHER PARTS OF THE PROJECT ######
############################################

# identify the language of a document
def indentifyLanguage(directory, filename):
    # send through the right paramaters and directories
    foreignDir = "../.:../*:../jars/*"
    langClassname = 'Upload'
    print(call(['java', '-cp', foreignDir, langClassname, filename, directory]))
    resultName = "langResult.txt"
    f = open(resultName, "r")
    result = str(f.readline())
    f.close()
    return result

# index the specified file
def indexFile(directory, filename, language, id_, title, desc):
    foreignDir = "../.:../*:../jars/*"
    langClassname = 'Index'
    print(call(['java', '-cp', foreignDir, langClassname, filename, directory, language, id_, title, desc]))

# read a text file
def readInTextFile(id):
    file = Upload.objects.get(id=id).docfile
    file.open(mode='rb')
    lines = file.readlines()
    file.close()
    fileContent = ""
    for l in lines:
        fileContent += l
        fileContent += "\n"
    return fileContent



