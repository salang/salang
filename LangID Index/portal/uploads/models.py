from __future__ import unicode_literals

from django.db import models
from authentication.models import Account

# Create your models here.

# the reviewers model, stores who reviewed an upload
class Reviewers(models.Model):
    no_reviewers = models.IntegerField(default=0)
    reviewer1 = models.IntegerField(default=-1)
    reviewer2 = models.IntegerField(default=-1)
    reviewer3 = models.IntegerField(default=-1)
    reviewer4 = models.IntegerField(default=-1)
    reviewer5 = models.IntegerField(default=-1)
    reviewer6 = models.IntegerField(default=-1)
    def __unicode__(self):
        return str(self.id)


# upload model for a user upload
class Upload(models.Model):
    # user that uploaded the document
    author = models.ForeignKey(Account)
    # the file
    docfile = models.FileField(upload_to='documents/')
    # the title
    title = models.CharField(max_length=40, blank=True)
    # the filename
    filename = models.CharField(max_length=100)
    # the description
    description = models.TextField(blank=True)
    # the language of the document
    language = models.CharField(max_length=20)
    # created and updated timestamps
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # the number of views on an upload
    noViews = models.IntegerField(default=0)
    # for the reviews
    # how many bad reviews
    no_bad_reviews = models.IntegerField(default=0)
    # how many good reviews
    no_good_reviews = models.IntegerField(default=0)
    # how many reviews it has
    no_reviews = models.IntegerField(default=0)
    # if the doc was accepted because of good reviews
    approved = models.BooleanField(default=False)
    # if the doc was rejected for bad reviews
    rejected = models.BooleanField(default=False)
    # users that have reviewed this doc
    reviewers = models.ForeignKey(Reviewers, blank=True, null=True, on_delete=models.SET_NULL)
    def __unicode__(self):
        return self.filename
