# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-14 15:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uploads', '0007_auto_20160814_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='upload',
            name='approved',
            field=models.BooleanField(default=False),
        ),
    ]
