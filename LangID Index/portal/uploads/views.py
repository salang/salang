from rest_framework import permissions, viewsets, status
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser, JSONParser, MultiPartParser, FormParser
from uploads.models import Upload, Reviewers
from uploads.permissions import IsAuthorOfDoc
from uploads.serializers import UploadSerializer
from authentication.models import Account
from authentication.views import rewardUser
from uploads.linker import indentifyLanguage, readInTextFile, indexFile
from rest_framework.decorators import list_route, api_view
import string
from django.db.models import Q
from review.models import Review
from datetime import datetime
from django.http import JsonResponse
from random import sample

####################################
# METHODS FOR THE UPLOAD VIEW ######
####################################

class UploadViewSet(viewsets.ModelViewSet):
    # create query set
    queryset = Upload.objects.order_by('-created_at')
    serializer_class = UploadSerializer
    parser_classes = (FormParser, MultiPartParser, JSONParser, FileUploadParser,)

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(), )
        return (permissions.IsAuthenticated(), IsAuthorOfDoc(), )


    def perform_create(self, serializer):
        instance = serializer.save(author=self.request.user)
        return super(UploadViewSet, self).perform_create(serializer)


##############################################
# METHODS FOR THE UPLOAD'S ACCOUNT VIEW ######
##############################################

class AccountUploadsViewSet(viewsets.ViewSet):
    # get query set
    queryset = Upload.objects.select_related('author').all().order_by('-created_at')
    serializer_class = UploadSerializer
    # list the accounts
    def list(self, request, account_username=None):
        queryset = self.queryset.filter(author__username=account_username)
        if len(queryset) is 0:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

##############################################
# ACCESS METHODS AND MUTATORS ################
##############################################

# get all docs for a user
@api_view(['GET', ])
def get_user_docs(request):
    # get item
    userID = request.GET.get('id')
    account = Account.objects.get(id=userID)
    queryset = Upload.objects.filter(author=account)
    # catch no documents
    if len(queryset) is 0:
        return Response(status=status.HTTP_404_NOT_FOUND)
    serializer = UploadSerializer(queryset, many=True)
    return Response(serializer.data)


# the process of assigining all the values for a document
@api_view(['GET', ])
def call_post_processes(request):
    docID = request.GET.get('id')
    entry = Upload.objects.get(id=docID)
    # get values that need to be assigned and assign them
    entry.language = request.GET.get('lang')
    entry.title = request.GET.get('title')
    entry.description = request.GET.get('desc')
    r = Reviewers.objects.create()
    r.save()
    # add author to reviews
    entry.reviewers = r
    entry.save()
    # index document
    handleIndexing(docID)
    # reward the user with points
    if int(request.GET.get('reward')) == 1:
        rewardUser(entry.author.id, 0)
    return Response(status=status.HTTP_202_ACCEPTED)

# index document
def handleIndexing(id_):
    # get all values and format for the indexer
    upload = Upload.objects.get(id=id_)
    filename = str(upload.docfile.name)
    filename = string.replace(filename, 'documents/', '')
    dir_ = "media/documents/"
    _desc = str(upload.description).replace(' ', '%20')
    _title = str(upload.title).replace(' ', '%20')
    _language = str(upload.language).replace(' ', '_')
    _id = str(upload.id)
    indexFile(directory=dir_, filename=filename, language=_language, id_=_id, title=_title, desc=_desc)


# indetify the language of an upload
@api_view(['GET', ])
def identify_language(request):
    # get language from lang identifier
    # identify the language
    id = request.GET.get('id')
    filename = Upload.objects.get(id=id).docfile.name
    filename = string.replace(filename, 'documents/', '')
    language = indentifyLanguage("media/documents/", filename)
    if language:
        return Response({
            'status': 'Language Identified',
            'message': language
        }, status=status.HTTP_202_ACCEPTED)
    else:
        return Response({
            'status': 'Language Not Identified',
            'message': 'Error'
        }, status=status.HTTP_404_NOT_FOUND)

# clean an upload if it failed
@api_view(['GET', ])
def clean_fail_upload(request):
    Upload.objects.all().get(id=request.GET.get('id')).delete()
    return Response({
        'status': 'Deleted',
        'message': 'Bad entry was deleted'
    }, status=status.HTTP_202_ACCEPTED)


######################################
######## MAINTENANCE QUERIES #########
######################################

# fetch the last uploaded document
@api_view(['GET', ])
def fetch_last_doc(request):
    # find last document
    doc = Upload.objects.all().filter(title=request.GET.get('title'));
    if len(doc) == 1:
        return Response({'id': doc[0].id})
    else:
        for e in doc:
            if e.description == request.GET.get('desc'):
                return Response({'id': e.id})
    return Response({
        'status': 'Document not found',
        'message': 'No doc found with that title and desc'
    }, status=status.HTTP_404_NOT_FOUND)

# get specific document
@api_view(['GET', ])
def get_document(request):
    doc = Upload.objects.get(id=request.GET.get('id'))
    serializer = UploadSerializer(doc)
    return Response(serializer.data)

# update how many views a document has
@api_view(['GET', ])
def update_no_views(request):
    upload = Upload.objects.get(id=request.GET.get('id'))
    upload.noViews += 1
    upload.save()
    rewardUser(upload.author.id, 2)
    return Response({
            'status': 'Accepted Request',
            'message': 'Successful'
        }, status=status.HTTP_202_ACCEPTED)


# delete a specific document
@api_view(['GET', ])
def delete_doc(request):
    upload = Upload.objects.get(id=request.GET.get('id'))
    upload.delete()
    return Response({
            'status': 'Accepted Request',
            'message': 'Successful'
        }, status=status.HTTP_202_ACCEPTED)


# if a document gets reported
@api_view(['GET', ])
def report_doc(request):
    # write into the document which file got reported
    upload = Upload.objects.get(id=request.GET.get('id'))
    f = open("reportedDocs.txt", "a")
    f.write("Document: " + str(upload.id) + " , " + str(upload.title) + " , " + str(request.GET.get('username')) + " , " + str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')) + '\n')
    f.close()
    return Response({
            'status': 'Accepted Report',
            'message': 'Successful'
        }, status=status.HTTP_202_ACCEPTED)

# the documents to review
@api_view(['GET', ])
def get_review_doc(request):
    # user we are looking for docs for
    account = Account.objects.get(id=request.GET.get('id'))
    # languages filtering
    langs = account.languages.all()

    # create query sets to exclude certain documents to review
    qs1 = Q()
    qs2 = Q()

    # filter by the languages the user can speak
    for i in langs:
        qs1 |= Q(language=i)

    # filter out the past reviewers
    qs2 |= Q(reviewers__reviewer1=account.id)
    qs2 |= Q(reviewers__reviewer2=account.id)
    qs2 |= Q(reviewers__reviewer3=account.id)
    qs2 |= Q(reviewers__reviewer4=account.id)
    qs2 |= Q(reviewers__reviewer5=account.id)
    qs2 |= Q(reviewers__reviewer6=account.id)
    qs2 |= Q(author=account)

    # we filter by any of the saved languages excluding the users own ones ordered by date
    finalList = Upload.objects.exclude(qs2).filter(qs1).order_by('created_at')

    serializer = UploadSerializer(finalList, many=True)
    return Response(serializer.data)


####################################
# PORTAL STATISTICS ################
####################################

# get the statsitics of the portal
@api_view(['GET', ])
def get_stats(request):
    number_of_stats = 3
    data = {
        'no_users': len(Account.objects.all()),
        'no_reviews': len(Review.objects.all()),
        'no_uploads': len(Upload.objects.all()),
        'length': number_of_stats
    }
    return JsonResponse(data)

# get certain amount of random stats
@api_view(['GET', ])
def get_random_stats(request):
    # change this if anything is added
    number_of_stats = 3

    data = {
        0: {'name': 'no_uploads', 'amount': len(Upload.objects.all())},
        1: {'name': 'no_users', 'amount': len(Account.objects.all())},
        2: {'name': 'no_reviews', 'amount': len(Review.objects.all())},
        'length': number_of_stats
    }
    request_no = request.GET.get('no')
    choices = []
    # add all values in
    for i in range(0, number_of_stats):
        choices.append(i)

    indexes = sample(choices, int(request_no))
    data2 = {}
    count = 0
    for j in indexes:
        data2[data[j]['name']] = data[j]['amount']
    request_no2 = request.GET.get('no_random')
    samples = request.GET.get('samples')
    choices2 = []
    for j in range(0, int(samples)):
        choices2.append(j)
    rands = sample(choices2, int(request_no2))
    # filter random data
    data2['rands'] = rands
    return JsonResponse(data2)





