from rest_framework.serializers import ModelSerializer
from authentication.serializers import AccountSerializer
from uploads.models import Upload, Reviewers

# serializes the upload
class UploadSerializer(ModelSerializer):
    author = AccountSerializer(read_only=True, required=False)
    # meta tags for the fields to be serialized
    class Meta:
        model = Upload
        fields = ('id', 'author', 'docfile', 'title', 'filename', 'description', 'language', 'created_at', 'noViews',
                  'no_bad_reviews', 'no_good_reviews', 'no_reviews', 'approved', 'rejected', 'reviewers')
        read_only_fields = ('id', 'created_at', 'author')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(UploadSerializer, self).get_validation_exclusions()
        return exclusions + ['author']

# serializes the review
class ReviewersSerializer(ModelSerializer):
    class Meta:
        model = Reviewers
        fields = ('id', 'no_reviewers', 'reviewer1', 'reviewer2', 'reviewer3', 'reviewer4', 'reviewer5', 'reviewer6')
        read_only_fields = 'id'
