(function () {
    'use strict';

    angular.module('salang.utils.configs').config(config);

    /**
     * @name config
     * @desc config for package
     */
    function config(NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 5000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'center',
            positionY: 'top',
            closeOnClick: true
        });
    }
})();



