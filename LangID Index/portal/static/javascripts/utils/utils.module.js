(function () {
    'use strict';

    angular
        .module('salang.utils', [
            'salang.utils.services',
            'salang.utils.controllers',
            'salang.utils.configs'
        ]);

    angular.module('salang.utils.services', []);
    angular.module('salang.utils.controllers', []);
    angular.module('salang.utils.configs', ['ui-notification']);
})()