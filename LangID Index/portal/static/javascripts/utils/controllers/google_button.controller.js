/**
 * GoogleButtonController
 * @namespace salang.utils.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.utils.controllers')
        .controller('GoogleButtonController', GoogleButtonController);

    GoogleButtonController.$inject = ['$scope', 'Authentication', '$rootScope'];

    /**
     * @namespace GoogleButtonController
     */
    function GoogleButtonController($scope, Authentication, $rootScope) {


        window.onSignIn = onSignIn;
        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();
            console.log(profile);


            Authentication.checkProfile(profile.getName()).then(profileSuccessFn, profileErrorFn);

            function profileSuccessFn(data, status, headers, config){
                //means the user has already registered
                Authentication.login(profile.getEmail(), 'default');
            }

            function profileErrorFn(data, status, headers, config){
                //user does not exist
                var username = profile.getGivenName();
                var email = profile.getEmail();
                var name = profile.getGivenName() + " " + profile.getFamilyName();
                var password = 'default';
                Authentication.register(email, password, username, name);
            }
        }
    }
})();