/**
 * FacebookButtonController
 * Controller for the facebook log in button
 * @namespace salang.utils.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.utils.controllers')
        .controller('FacebookButtonController', FacebookButtonController);

    FacebookButtonController.$inject = ['$scope', 'Facebook', 'Authentication'];

    /**
     * @namespace FacebookButtonController
     */
    function FacebookButtonController($scope, Facebook, Authentication) {


        //check if user is logged in
        window.checkLogin = function(){
            getLoginStatus();
        };

        /**
         * logging in
         * @private
         */
        function login_(){
            Facebook.login(function(response){
                //console.log("logged in");
                //console.log(response);
                $scope.getInformation();
            });
        }

        /**
         * get the status
         */
        function getLoginStatus() {
            Facebook.getLoginStatus(function(response) {
                if(response.status === 'connected') {
                    getInformation();
                } else {
                    login_();
                }
            });
        }

        /**
         * get the information from the facebook account
         */
        function getInformation() {
        //console.log("getInformation");
        Facebook.api('/me?fields=email,first_name,name', function(response) {
            // console.log(response);
            var profile = response;
            console.log(profile);
            Authentication.checkProfile(profile.first_name).then(profileSuccessFn, profileErrorFn);

            /**
             * success after logging in a profile from facebook account
             * @param data
             * @param status
             * @param headers
             * @param config
             */
            function profileSuccessFn(data, status, headers, config){
                //console.log("getInformation: success");
                //means the user has already registered
                Authentication.login(profile.email, 'default');
            }

            /**
             * after creating a profile from facebook account
             * @param data
             * @param status
             * @param headers
             * @param config
             */
            function profileErrorFn(data, status, headers, config){
               // console.log("getInformation: fail");
                //user does not exist
                var username = profile.first_name;
                var email = profile.email;
                var name = profile.name;
                var password = 'default';
                Authentication.register(email, password, username, name);
            }
        });
        }

    }
})();