/**
 * CodeGenerator
 * @namespace salang.utils.services
 */
(function ($, _) {
    'use strict';

    angular
        .module('salang.utils.services')
        .factory('CodeGenerator', CodeGenerator);

    /**
     * @namespace CodeGenerator
     */
    function CodeGenerator() {
        /**
         * @name CodeGenerator
         * @desc The factory to be returned
         */
        var CodeGenerator = {

        };

        return CodeGenerator;




    }
})($, _);
