/**
 * Shared
 * @namespace salang.utils.services
 */
(function ($, _) {
    'use strict';

    angular
        .module('salang.utils.services')
        .factory('Shared', Shared);

    /**
     * @namespace Shared
     */
    function Shared() {
        /**
         * @name Shared
         * @desc The factory to be returned
         */
        var Shared = {

        };

        return Shared;

    }
})($, _);
