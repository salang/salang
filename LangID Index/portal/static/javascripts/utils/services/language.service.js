/**
 * LanguageConverter
 * Service for the language calls
 * @namespace salang.utils.services
 */
(function ($, _) {
    'use strict';

    angular
        .module('salang.utils.services')
        .factory('LanguageConverter', LanguageConverter);

    /**
     * @namespace LanguageConverter
     */
    function LanguageConverter() {
        /**
         * @name LanguageConverter
         * @desc The factory to be returned
         */
        var LanguageConverter = {
            fromShortToLong : fromShortToLong,
            fromLongToShort : fromLongToShort
        };

        return LanguageConverter;

        /**
         * convert from short to long version of language
         * @param short - short version
         * @returns {long version}
         */
        function fromShortToLong(short){
            switch (short) {
                case 'nb':
                    return "isiNdebele";
                case 'sotho':
                    return "Sesotho sa Leboa";
                case 'st':
                    return "Sesotho";
                case 'ss':
                    return "siSwati";
                case 'ts':
                    return "Xitsonga";
                case 'tn':
                    return "Setswana";
                case 've':
                    return "Tshivenda";
                case 'xh':
                    return "isiXhosa";
                case 'zu':
                    return "isiZulu";
                default:
                    return "English";

            }
        }

        /**
         * convert from long to short version of language
         * @param long - long version
         * @returns {short version}
         */
        function fromLongToShort(long){
            switch (long) {
                case 'isiNdebele':
                    return "nd";
                case 'Sesotho sa Leboa':
                    return "sotho";
                case 'Sesotho':
                    return "st";
                case 'siSwati':
                    return "ss";
                case 'Xitsonga':
                    return "ts";
                case 'Setswana':
                    return "tn";
                case 'Tshivenda':
                    return "ve";
                case 'isiXhosa':
                    return "xh";
                case 'isiZulu':
                    return "zu";
                default:
                    return "English";

            }
        }

    }
})($, _);
