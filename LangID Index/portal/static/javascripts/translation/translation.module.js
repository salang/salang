(function () {
    'use strict';

    angular
        .module('salang.translation', [
            'salang.translation.controllers'
        ]);

    angular.module('salang.translation.controllers', []);
})();
