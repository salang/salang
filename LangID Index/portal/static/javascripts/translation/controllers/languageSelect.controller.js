/**
 * LanguageSelectController
 * Controller for selecting languages
 * @namespace salang.translation.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.translation.controllers')
        .controller('LanguageSelectController', LanguageSelectController);

    LanguageSelectController.$inject = ['$scope', '$rootScope', '$translate', 'SearchBackend' ];

    /**
     * @namespace LanguageSelectController
     */
    function LanguageSelectController($scope, $rootScope, $translate, $SearchBackend) {


        $scope.selectedLang = localStorage.getItem('NG_TRANSLATE_LANG_KEY');

        //function to change the language
        $scope.change = function(language) {
            changeLanguage(language);
        };

        /**
         * @name changeLanguage
         * @desc change the langauge
         * @memberOf salang.translation.controllers.LanguageSelectController
         */
        function changeLanguage(lang) {
            $translate.use(lang);
            $rootScope.$on('$translateChangeSuccess', function(event, data) {
                $rootScope.lang = data.language;
                $SearchBackend.defaultSearchLanguage();
            })
        }
    }
})();