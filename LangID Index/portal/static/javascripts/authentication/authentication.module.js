(function () {
  'use strict';

  angular.module('salang.authentication', [
      'salang.authentication.controllers',
      'salang.authentication.services'
    ]);

  angular.module('salang.authentication.controllers', []);
  angular.module('salang.authentication.services', ['ngCookies']);
})();
