/**
 * Authentication
 * Service to handle commands for authentication and accounts
 * @namespace salang.authentication.services
 */
(function () {
    'use strict';

    angular
        .module('salang.authentication.services')
        .factory('Authentication', Authentication);

    Authentication.$inject = ['$cookies', '$http', 'Notification', 'Facebook'];

    /**
     * @namespace Authentication
     * @returns {Factory}
     */
    function Authentication($cookies, $http, Notification, Facebook) {
        /**
         * @name Authentication
         * @desc The Factory to be returned
         */
        var Authentication = {
            login: login,
            logout: logout,
            register: register,
            getAuthenticatedAccount: getAuthenticatedAccount,
            isAuthenticated: isAuthenticated,
            setAuthenticatedAccount: setAuthenticatedAccount,
            unauthenticate: unauthenticate,
            checkProfile: checkProfile,
            getLangs: getLangs,
            setLangs: setLangs,
            updateLangs: updateLangs,
            getReloaded: getReloaded,
            setReloaded: setReloaded,
            removeReloaded: removeReloaded,
            getNewUsers: getNewUsers,
            changePassword: changePassword
        };

        return Authentication;

        ////////////////////

        /**
         * remove the storage item
         */
        function removeReloaded() {
            sessionStorage.removeItem("reloaded_");
        }

        /**
         * get the storage item
         * @returns {storage key}
         */
        function getReloaded(){
            return sessionStorage.reloaded_;
        }

        /**
         * set the reloaded value in storage
         * @param val - to be set
         * @returns {set value}
         */
        function setReloaded(val){
            sessionStorage.reloaded_ = val;
            return sessionStorage.reloaded_;
        }



        /**
         * @name register
         * @desc Try to register a new user
         * @param {string} email The email entered by the user
         * @param {string} password The password entered by the user
         * @param {string} username The username entered by the user
         * @param {string} name The name entered by the user
         * @returns {Promise}
         * @memberOf salang.authentication.services.Authentication
         */
        function register(email, password, username, name) {
            return $http.post('/api/v1/accounts/', {
                username: username,
                password: password,
                email: email,
                name: name
            }).then(registerSuccessFn, registerErrorFn);

            /**
             * @name registerSuccessFn
             * @desc Log the new user in
             */
            function registerSuccessFn(data, status, headers, config) {
                //do nothing here, need to capture languages
            }

            /**
             * @name registerErrorFn
             * @desc Log "Epic failure!" to the console
             */
            function registerErrorFn(data, status, headers, config) {
                console.error('Failed to register user');
                Notification.error('Failed to register user.');
            }
        }

        //////////////////////////

        /**
         * @name login
         * @desc Try to log in with email `email` and password `password`
         * @param {string} email The email entered by the user
         * @param {string} password The password entered by the user
         * @returns {Promise}
         * @memberOf salang.authentication.services.Authentication
         */
        function login(email, password) {

            return $http.post('/api/v1/auth/login/', {
                email: email,
                password: password
            }).then(loginSuccessFn, loginErrorFn);

            //////////////////////////

            /**
             * @name loginSuccessFn
             * @desc Set the authenticated account and redirect to index
             */
            function loginSuccessFn(data, status, headers, config) {
                Authentication.setAuthenticatedAccount(data.data);
                window.location = '/';
            }

            //////////////////////////

            /**
             * @name loginErrorFn
             * @desc Log "Epic failure!" to the console
             */
            function loginErrorFn(data, status, headers, config) {
                console.error('Failed to log in user');
                Notification.error('Failed to log in user.');
            }
        }

        //////////////////////////

        /**
         * @name getAuthenticatedAccount
         * @desc Return the currently authenticated account
         * @returns {object|undefined} Account if authenticated, else `undefined`
         * @memberOf salang.authentication.services.Authentication
         */
        function getAuthenticatedAccount() {
            if (!$cookies.get("authUser")) {
                console.error("Error: No authenticated user!");
                Notification.error('Failed to authenticate user.');
                logout();
                return;
            }

            return JSON.parse($cookies.get("authUser"));
        }

        //////////////////////////

        /**
         * @name isAuthenticated
         * @desc Check if the current user is authenticated
         * @returns {boolean} True is user is authenticated, else false.
         * @memberOf salang.authentication.services.Authentication
         */
        function isAuthenticated() {
            return !!$cookies.get("authUser");
        }

        //////////////////////////

        /**
         * @name setAuthenticatedAccount
         * @desc Stringify the account object and store it in a cookie
         * @param {Object} account The account object to be stored
         * @returns {undefined}
         * @memberOf salang.authentication.services.Authentication
         */
        function setAuthenticatedAccount(account) {
            $cookies.put("authUser", JSON.stringify(account));
        }

        //////////////////////////

        /**
         * @name unauthenticate
         * @desc Delete the cookie where the user object is stored
         * @returns {undefined}
         * @memberOf salang.authentication.services.Authentication
         */
        function unauthenticate() {
            delete $cookies.remove("authUser");
        }

        //////////////////////////

        /**
         * sings someone out of facebook with logout
         */
        function facebookSingOut(){
            Facebook.logout();
            console.log('User signed out of facebook.');
        }

        /**
         * @name logout
         * @desc Try to log the user out
         * @returns {Promise}
         * @memberOf salang.authentication.services.Authentication
         */
        function logout() {
            //facebook
            facebookSingOut();

            /**
             * make the call to logout
             */
            return $http.post('/api/v1/auth/logout/').then(logoutSuccessFn, logoutErrorFn);

            /**
             * @name logoutSuccessFn
             * @desc Unauthenticate and redirect to index with page reload
             */
            function logoutSuccessFn(data, status, headers, config) {
                Authentication.unauthenticate();
                window.location = '/';
            }

            /**
             * @name logoutErrorFn
             * @desc Log "Epic failure!" to the console
             */
            function logoutErrorFn(data, status, headers, config) {
                console.error('Failed to log out user');
                Notification.error('Failed to log out user.');
            }
        }


        /////////////////////

        /**
         * @name checkProfile
         * @desc check if given profile exists
         * @param {Object} username The profile to be updated
         * @returns {Promise}
         * @memberOf salang.upload.services.Authentication
         */
        function checkProfile(username) {
            return $http.get('/api/v1/accounts/checkAccount?searchedName='+username);
        }


        /////////////////////

        /**
         * @name getLangs
         * @desc get languages for a user
         * @param {String} id the id of the user
         * @returns {Promise}
         * @memberOf salang.upload.services.Authentication
         */
        function getLangs(id) {
            return $http.get('/api/v1/accounts/getLanguages?id='+id);
        }


        /////////////////////

        /**
         * @name setLangs
         * @desc set languages for a user
         * @param {Object} email email of the user
         * @param {Object} amount amount of languages
         * @param {Object} langs object of languages
         * @returns {Promise}
         * @memberOf salang.upload.services.Authentication
         */
        function setLangs(email, amount, langs) {

            return $http({
                method: 'POST',
                url: '/api/v1/accounts/setLanguages/',
                data: {
                    no: amount,
                    email: email,
                    languages: langs
                }
            });
        }

        /////////////////////

        /**
         * @name updateLangs
         * @desc set languages for a user
         * @param {Object} id id of the user
         * @param {Object} amount amount of languages
         * @param {Object} langs object of languages
         * @returns {Promise}
         * @memberOf salang.upload.services.Authentication
         */
        function updateLangs(id, amount, langs) {

            return $http({
                method: 'POST',
                url: '/api/v1/accounts/updateLanguages/',
                data: {
                    no: amount,
                    id: id,
                    languages: langs
                }
            });
        }


        /////////////////////

        /**
         * @name getNewUsers
         * @desc get latest users
         * @param {Object} no number of users
         * @returns {Promise}
         * @memberOf salang.upload.services.Authentication
         */
        function getNewUsers(no) {
            return $http.get('/api/v1/accounts/getNewUsers?no='+no);
        }


        /**
         * @name updateLangs
         * @desc set languages for a user
         * @param {Object} id id of the user
         * @param {Object} old amount of languages
         * @param {Object} new1 object of languages
         * @param {Object} new2 object of languages
         * @returns {Promise}
         * @memberOf salang.upload.services.Authentication
         */
        function changePassword(id, old, new1, new2) {

            return $http({
                method: 'POST',
                url: '/api/v1/accounts/changePassword/',
                data: {
                    passwordOld: old,
                    id: id,
                    passwordNew1: new1,
                    passwordNew2: new2
                }
            });
        }



    }
})();
