/**
 * LoginController
 * controller to handle all authentication related controlling
 * @namespace salang.authentication.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.authentication.controllers')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', '$scope', 'Authentication', 'Notification'];

    /**
     * @namespace LoginController
     */
    function LoginController($location, $scope, Authentication, Notification) {


        var vm = this;
        vm.login = login;
        vm.register = register;
        vm.captureLanguages = captureLanguages;

        vm.showLangs = true;
        activate();


        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf salang.authentication.controllers.LoginController
         */
        function activate() {
            //reload page for the facebook button
            if (!Authentication.getReloaded()){
                Authentication.setReloaded(true);
                location.reload(false);
            }
            else{
                Authentication.removeReloaded();
            }

            // If the user is authenticated, they should not be here and we redirect to the index page
            if (Authentication.isAuthenticated()) {
                $location.url('/');
            }
        }

        /**
         * @name login
         * @desc Log the user in
         * @memberOf salang.authentication.controllers.LoginController
         */
        function login() {
            Authentication.login(vm.email, vm.password);
        }


        /**
         * @name register
         * @desc Register a new user
         * @memberOf salang.authentication.controllers.LoginController
         */
        function register() {
            Authentication.register(vm.email, vm.password, vm.username, vm.name).then(successFn, errorFn);
        }


        /**
         * success function for the register call
         */
        function successFn(data, status, headers, config){
            vm.showLangs = false;
        }

        /**
         * Error function for the register call
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function errorFn(data, status, headers, config){
            //show error
            console.log("Error registering account.");
            Notification.error("Error registering account! Please try again later.");
        }

        /**
         * capture the langages a person chooses
         */
        function captureLanguages(){

            var langs = '{';

            var counter = 0;

            if ($scope.zu_check){
                langs += '"' + String(counter) + '" : { "name": "isiZulu", "short": "zu" },';
                counter++;
            }
            if ($scope.xh_check){
                langs += '"' + String(counter) + '" : { "name": "isiXhosa", "short": "xh" },';
                counter++;
            }
            if ($scope.nd_check){
                langs += '"' + String(counter) + '" : { "name": "isiNdebele", "short": "nd" },';
                counter++;
            }
            if ($scope.si_check){
                langs += '"' + String(counter) + '" : { "name": "siSwati", "short": "ss" },';
                counter++;
            }
            if ($scope.so_check){
                langs += '"' + String(counter) + '" : { "name": "Sesotho", "short": "sotho" },';
                counter++;
            }
            if ($scope.sotho_check){
                langs += '"' + String(counter) + '" : { "name": "Sesotho sa Leboa", "short": "st" },';
                counter++;
            }
            if ($scope.tswana_check){
                langs += '"' + String(counter) + '" : { "name": "Setswana", "short": "tn" },';
                counter++;
            }
            if ($scope.ve_check){
                langs += '"' + String(counter) + '" : { "name": "Tshivenda", "short": "ve" },';
                counter++;
            }
            if ($scope.ts_check){
                langs += '"' + String(counter) + '" : { "name": "Xitsonga", "short": "ts" },';
                counter++;
            }
            langs = langs.substring(0,langs.length-1) +  '}';
            var obj = JSON.parse(langs);
            //set the language for the person
            Authentication.setLangs(vm.email, counter, obj).then(langSuccessFn, langErrorFn);
        }

        /**
         * if the language were set successfully
         */
        function langSuccessFn(data, status, headers, config){
            Authentication.login(vm.email, vm.password);
        }

        /**
         * if the language were not set successfully
         */
        function langErrorFn(data, status, headers, config){
            console.log("failed to set languages");
            Notification.error("Failed to set languages");
        }
    }
})();
