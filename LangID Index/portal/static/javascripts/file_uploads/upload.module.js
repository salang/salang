(function () {
    'use strict';
    angular
        .module('salang.upload', [
            'salang.upload.controllers',
            'salang.upload.directives',
            'salang.upload.services'
        ]);

    angular.module('salang.upload.controllers', []);
    angular.module('salang.upload.directives', []);
    angular.module('salang.upload.services', []);
})();
