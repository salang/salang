(function(){
    angular
        .module('salang.upload.directives')
        .directive('docGroup', docGroup);

    docGroup.$inject = ['$compile'];

    function docGroup($compile) {
        var directive = {
            restrict: 'E',
            scope: {},
            link: linkFunc
        };

        function linkFunc(scope, element){
            // scope.checked = true;
            // scope.someText = 'some text';
            // scope.someNumber = 5;

            var markupToAppend = [
                '<input type="checkbox" ng-model="checked"/>',
                '<input type="text" ng-model="someText"/>',
                '<input type="number" ng-model="someNumber"/>'];

            markupToAppend.forEach(function(x){
                element.append($compile(x)(scope));
            });

        };

        return directive;
    };
})();