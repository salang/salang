/**
 * RecentController
 * class for the recent uploads
 * @namespace salang.upload.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.upload.controllers')
        .controller('RecentController', RecentController);

    RecentController.$inject = ['$scope', 'Uploads', 'Notification', 'LanguageConverter'];

    /**
     * @namespace RecentController
     */
    function RecentController($scope, Uploads, Notification, LanguageConverter) {

        var vm = this;
        vm.uploads = undefined;

        // get all uploads
        Uploads.all().then(allSuccessFn, allErrorFn);

        $scope.formDataDocs = {};

        /**
         * success function
         */
        function allSuccessFn(data, status, headers, config){
            //get data from response
            vm.uploads = data.data;
            // set the pager of the page
            var max  = Math.max(Math.ceil(vm.uploads.length/10),1);
            setPager(max);
            var url = window.location.href;
            var params = url.split('?');
            setActivePager(params[1], max);
            //get the correct range of documents
            var content = [];
            var mult = params[1];
            var start = 10 * (mult-1);
            var end = 10 * mult;
            end = Math.min(vm.uploads.length, end);
            // loop through the documents and get their values
            for (var i = start; i < end; i++){
                var newLang = vm.uploads[i].language;
                if (newLang === 'Sesotho sa Leboa'){
                    newLang = 'Sepedi';
                }
                var newitem = {
                    title: vm.uploads[i].title,
                    language: vm.uploads[i].language,
                    languageName: newLang,
                    description: vm.uploads[i].description,
                    id: vm.uploads[i].id
                };
                content.push(newitem);
            }
            //add new values into scope
            $scope.formDataDocs.uploads = content;
        }

        /**
         * error function
         */
        function allErrorFn(data, status, headers, config){
            console.error("can't get uploads for other users!");
            Notification.error("Can't fetch uploads for other users!");
        }

        // to get the language colour for the language pill
        $scope.getLanguageColor = function (long){
            return LanguageConverter.fromLongToShort(long);
        };

        /**
         * creates the pager numbers for the page
         * @param max
         */
        function setPager(max){
            var pager = document.getElementById('pager_recent');
            var end = document.getElementById('pager_end_recent');
            for (var i = 0; i < max; i++){
                var li = document.createElement('li');
                li.setAttribute('id', 'pager_recent_'+(i+1));
                var a = document.createElement('a');
                a.setAttribute('href', '/recent_uploads?'+(i+1));
                a.appendChild(document.createTextNode((i+1)));
                li.appendChild(a);
                pager.insertBefore(li,end);
            }
        }

        /**
         * set the active pager for the page we are on
         * @param number - page
         * @param max - what max doc is
         */
        function setActivePager(number, max){
            //set variables
            var start = document.getElementById('pager_start_recent');
            var end = document.getElementById('pager_end_recent');
            var start_url = document.getElementById('pager_start_a_recent');
            var end_url = document.getElementById('pager_end_a_recent');
            var nextPage = parseInt(number) + 1;
            var previousPage = parseInt(number) - 1;
            //set next and previous links
            start_url.setAttribute('href', 'recent_uploads?'+ previousPage);
            end_url.setAttribute('href', 'recent_uploads?'+ nextPage);
            if (max < 1){
                max = 1;
            }
            //disable next/prev if at end/beg
            if (number ==  max){
                end.setAttribute('class', 'disabled');
                end_url.setAttribute('href', '#');
            }
            if (number == 1){
                start.setAttribute('class', 'disabled');
                start_url.setAttribute('href', '#');
            }
            //set selected
            var selected = document.getElementById('pager_recent_'+number);
            selected.setAttribute('class', 'active');
        }

    }
})();



