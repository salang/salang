/**
 * FUSuccessController
 * controller for the success page after uploading
 * @namespace salang.upload.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.upload.controllers')
        .controller('FUSuccessController', FUSuccessController);

    FUSuccessController.$inject = ['$scope', 'Uploads', 'Notification', 'Authentication', 'LanguageConverter'];

    /**
     * @namespace FUSuccessController
     */
    function FUSuccessController($scope, Uploads, Notification, Authentication, LanguageConverter) {

        var vm = this;
        vm.ownUploads = undefined;
        vm.otherUploads = undefined;

        //set scope vars
        $scope.formDataDocs = {};
        $scope.getLanguageColor = function (long){
            return LanguageConverter.fromLongToShort(long);
        };

        var currentUser = Authentication.getAuthenticatedAccount().id;
        // get all uploads for a user
        Uploads.getUserUploads(currentUser).then(filteredSuccessFn, filteredErrorFn);

        /**
         * success function
         */
        function filteredSuccessFn(data, status, headers, config){
            //all user uploads
            vm.ownUploads = data.data;
            createOwnList();
            //get other uploads
            Uploads.all().then(allSuccessFn, allErrorFn);
        }

        /**
         * error function
         */
        function filteredErrorFn(data, status, headers, config){
            //show error
            console.error("Can't get uploads for user!");
            Notification.error("Can't fetch uploads for user!");
        }

        /**
         * success function
         */
        function allSuccessFn(data, status, headers, config){
            //get the data from the response
            vm.otherUploads = data.data;
            //create html items
            createOtherList();
        }

        /**
         * error function
         */
        function allErrorFn(data, status, headers, config){
            //show error
            console.error("can't get uploads for other users!");
            Notification.error("Can't fetch uploads for other users!");
        }

        /**
         * create the html elements for the users documents
         */
        function createOwnList() {
            var content = [];
            //go through documents and create items for them with their data
            for (var i = 0; i < vm.ownUploads.length; i++){
                var newLang = vm.ownUploads[i].language;
                if (newLang === 'Sesotho sa Leboa'){
                    newLang = 'Sepedi';
                }
                var newitem = {
                    title: vm.ownUploads[i].title,
                    language: vm.ownUploads[i].language,
                    languageName: newLang,
                    description: vm.ownUploads[i].description,
                    id: vm.ownUploads[i].id
                };
                content.push(newitem);
            }
            $scope.formDataDocs.ownuploads = content;
        }

        /**
         * create the html elements for the users documents
         */
        function createOtherList() {
            var content = [];
            var end = Math.min(vm.otherUploads.length, 10);
            //go through documents and create items for them with their data
            for (var i = 0; i < end; i++){
                var newLang = vm.otherUploads[i].language;
                if (newLang === 'Sesotho sa Leboa'){
                    newLang = 'Sepedi';
                }
                var newitem = {
                    title: vm.otherUploads[i].title,
                    language: vm.otherUploads[i].language,
                    languageName: newLang,
                    description: vm.otherUploads[i].description,
                    id: vm.otherUploads[i].id
                };
                content.push(newitem);
            }

            $scope.formDataDocs.alluploads = content;
        }
    }
})();
