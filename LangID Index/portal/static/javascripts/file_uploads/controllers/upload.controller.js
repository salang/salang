/**
 * FileUploadController
 * controller for uploading
 * @namespace salang.upload.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.upload.controllers')
        .controller('FileUploadController', FileUploadController);

    FileUploadController.$inject = ['$scope', '$location', 'Uploads', 'Authentication', 'Notification', 'Tours'];

    /**
     * @namespace FileUploadController
     */
    function FileUploadController($scope, $location, Uploads, Authentication, Notification, Tours) {
        var vm = this;

        //vcheck if user is logged in - restricted access page
        if (!Authentication.isAuthenticated()){
            Notification.error("Please log in.");
            $location.url('/login');
        }

        // check if the tour has been started already
        if (!Tours.checkUploadTour()){
            Tours.startUploadP1Tour();
        }

        //create variables and assign functions
        vm.getLocalFile = getLocalFile;
        vm.finishUpload = finishUpload;
        var languageIdens = 0;
        vm.noFiles = undefined;
        vm.files = undefined;
        vm.filename = undefined;
        vm.uploads = [];
        vm.lang_iden = [];
        vm.icon = true;

        var content = [];
        var hashtable = [];
        $scope.formData = {};
        $scope.formData.docs = content;


        /**
         * @name getLocalFile
         * @desc get the file selected
         * @memberOf salang.upload.controllers.FileUploadController
         */
        function getLocalFile($files){
            console.log($files);
            vm.files = $files;
            uploadFiles();
        }

        /**
         * upload the files with the Service
         */
        function uploadFiles(){
            // increment the progress bar for uploading
            var len = vm.files.length;
            vm.noFiles = len;
            var increment = 100/len;
            var percentage = 0;
            for (var i = 0; i < len; i++){
                //indetify language and upload each document
                languageIdens++;
                Uploads.sendPost(vm.files[i], vm.files[i].name).then(uploadSuccessFn, uploadErrorFn);
                var newIndex = {
                    name : vm.files[i].name
                };
                hashtable.push(newIndex);
                percentage += increment;
                incrementBar(percentage);
            }
            buildJsonObjects();
        }

        /**
         * increments the upload progress bar
         * @param amount - increment amount
         */
        function incrementBar(amount){
            $('.progress-bar').animate({
                width: amount + "%"
            }, 500);
        }

        /**
         * Build the objects for the remplate
         */
        function buildJsonObjects() {
            document.getElementById('mainpanel').removeAttribute('style');
            // add new items into html with specified values
            for (var i = 0; i < vm.noFiles; i++){
                var newitem = {
                    heading: 'Document ' + String(i+1) + " - ",
                    filename: vm.files[i].name,
                    title: '',
                    language: vm.lang_iden[i],
                    description: ''
                };
                content.push(newitem);
            }
            // apply changes
            $scope.$apply();
            // start second tour if not started before
            if (!Tours.checkUploadTour()){
                Tours.startUploadP2Tour();
            }
        }

        /**
         * disable the progress bar
         */
        function disableElements() {
            var fu = document.getElementById('progress-bar-overlay');
            fu.setAttribute('hidden', "true");
            vm.showFields = true;
        }


        /**
         * success function
         */
        function uploadSuccessFn(data, status, headers, config){
            vm.uploads.push(data.data);
            //if the document uploads then indetify the language
            Uploads.identifyLanguage(data.data.id).then(langSuccessFn, langErrorFn);
        }

        /**
         * error function
         */
        function uploadErrorFn(data, status, headers, config){
            console.log("Upload failed");
            Notification.error("Upload failed!");
            //clean the upload if it fails
            Uploads.failedUpload(data.data.id);
        }


        /**
         * if the language is correctly identified
         */
        function langSuccessFn(data, status, headers, config){
            //assign languge and move to next step
            vm.lang_iden.push(data.data.message);
            languageIdens--;
            $scope.formData.docs[languageIdens].language = data.data.message;
            if (languageIdens == 0){
                disableElements();
            }
        }


        /**
         * if the language is correctly identified
         */
        function langErrorFn(data, status, headers, config){
            console.log("identification error");
            Notification.error("Language could not be identified!");
            vm.lang_iden.push('error');
        }

        /**
         * Once the upload is finished
         */
        function finishUpload(){
            // run through the files and get all the values and handle the post uploading process
            for (var i = 0; i < vm.noFiles; i++){
                var id = vm.uploads[i].id;
                var language = $scope.formData.docs[i].language;
                var title = $scope.formData.docs[i].title;
                var desc = $scope.formData.docs[i].description;
                Uploads.postUploadHandling(id, language, title, desc).then(postSuccessFn, postErrorFn);
            }

            /**
             * Success function for post process
             */
            function postSuccessFn(data, status, headers, config){
                //if we success we show notifications of success and points
                Notification.success("Upload successful!");
                Notification.info({message: 'Congratulations! You earned <b style="color: red;">60</b> points!'});
                $location.url("/upload_success");
            }

            /**
             * Error function for post process
             */
            function postErrorFn(data, status, headers, config){
                console.log("could not handle post processing");
                Notification.error("Upload failed!");
            }
        }



    }
})();
