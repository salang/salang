/**
 * Uploads
 * Service to handle all uploading and upload requests
 * @namespace salang.upload.services
 */
(function () {
    'use strict';

    angular
        .module('salang.upload.services')
        .factory('Uploads', Uploads);

    Uploads.$inject = ['$http', 'Authentication'];

    /**
     * @namespace Uploads
     * @returns {Factory}
     */
    function Uploads($http, Authentication) {
        var Uploads = {
            all: all,
            getUserUploads: getUserUploads,
            sendPost: sendPost,
            failedUpload: failedUpload,
            postUploadHandling: postUploadHandling,
            identifyLanguage: identifyLanguage,
            getID: getID,
            updateDocumentDetails: updateDocumentDetails
        };

        return Uploads;

        ////////////////////

        /**
         * @name all
         * @desc Get all uploads
         * @returns {Promise}
         * @memberOf salang.upload.services.Uploads
         */
        function all() {
            return $http.get('/api/v1/uploads/');
        }


        /**
         * @name sendPost
         * @desc upload a doc and details
         * @returns {Promise}
         * @memberOf salang.upload.services.Uploads
         */
        function sendPost(file, fileName){
            var formdata = new FormData();
            formdata.append('docfile', file);
            // formdata.append('title', title);
            // formdata.append('description', desc);
            formdata.append('language', "NID");
            formdata.append('filename', fileName);
            formdata.append('author', Authentication.getAuthenticatedAccount().username);

            return $http({
                method: 'POST',
                url: '/api/v1/uploads/',
                headers: {'Content-Type': undefined},
                data: formdata
            });
        }



        /**
         * @name get
         * @desc Get the uploads of a given user
         * @param {string} id The id to get uploads for
         * @returns {Promise}
         * @memberOf salang.upload.services.Uploads
         */
        function getUserUploads(id) {
            return $http.get('/api/v1/documents/get_user_docs?id=' + id );
        }




        /**
         * @name failedUpload
         * @desc clean the failed upload
         * @param {Object} id the id of doc
         * @returns {Promise}
         * @memberOf salang.upload.services.Uploads
         */
        function failedUpload(id) {
            return $http.get('/api/v1/documents/clean_fail_upload?id='+id);
        }



        /**
         * @name identifyLanguage
         * @desc get lang of doc
         * @param {Object} id id of doc
         * @returns {Promise}
         * @memberOf salang.upload.services.Uploads
         */
        function identifyLanguage(id) {
            return $http.get('/api/v1/documents/identify_language?id=' + id);
        }




        /**
         * @name postUploadHandling
         * @desc processing after upload
         * @param {String} id id of doc
         * @param {String} language language of the doc
         * @param {String} title title of the doc
         * @param {String} desc description of the doc
         * @returns {Promise}
         * @memberOf salang.upload.services.Uploads
         */
        function postUploadHandling(id, language, title, desc) {
            return $http.get('/api/v1/documents/call_post_processes?id=' + id + "&lang=" + language + "&title=" + title + "&desc=" + desc + "&reward=1");
        }

        /**
         * @name getID
         * @desc get id of doc just uploaded
         * @param {String} title title of doc
         * @param {String} desc desc of the doc
         * @returns {Promise}
         * @memberOf salang.upload.services.Uploads
         */
        function getID(title, desc) {
            return $http.get('/api/v1/documents/fetch_last_doc?title=' + title + "&lang=" + desc);
        }

        /**
         * @name updateDocumentDetails
         * @desc update document
         * @param {String} id id of doc
         * @param {String} language language of the doc
         * @param {String} title title of the doc
         * @param {String} desc description of the doc
         * @returns {Promise}
         * @memberOf salang.upload.services.Uploads
         */
        function updateDocumentDetails(id, language, title, desc) {
            console.log('called service');
            return $http.get('/api/v1/documents/call_post_processes?id=' + id + "&lang=" + language + "&title=" + title + "&desc=" + desc + "&reward=0");
        }




    }
})();