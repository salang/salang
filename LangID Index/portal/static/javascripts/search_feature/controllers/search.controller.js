/**
 * searchController
 * @namespace salang.search.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.search.controllers')
        .controller('searchController', searchController);

    searchController.$inject = ['$scope', '$location', '$rootScope', ];

    /**
     * @namespace searchController
     */
    function searchController($scope, $location, $rootScope) {

        /**
         * Started the searching process
         * when search button pressed
         */
        $scope.search = function() {
            if (this.originalQuery) {
                this.getNumCollections();
                $location.path('/results');
                $location.search('q', this.originalQuery);

            } else {
                alert("Please enter a search query.");
            }
        };

        $scope.currentPath = function() {
            if ($location.path() == '/results') {
                return '/results';
            } else {
                $rootScope.originalQuery = "";
            }
        };

        /**
         * Used when automatically selecting a language in advance settings.
         * Matches to terminal selected language.
         */
        $scope.getChecked = function(lang) {
            if (lang === localStorage.getItem('NG_TRANSLATE_LANG_KEY')) {
                return true;
            }
        };

        /**
         * Get number of languages selected in order to know how many http calls to solr needed.
         */
        $rootScope.getNumCollections = function() {
            $rootScope.langCount = 0;
            if ($rootScope.searchLanguage.english) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.ndebele) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.sotho) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.sesotho) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.swazi) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.tsonga) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.tswana) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.venda) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.xhosa) {
                $rootScope.langCount++;
            }
            if ($rootScope.searchLanguage.zulu) {
                $rootScope.langCount++;
            }

            $rootScope.fixedLangCount = $rootScope.langCount;
        };

        /**
         * Return the language name for the abbreviation
         * @param {String} abbr
         * @param {String} full language name 
         */
        $rootScope.getLanguageName = function(abbr) {
            switch (abbr) {
                case 'nd':
                    return "isiNdebele";
                case 'sotho':
                    return "Sesotho sa Leboa";
                case 'st':
                    return "Sesotho";
                case 'ss':
                    return "siSwati";
                case 'ts':
                    return "Xitsonga";
                case 'tn':
                    return "Setswana";
                case 've':
                    return "Tshivenda";
                case 'xh':
                    return "isiXhosa";
                case 'zu':
                    return "isiZulu";
                default:
                    return "English";

            }
        };

        $scope.getQuery = function() {
            return $location.path().substring(8);
        };


    }
})();