/**
 * resultsController
 * @namespace salang.search.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.search.controllers')
        .controller('resultsController', resultsController);

    resultsController.$inject = ['$scope', '$rootScope', '$http', 'SearchBackend', 'Authentication'];

    /**
     * @namespace resultsController
     */
    function resultsController($scope, $rootScope, $http, SearchBackend, Authentication) {
        /**
         * Check if result has a title.
         * If not, use the ID as a title.
         * @param {Document Object} doc
         */
        $scope.checkTitle = function(doc) {
            if (doc.title === undefined || doc.title === null) {
                doc.title = [doc.source_url];
            }
            doc = checkURL(doc);
            return doc;
        };

        /**
         * Special case for user documents in solr
         * The link to doc in id
         * @param {Document Object} doc
         */
        function checkURL(doc) {
            if (doc.source_url.indexOf("http") === -1) {
                doc.source_url = doc.id;
            }
            return doc;
        }

        /**
         * This method is for storing results for BLIND INTERLEAVING.
         * @param {Docmunt Object[]} docs
         * @param {int} numFound
         */
        $rootScope.blindInterleavingStorage = function(docs, numFound) {
            var tempDocs = [];
            for (var i = 0; i < Math.min($rootScope.numSearchResults, numFound); i++) {
                tempDocs.push($scope.checkTitle(docs[i]));
            }
            $rootScope.docs[$rootScope.langCount] = tempDocs;
        };

        /**
         * This method is for ordering results for BLIND INTERLEAVING.
         */
        $rootScope.blindInterleaving = function() {

            var tempDocs = [];
            //blindly interleave collections//
            for (var i = 0; i < 20; i++) {
                for (var k = 0; k < $rootScope.docs.length; k++) {
                    if ($rootScope.docs[k][i] != null) {
                        tempDocs.push($rootScope.docs[k][i]);
                    }
                }
            }
            $rootScope.docs = tempDocs;
        };

        /**
         * This method is for storing results for SMART INTERLEAVING.
         */
        $rootScope.smartInterleavingStorage = function(docs, numFound) {
            for (var i = 0; i < Math.min($rootScope.numSearchResults, numFound); i++) {
                $rootScope.docs.push($scope.checkTitle(docs[i]))
            }
        };

        /**
         * This method is for ordering results for SMART INTERLEAVING.
         */
        $rootScope.smartInterleaving = function() {
            $rootScope.docs.sort(function(a, b) {
                return b.score - a.score
            });
        };


        /**
         * RELEVANCE EVALUATION
         */

        /**
         * Stores users vote information in the backend
         * Used to calculate relevance when votes submitted
         * @param {int} vote
         * @param {int} pos 
         */
        $scope.relevanceVote = function(vote, pos) {
            console.log(vote + " vote on result " + pos);
            var id = undefined;
            if (Authentication.isAuthenticated()) {
                id = Authentication.getAuthenticatedAccount().id;
            }

            SearchBackend.relevanceVote(vote, pos, id).then(voteSuccessFn, voteErrorFn);

            function voteSuccessFn(data, status, headers, config) {
                console.log("The vote method ran successfully!!!");
            }

            function voteErrorFn(data, status, headers, config) {
                console.log("The vote method failed!!!");
            }
        };

        /**
         * Submit the relevance votes submitted by the user
         * Write data to file
         */
        $scope.submitEvaluation = function() {
            var id = undefined;
            if (Authentication.isAuthenticated()) {
                id = Authentication.getAuthenticatedAccount().id;
            }
            SearchBackend.submitEvaluation($rootScope.originalQuery, $scope.languagesSearchedString(), $rootScope.primary.language, $rootScope.testing.method, id).then(voteSuccessFn, voteErrorFn);

            function voteSuccessFn(data, status, headers, config) {
                console.log("The submit vote method ran successfully!!!");
                alert("Results submitted successfully!");
            }

            function voteErrorFn(data, status, headers, config) {
                $scope.clearData();
                alert("Something went wrong.\nCould you please enter the relevancy judgements again and click submit.\nSorry for the inconvenience!");
                console.log("The submit vote method failed!!!");
            }
        };

        /**
         * Clear data stored in the back for relevance evaluation.
         */
        $scope.clearData = function() {
            var id = undefined;
            if (Authentication.isAuthenticated()) {
                id = Authentication.getAuthenticatedAccount().id;
            }

            SearchBackend.clearData(id).then(voteSuccessFn, voteErrorFn);

            function voteSuccessFn(data, status, headers, config) {
                console.log("The clear data method ran successfully!!!");
            }

            function voteErrorFn(data, status, headers, config) {
                console.log("The clear data method failed!!!");
            }
        };

        /**
         * Record a language report on a returned result
         * @param {String} url
         * @param {String} collection
         */
        $scope.reportLanguage = function(url, collection) {
            var id = undefined;
            if (Authentication.isAuthenticated()) {
                id = Authentication.getAuthenticatedAccount().id;
            }
            SearchBackend.reportLanguage(url, collection, id).then(voteSuccessFn, voteErrorFn);

            function voteSuccessFn(data, status, headers, config) {
                alert("Web page successfully reported.");
                console.log("Language reported successfully!!!");
            }

            function voteErrorFn(data, status, headers, config) {
                console.log("Language report failed!!!");
            }
        };

        /**
         * Create a string of languages in which in the query was searched 
         * Used when writing data to file for evaluation
         * @return {String} languagesString
         */
        $scope.languagesSearchedString = function() {
            var languagesString = "";
            if ($rootScope.searchLanguage.english) {
                languagesString += ", English";
            }
            if ($rootScope.searchLanguage.ndebele) {
                languagesString += ", isiNdebele";
            }
            if ($rootScope.searchLanguage.sotho) {
                languagesString += ", Sesotho sa Leboa";
            }
            if ($rootScope.searchLanguage.sesotho) {
                languagesString += ", Sesotho";
            }
            if ($rootScope.searchLanguage.swazi) {
                languagesString += ", siSwati";
            }
            if ($rootScope.searchLanguage.tsonga) {
                languagesString += ", Xitsonga";
            }
            if ($rootScope.searchLanguage.tswana) {
                languagesString += ", Setswana";
            }
            if ($rootScope.searchLanguage.venda) {
                languagesString += ", Tshivenda";
            }
            if ($rootScope.searchLanguage.xhosa) {
                languagesString += ", isiXhosa";
            }
            if ($rootScope.searchLanguage.zulu) {
                languagesString += ", isiZulu";
            }
            return languagesString;
        };

    }
})();