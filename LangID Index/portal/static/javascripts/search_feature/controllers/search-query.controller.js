/**
 * queryController
 * @namespace salang.search.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.search.controllers')
        .controller('queryController', queryController);

    queryController.$inject = ['$scope', '$rootScope', '$http', 'SearchBackend', ];

    /**
     * @namespace queryController
     */
    function queryController($scope, $rootScope, $http, SearchBackend) {

        $rootScope.refineQuery = function(language) {
            //$scope.language = language;
            $rootScope.query = $rootScope.query.toLowerCase();
            //$scope.boolOp();
            $scope.punctuation();
            //$scope.transNums();
            //$scope.stopWords();
        }

        /**
         * Searches for boolean operators in the Bantu languages
         * Replaces the boolean operators with AND/OR so solr understands
         */
        $scope.boolOp = function() {
            var ORs = [];
            var ANDs = [];

            switch ($scope.language) {
                case 'ndebele':
                    ORs = [];
                    ANDs = [];
                    break;
                case 'sotho':
                    ORs = ["kapa"];
                    ANDs = ["mme", "le"];
                    break;
                case 'sesotho':
                    ORs = ["kapa"];
                    ANDs = ["mme", "le"];
                    break;
                case 'swazi':
                    ORs = [];
                    ANDs = [];
                    break;
                case 'tsonga':
                    ORs = [];
                    ANDs = [];
                    break;
                case 'tswana':
                    ORs = ["kana", "kgotsa"];
                    ANDs = ["le", "mme"];
                    break;
                case 'venda':
                    ORs = ["kana"];
                    ANDs = ["na"];
                    break;
                case 'xhosa':
                    ORs = [];
                    ANDs = ["kwaye", "futhi"];
                    break;
                case 'zulu':
                    ORs = ["noma"];
                    ANDs = ["futhi"];
                    break;
                default:
                    //english
                    ORs = [];
                    ANDs = [];
                    break;
            }

            do {
                var noneLeft = true;
                // ORs //
                for (var i = 0; i < ORs.length; i++) {
                    var regWord = new RegExp('\\b' + ORs[i] + '\\b');
                    var pos = $rootScope.query.search(regWord);
                    if (pos > -1) {
                        $rootScope.query = $rootScope.query.substring(0, pos) + "OR" + $rootScope.query.substring(pos + ORs[i].length);
                        noneLeft = false;
                    }
                }
                // ANDs //
                for (var i = 0; i < ANDs.length; i++) {
                    var regWord = new RegExp('\\b' + ANDs[i] + '\\b');
                    var pos = $rootScope.query.search(regWord);
                    if (pos > -1) {
                        $rootScope.query = $rootScope.query.substring(0, pos) + "AND" + $rootScope.query.substring(pos + ANDs[i].length);
                        noneLeft = false;
                    }
                }

            } while (!noneLeft);
        }

        /**
         * Replaces the digit number with the written number in the respecitive bantu language.
         */
        $scope.transNums = function() {
            var numbers = [];

            switch ($scope.language) {
                case 'ndebele':
                    numbers = ["kunye", "kubili", "kuthathu", "kune", "kuhlanu", "sithandathu", "likhomba", "bunane", "lithoba", "itjhumi"];
                    break;
                case 'sotho':
                    numbers = ["tee", "pedi", "tharo", "nne", "hlano", "tshela", "šupa", "seswai", "senyane", "lesome"];
                    break;
                case 'sesotho':
                    numbers = ["nngwe", "pedi", "tharo", "nne", "hlano", "tshelela", "supa", "robedi", "robong", "leshome"];
                    break;
                case 'swazi':
                    numbers = ["kunye", "kubili", "kutsatfu", "kune", "kusihlanu", "kusitfupha", "kusikhombisa", "kusiphohlongo", "kuyimfica", "kulishumi",
                        "lishumi nakune", "ishumi nakubili", "lishumi nakutsatfu", "lishumi nakune", "lishumi nesihlanu"
                    ];
                    break;
                case 'tsonga':
                    numbers = ["n'we", "mbirhi", "nharhu", "mune", "ntlhanu", "tsevu", "nkombo", "nhungu", "kaye", "khume", "khumen'we", "khumembirhi",
                        "khumenharhu", "khumemune", "khumentlhanu", "khumetsevu", "khumekombo", "khumenhungu", "khumekaye", "makumembirhi"
                    ];
                    break;
                case 'tswana':
                    numbers = ["mmgwe", "pedi", "tharo", "nne", "tlhano", "thataro", "supa", "robedi", "robongwe", "lesome", "lesomenngwe", "lesomepedi",
                        "lesomenne", "lesometharo", "lesometlhano", "lesomethataro", "lesomesupa", "lesomerobedi", "lesomerobongwe", "masomepedi"
                    ];
                    break;
                case 'venda':
                    numbers = ["thihi", "mbili", "tharu", "iṋa", "ṱhanu", "rathi", "sumbe", "malo", "ṱahe", "fumi", "fuminthihi", "fumimbili",
                        "fumiraru", "fumiṋa", "fumiṱhanu", "fumirathi", "fumisumbe", "fumimalo", "fumiṱahe", "fumbili"
                    ];
                    break;
                case 'xhosa':
                    numbers = ["nye", "mbini", "ntathu", "ne", "hlanu", "thandathu", "sixhenxe", "bhozo", "lithoba", "lishumi"];
                    break;
                case 'zulu':
                    numbers = ["kunye", "kubili", "kuthathu", "kune", "kuhlanu", "isithupha", "isikhombisa", "isishiyagalombili", "isishiyagalolunye", "ishumi"];
                    break;
                default:
                    //english
                    numbers = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
                        "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty"
                    ];
                    break;
            }


            //get both numeral and word versions of a number into the query//
            for (var i = 1; i <= numbers.length; i++) {
                var regNum = new RegExp('\\b' + i + '\\b');
                var regWord = new RegExp('\\b' + numbers[i - 1] + '\\b');
                var posNum = $rootScope.query.search(regNum);
                var posWord = $rootScope.query.search(regWord);
                //change numeral first if further down the query
                if (posNum > posWord) {
                    if (posNum > -1) {
                        $rootScope.query = $rootScope.query.substring(0, posNum) + "\"" + i + " OR " + numbers[i - 1] + "\" " + $rootScope.query.substring(posNum + 2);
                    }
                    if (posWord > -1) {
                        $rootScope.query = $rootScope.query.substring(0, posWord) + "\"" + i + " OR " + numbers[i - 1] + "\" " + $rootScope.query.substring(posWord + numbers[i - 1].length);
                    }
                }
                //change word first if further down the query
                else {
                    if (posWord > -1) {
                        $rootScope.query = $rootScope.query.substring(0, posWord) + "\"" + i + " OR " + numbers[i - 1] + "\" " + $rootScope.query.substring(posWord + numbers[i - 1].length);
                    }
                    if (posNum > -1) {
                        $rootScope.query = $rootScope.query.substring(0, posNum) + "\"" + i + " OR " + numbers[i - 1] + "\" " + $rootScope.query.substring(posNum + 2);
                    }
                }

            }
        }

        /**
         * Find and remove stop words from the Bantu web query
         */
        $scope.stopWords = function() {

            var stopwords = [];

            switch ($scope.language) {
                case 'ndebele':
                    stopwords = ["ngiyacela", "ye", "wena", "yini", "ilizwi", "ulutho", "nxa", "yebo", "hayi", "hatshi", "ehe", "hayikhona"];
                    break;
                case 'sotho':
                    stopwords = ["ee", "aowa", "mohlômong", "mohlômongwê", "ka kgopelo", "hle", "ke nyaka", "ke"];
                    break;
                case 'sesotho':
                    stopwords = ["Êe", "êe", "tjhee", "kopo", "jwang", "eng", "neng", "hobaneng", "mang", "hokae", "ba bakae", "e le", "ke",
                        "hae", "hore", "o ile a", "e ne e", "bakeng sa", "ka", "ba", "le", "ba", "ena", "tloha", "empa", "e", "u", "ba bang ba",
                        "ne", "ea", "ho", "le", "a", "re", "ka", "tse ling tse", "e leng", "etsa", "ba bona ba", "haeba", "tla", "joang",
                        "mong le e mong", "etsang", "batla", "e boetse e", "mona", "lokela ho", "joalo", "ke hobane’ng ha", "a tsoela", "re", "hape", "rōna",
                        "haholo", "feela", "le eena o", "ka nako eo", "joaloka", "e le", "mo", "hona joale", "ho fihlela", "‘na", "e mong",
                        "a", "bane", "bona", "eaba", "ena", "ha", "la", "li", "me", "moo", "o", "oa", "re", "sa", "se", "tsa", "tse"
                    ];
                    break;
                case 'swazi':
                    stopwords = ["yebo", "cha", "chake", "ngiyacela", "mine", "ngimi", "ngitsi", "kwetfu", "kwami", "leni", "kuphi", "njani", "kanjani",
                        "malini"
                    ];
                    break;
                case 'tsonga':
                    stopwords = ["ina", "e-e", "Êe", "êe", "xa mina", "xa vona", "xa wena", "ndza kombela", "sweswi", "mina", "vona", "hina", "wena", "u",
                        "ndzi ta", "ni nga", "ndzi nga", "ni", "xihi na xihi", "xilo", "swilo"
                    ];
                    break;
                case 'tswana':
                    stopwords = ["tswêê-tswêê", "ee", "nnyaa", "tswee-tswee", "tsweetswee", "mang", "leng", "kae", "bokae", "mokae", "jang", "eng", "efe", "sharpu", "gape",
                        "jang", "gonne", "ka gonne", "ka ntha ya gone", "mo go", "fela", "pila", "sentle", "thata", "mang", "nako", "wena", "dira", "dirile",
                        "nna", "rona", "lona", "bone", "ke", "jaaka", "ka jalo", "ka gore", "wa", "ne", "na", "a", "tse", "go", "re", "a", "tle", "ng", "pa", "o"
                    ];
                    break;
                case 'venda':
                    stopwords = ["ee", "hai", "ndi khou tou humbela", "nne", "ndi", "rine", "ri", "vhone", "vha", "ene", "u", "inwi", "ni", "iwe", "nnyi",
                        "mini", "lini", "gai", "ngafhi", "ngani", "hani", "na mini", "khamusi"
                    ];
                    break;
                case 'xhosa':
                    stopwords = ["ewe", "hayi", "omnye", "mhlawumbi", "ndicela", "kona", "mna", "lam", "wena", "yakho", "njalo-njalo", "yena", "thina", "nina",
                        "bona", "lo", "aba", "le", "eli", "la", "esi", "ezi", "olu", "obu", "oku", "lowa", "abaya", "leya", "eliya", "lawa", "esiya", "eziya",
                        "oluya", "obuya", "okuya", "apha", "apho", "ngubani", "ntoni", "phi", "ni", "njani", "kanjani", "na", "ne", "no", "kunye", "ukuba",
                        "kaloku", "ngokuba", "uze", "phakathi", "nge", "kodwa", "ye", "suka", "intoni", "ubani", "ngoba", "eyam", "yakhe", "yethu", "yabo"
                    ];
                    break;
                case 'zulu':
                    stopwords = ["yebo", "cha", "okunye", "mhlawumbe", "ake", "ngicela", "siza", "njengoba", "I", "wakhe", "ukuthi", "yena", "kwaba", "ngoba",
                        "on", "kukhona", "nge", "bona", "kube", "at", "abe", "le", "kusuka", "by", "kodwana", "lokho", "kwezinye", "kuyinto", "it", "wena",
                        "kwadingeka", "lo", "of", "kuya", "a", "in", "thina", "okusemandleni", "ezinye", "babengamadoda", "okuyinto", "enza", "yabo", "uma",
                        "kuthanda", "indlela", "an", "lapha", "kungani", "waya", "nathi", "yisiphi", "lapho", "kodwa", "weza", "ethu", "kakhulu", "nje", "khona",
                        "konke", "sakho", "ke", "kanjalo", "la", "belungafica", "ine", "wenza", "yami", "akukho", "nalabo", "kuze", "awu", "nokho", "ngiyindlela",
                        "phezulu", "emuva", "ngemuva", "bonke", "yonke", "onke", "zonke", "sobabili", "nobabili", "kepha", "futhi", "kahle", "kanye", "kusho", "lakhe",
                        "mina", "ngesikhathi", "phansi", "u", "ukuba", "ukuze", "wahamba", "wami", "wase", "wathi", "yakhe", "zakhe"
                    ];
                    break;
                default:
                    //english
                    stopwords = ["the", "on", "in", "yes", "no", "other", "how", "many", "what", "when", "why", "who", "where", "a", "this", "is"];
                    break;
            }

            //Check if there are non-stopwords in the query before removing stop words.
            var isNoneStopword = false;
            var queryWords = $rootScope.query.split(" ");
            for (var i = 0; i < queryWords.length; i++) {
                if (!stopwords.includes(queryWords[i])) {
                    isNoneStopword = true;
                    break;
                }
            }

            //if there are none-stopwords in the query, remove the stop words
            if (isNoneStopword) {
                do {
                    var noneLeft = true;
                    for (var i = 0; i < stopwords.length; i++) {
                        var regWord = new RegExp('\\b' + stopwords[i] + '\\b');
                        var pos = $rootScope.query.search(regWord);
                        if (pos > -1) {
                            $rootScope.query = $rootScope.query.substring(0, pos) + $rootScope.query.substring(pos + stopwords[i].length);
                            noneLeft = false;
                        }
                    }

                } while (!noneLeft);
            }

        }

        /**
         * Remove punctuation from the query which causes http call to solr to crash.
         */
        $scope.punctuation = function() {
            var regWord = new RegExp('\\?');
            var pos = $rootScope.query.search(regWord);
            if (pos > -1) {
                $rootScope.query = $rootScope.query.substring(0, pos) + $rootScope.query.substring(pos + 1);
            }

            regWord = new RegExp('\-');
            pos = $rootScope.query.search(regWord);
            if (pos > -1) {
                $rootScope.query = $rootScope.query.substring(0, pos) + $rootScope.query.substring(pos + 1);
            }

            regWord = new RegExp('\,');
            pos = $rootScope.query.search(regWord);
            if (pos > -1) {
                $rootScope.query = $rootScope.query.substring(0, pos) + $rootScope.query.substring(pos + 1);
            }
        }
    }
})();