/**
 * SearchBackend
 * @namespace salang.search.services
 */
(function() {
    'use strict';

    angular
        .module('salang.search.services')
        .factory('SearchBackend', SearchBackend);

    SearchBackend.$inject = ['$http', '$rootScope', ];

    /**
     * @namespace SearchBackend
     */
    function SearchBackend($http, $rootScope) {
        /**
         * @name SearchBackend
         * @desc The factory to be returned
         * @memberOf salang.search.services.SearchBackend
         */
        var SearchBackend = {
            increment: increment,
            defaultSearchLanguage: defaultSearchLanguage,
            relevanceVote: relevanceVote,
            submitEvaluation: submitEvaluation,
            reportLanguage: reportLanguage,
            clearData: clearData,
            translateQuery: translateQuery

        };

        return SearchBackend;



        /**
         * @name increment
         * @desc increment solr index
         * @returns {Promise}
         * @memberOf salang.search.services.SearchBackend
         */
        function increment(collection, id) {
            return $http.get('/api/v1/search/increment_counter?collection=' + collection + '&id=' + id);
        }

        /**
         * @name relevanceVote
         * @desc for relevance votes
         * @returns {Promise}
         * @memberOf salang.search.services.SearchBackend
         */
        function relevanceVote(vote, pos, id) {
            return $http.get('/api/v1/search/relevance_Vote?vote=' + vote + '&pos=' + pos + "&id=" + id);
        }

        /**
         * @name relevanceVote
         * @desc for relevance votes
         * @returns {Promise}
         * @memberOf salang.search.services.SearchBackend
         */
        function reportLanguage(url, collection, id) {
            return $http.get('/api/v1/search/report_Language?url=' + url + '&collection=' + collection + "&id=" + id);
        }


        /**
         * @name submitEvaluation
         * @desc for submitting evaluation
         * @returns {Promise}
         * @memberOf salang.search.services.SearchBackend
         */
        function submitEvaluation(query, languages, primaryLanguage, testMethod, id) {
            return $http.get('/api/v1/search/submit_Evaluation?query=' + query + '&languages=' + languages + "&primaryLanguage=" + primaryLanguage + "&testMethod=" + testMethod + "&id=" + id);
        }

        /**
         * @name clearData
         * @desc for clearing data
         * @returns {Promise}
         * @memberOf salang.search.services.SearchBackend
         */
        function clearData(id) {
            return $http.get('/api/v1/search/clear_Data?id=' + id);
        }



        /**
         * When the language of the portal is changed
         * the default search language is updated.
         * This is then used to update the checked search languages
         */
        function defaultSearchLanguage() {

            $rootScope.searchLanguage.english = false;
            $rootScope.searchLanguage.ndebele = false;
            $rootScope.searchLanguage.sotho = false;
            $rootScope.searchLanguage.sesotho = false;
            $rootScope.searchLanguage.swazi = false;
            $rootScope.searchLanguage.tsonga = false;
            $rootScope.searchLanguage.tswana = false;
            $rootScope.searchLanguage.venda = false;
            $rootScope.searchLanguage.xhosa = false;
            $rootScope.searchLanguage.zulu = false;

            switch (localStorage.getItem('NG_TRANSLATE_LANG_KEY')) {
                case 'en':
                    //$rootScope.searchLanguage.english = true;
                    break;
                case 'nd':
                    $rootScope.searchLanguage.ndebele = true;
                    break;
                case 'sotho':
                    $rootScope.searchLanguage.sotho = true;
                    break;
                case 'st':
                    $rootScope.searchLanguage.sesotho = true;
                    break;
                case 'ss':
                    $rootScope.searchLanguage.swazi = true;
                    break;
                case 'ts':
                    $rootScope.searchLanguage.tsonga = true;
                    break;
                case 'tn':
                    $rootScope.searchLanguage.tswana = true;
                    break;
                case 've':
                    $rootScope.searchLanguage.venda = true;
                    break;
                case 'xh':
                    $rootScope.searchLanguage.xhosa = true;
                    break;
                case 'zu':
                    $rootScope.searchLanguage.zulu = true;
                    break;
                default:
                    //$rootScope.searchLanguage.english = true;
                    break;
            }
        }

        /**
         * Send query to google translate api to be translated.
         * @param {String} source
         * @param {String} target
         * @param {String} origQuery
         */
        function translateQuery(source, target, origQuery) {

            return $http({
                method: 'GET',
                url: 'https://www.googleapis.com/language/translate/v2',
                params: {
                    'key': 'AIzaSyCui5dXh7gXxjOlKS5ZuwlIpeUNcKPOXAw',
                    'source': source,
                    'target': target,
                    'q': origQuery

                }
            }).then(function(response) {
                console.log("TRANSLATION SUCCESSFUL!!!");
                return response.data;
            }, function(response) {
                console.log("TRANSLATION FAILED");
            });

        }
    }
})();