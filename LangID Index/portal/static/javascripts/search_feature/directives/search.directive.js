/**
 * solrSearchResults
 * @namespace salang.search.directives
 */
(function() {
    'use strict';

    angular
        .module('salang.search.directives')
        .directive('solrSearchResults', solrSearchResults);

    /**
     * @namespace solrSearchResults
     */
    function solrSearchResults($rootScope, $location, $window) {
        /**
         * @name directive
         * @desc The directive to be returned
         * @memberOf salang.search.directives.solrSearchResults
         */
        var directive = {
            scope: {
                solrCollectionUrl: '=',
                currentCollection: '=',
                results: '&'
            },
            restrict: 'E',
            controller: function($scope, $http, SearchBackend) {

                //Refresh the page when a new search query is entered//
                $scope.$on('$locationChangeSuccess', function() {
                    // if ($location.path() == '/results') {
                    //     $window.location.reload();
                    // }                    
                });

                //If the number of collections to search is 0, then the results won't print.
                //This generally occurs when the search query is enter through the URL.
                if ($rootScope.fixedLangCount == 0) {
                    console.log("refreshing the number of collections to search...");
                    $rootScope.getNumCollections();
                }
                $rootScope.docs = []; //clear old results from docs list
                $scope.results.canPrint = false;

                //Get search query from URL, store in rootScope query and submit search to solr
                $rootScope.query = $rootScope.originalQuery = $location.search().q;
                $rootScope.refineQuery('english');
                if (($rootScope.primary.language == 'en' || $rootScope.primary.language == 'zu' || $rootScope.primary.language == 'xh' || $rootScope.primary.language == 'st' || $rootScope.primary.language == 'sotho') && ($scope.currentCollection == 'zu' || $scope.currentCollection == 'xh' || $scope.currentCollection == 'st' || $rootScope.primary.language == 'sotho') && ($rootScope.primary.language != $scope.currentCollection)) {
                    var transQuery = SearchBackend.translateQuery($rootScope.primary.language, $scope.currentCollection, $rootScope.originalQuery);
                    transQuery.then(function(result) {
                        $rootScope.query = result.data.translations[0].translatedText;
                        $rootScope.refineQuery('english');
                        console.log('Searching for ' + $rootScope.query + ' at ' + $scope.solrCollectionUrl);
                        console.log('ORIGINAL QUERY: ' + $rootScope.originalQuery);
                        console.log("REFINED QUERY: " + $rootScope.query);

                        runSolrSearch();

                    });
                } else {
                    runSolrSearch();
                }

                function runSolrSearch() {
                    $http({
                            method: 'JSONP',
                            url: 'http://137.158.60.198:8983/solr' + $scope.solrCollectionUrl,
                            //url: 'http://192.168.0.7:8983/solr' + $scope.solrCollectionUrl,
                            params: {
                                'fl': 'source_url, id, title, description, score',
                                'sort': 'score desc',
                                'rows': $rootScope.numSearchResults,
                                'json.wrf': 'JSON_CALLBACK',
                                'wt': 'json',
                                'defType': 'edismax',
                                'lowercaseOperators': true, //supports boolean operators - upper and lower case
                                'mm': '1<75% 6<80%', //mm = minimum number should match,
                                'q': $rootScope.query,
                                'rq': '{!rerank reRankQuery=' + $rootScope.query + ' reRankDocs=10 reRankWeight=1}' /*reRank the first 10 results*/
                            }
                        })
                        .success(function(data) {

                            console.log("SEARCH SUCCESS!!!");
                            console.log(data.response);

                            $rootScope.numResults = data.response.numFound;
                            if ($rootScope.docs.length == 0) {
                                $rootScope.docs = [];
                            }

                            //Results from solr and added into the rootscope
                            for (var i = 0; i < Math.min($rootScope.numSearchResults, data.response.numFound); i++) {
                                data.response.docs[i].collection = $scope.currentCollection;
                                data.response.docs[i].language = $rootScope.getLanguageName($scope.currentCollection);
                            }

                            $rootScope.langCount--;

                            //INTERLEAVING OF RESULTS//
                            if ($rootScope.testing.method === "one") {
                                $rootScope.blindInterleavingStorage(data.response.docs, data.response.numFound);
                            } else if ($rootScope.testing.method === "two") {
                                $rootScope.smartInterleavingStorage(data.response.docs, data.response.numFound);
                            } else if ($rootScope.testing.method === "three") {
                                console.log("At this point we would be running ranking method three - SOMETHING ELSE!");
                            }

                            // //Once have all search results - order results on solr score
                            if ($rootScope.langCount == 0) {
                                if ($rootScope.testing.method === "one") {
                                    $rootScope.blindInterleaving();
                                } else if ($rootScope.testing.method === "two") {
                                    $rootScope.smartInterleaving();
                                } else if ($rootScope.testing.method === "three") {
                                    console.log("At this point we would be running ranking method three - SOMETHING ELSE!");
                                }

                                $scope.results.canPrint = true;
                                $rootScope.langCount = $rootScope.fixedLangCount;

                            }

                            //Only store the first 10 results in the docs array used in show results.//
                            $scope.results.docs = [];
                            for (var i = 0; i < Math.min(10, data.response.numFound); i++) {
                                $scope.results.docs[i] = $rootScope.docs[i];
                            }
                            $scope.results.numResults = $rootScope.numResults;

                        }).error(function() {
                            console.log('Search failed!');
                        });
                };
            },
            templateUrl: 'static/templates/search_feature/search-feature.showResults.html'
        };

        return directive;
    }
})();