/**
 * totalSearch
 * @namespace salang.search.directives
 */
(function() {
    'use strict';

    angular
        .module('salang.search.directives')
        .directive('totalSearchResults', totalSearchResults);

    /**
     * @namespace totalSeach
     */
    function totalSearchResults($rootScope, $location, $window) {
        /**
         * @name directive
         * @desc The directive to be returned
         * @memberOf salang.search.directives.solrSearchResults
         */
        var directive = {
            scope: {
                solrCollectionUrl: '=',
                currentCollection: '=',
                results: '&'
            },
            restrict: 'E',
            controller: function($scope, $http) {

                $scope.results.canPrint = true;
                $scope.$watch('searchQuery', function() {
                    $http({
                            method: 'JSONP',
                            url: 'http://137.158.60.198:8983/solr' + $scope.solrCollectionUrl,
                            params: {
                                'fl': 'source_url, title, description, score',
                                'rows': 100,
                                'json.wrf': 'JSON_CALLBACK',
                                'wt': 'json',
                                'q': '*:*',
                            }
                        })
                        .success(function(data) {

                            console.log("SEARCH SUCCESS!!!");
                            console.log(data.response.docs);

                            //STORED IF WANT TO ACCESS LATER//
                            $rootScope.numResults = data.response.numFound;
                            $rootScope.docs = data.response.docs;

                            //STORED TO PRINT OUT IN SHOWRESULTS//
                            $scope.results.docs = $rootScope.docs;
                            $scope.results.numResults = $rootScope.numResults;

                        }).error(function() {
                            console.log('Search failed!');
                        });
                });
            },
            templateUrl: 'static/templates/search_feature/search-feature.showResults.html'
        };

        return directive;
    }
})();