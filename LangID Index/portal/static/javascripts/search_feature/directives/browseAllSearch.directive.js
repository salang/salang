/**
 * browseAllSearchResults
 * @namespace salang.search.directives
 */
(function() {
    'use strict';

    angular
        .module('salang.search.directives')
        .directive('browseAllSearchResults', browseAllSearchResults);

    /**
     * @namespace browseAllSearchResults
     */
    function browseAllSearchResults($rootScope, $location, $window) {
        /**
         * @name directive
         * @desc The directive to be returned
         * @memberOf salang.search.directives.solrSearchResults
         */
        var directive = {
            scope: {
                //solrCollectionUrl: '=',
                //currentCollection: '=',
                results: '&'
            },
            restrict: 'E',
            controller: function($scope, $http) {

                $scope.IP = '192.168.0.7:8983';
                $scope.results.canPrint = true;
                $scope.$watch('searchQuery', function() {
                    $http({
                            method: 'JSONP',
                            url: 'http://' + $scope.IP + '/solr/isiXhosa/select',
                            params: {
                                'shards': $scope.IP + '/solr/Tshivenda,' + $scope.IP + '/solr/isiXhosa,' + $scope.IP + '/solr/Xitsonga,' +
                                    $scope.IP + '/solr/Sesotho,' + $scope.IP + '/solr/Setswana,' + $scope.IP + '/solr/isiNdebele,' +
                                    $scope.IP + '/solr/Sesotho_sa_Leboa,' + $scope.IP + '/solr/isiZulu,' + $scope.IP + '/solr/siSwati',
                                'fl': 'source_url, title, description, score',
                                'rows': 100,
                                'json.wrf': 'JSON_CALLBACK',
                                'wt': 'json',
                                'q': '*:*',
                            }
                        })
                        .success(function(data) {

                            console.log("SEARCH SUCCESS!!!");
                            console.log(data.response.docs);

                            //STORED IF WANT TO ACCESS LATER//
                            $rootScope.numResults = data.response.numFound;
                            $rootScope.docs = data.response.docs;

                            //STORED TO PRINT OUT IN SHOWRESULTS//
                            $scope.results.docs = $rootScope.docs;
                            $scope.results.numResults = $rootScope.numResults;

                        }).error(function() {
                            console.log('Search failed!');
                        });
                });
            },
            templateUrl: 'static/templates/search_feature/search-feature.showResults.html'
        };

        return directive;
    }
})();