(function() {
    'use strict';
    var searchFeature = angular
        .module('salang.search', [
            'salang.search.controllers',
            'salang.search.directives',
            'salang.search.services'
        ]);

    angular.module('salang.search.controllers', []);
    angular.module('salang.search.directives', []);
    angular.module('salang.search.services', []);


    searchFeature.run(function($rootScope) {
        $rootScope.query = '';
        $rootScope.originalQuery = '';
        $rootScope.docs = '';
        $rootScope.numResults = '';
        $rootScope.descrip = '';

        $rootScope.langCount = 1;
        $rootScope.fixedLangCount = 0;
        $rootScope.numSearchResults = 10;

        $rootScope.primary = {
            language: ''
        };

        $rootScope.testing = {
            method: 'two'
        };

        $rootScope.collectionSize = {
            english: 0,
            ndebele: 0,
            sotho: 0,
            sesotho: 0,
            swazi: 0,
            tsonga: 0,
            tswana: 0,
            venda: 0,
            xhosa: 0,
            zulu: 0
        }

        //South African language search boolean operators
        $rootScope.searchLanguage = {
            english: false,
            ndebele: false,
            sotho: false,
            sesotho: false,
            swazi: false,
            tsonga: false,
            tswana: false,
            venda: false,
            xhosa: false,
            zulu: false
        };

        switch (localStorage.getItem('NG_TRANSLATE_LANG_KEY')) {
            case 'en':
                //$rootScope.searchLanguage.english = true;
                break;
            case 'nd':
                $rootScope.searchLanguage.ndebele = true;
                break;
            case 'sotho':
                $rootScope.searchLanguage.sotho = true;
                break;
            case 'st':
                $rootScope.searchLanguage.sesotho = true;
                break;
            case 'ss':
                $rootScope.searchLanguage.swazi = true;
                break;
            case 'ts':
                $rootScope.searchLanguage.tsonga = true;
                break;
            case 'tn':
                $rootScope.searchLanguage.tswana = true;
                break;
            case 've':
                $rootScope.searchLanguage.venda = true;
                break;
            case 'xh':
                $rootScope.searchLanguage.xhosa = true;
                break;
            case 'zu':
                $rootScope.searchLanguage.zulu = true;
                break;
            default:
                //$rootScope.searchLanguage.english = true;
                break;
        }



    });


})();