/**
 * Leaderboard
 * Service to handle all requests for the leaderboard
 * @namespace salang.leaderboard.services
 */
(function () {
    'use strict';

    angular
        .module('salang.leaderboard.services')
        .factory('Leaderboard', Leaderboard);

    Leaderboard.$inject = ['$http'];

    /**
     * @namespace Leaderboard
     */
    function Leaderboard($http) {
        /**
         * @name Leaderboard
         * @desc The factory to be returned
         * @memberOf salang.leaderboard.services.Leaderboard
         */
        var Leaderboard = {
            getAllUsers: getAllUsers,
            getTopUsers: getTopUsers
        };

        return Leaderboard;


        /**
         * @name getAllUsers
         * @desc Gets all profiles ranked in descending order
         * @returns {Promise}
         * @memberOf salang.leaderboard.services.Leaderboard
         */
        function getAllUsers() {
            return $http.get('/api/v1/leaderboard/get_all_users/');
        }


        /**
         * @name getTopUsers
         * @desc Gets top 10 profiles ranked in descending order
         * @returns {Promise}
         * @memberOf salang.leaderboard.services.Leaderboard
         */
        function getTopUsers() {
            return $http.get('/api/v1/leaderboard/get_top_users/');
        }


    }
})();
