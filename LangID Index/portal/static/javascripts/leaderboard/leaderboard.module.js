(function () {
    'use strict';
    angular
        .module('salang.leaderboard', [
            'salang.leaderboard.controllers',
            'salang.leaderboard.services'
        ]);

    angular.module('salang.leaderboard.controllers', []);
    angular.module('salang.leaderboard.services', []);
})();
