/**
 * LeaderBoardController
 * Controller for the leaderboard
 * @namespace salang.leaderboard.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.leaderboard.controllers')
        .controller('LeaderboardController', LeaderboardController);

    LeaderboardController.$inject = ['$location', '$routeParams', 'Leaderboard', 'Notification', 'Authentication', '$scope'];

    /**
     * @namespace LeaderboardController
     */
    function LeaderboardController($location, $routeParams, Leaderboard, Notification, Authentication, $scope) {
        var vm = this;
        vm.all_users = undefined;
        vm.authUser = undefined;
        $scope.formData = {};
        $scope.formDataStats = {};


        activate();
        /**
         * @name activate
         * @desc Actions to be performed when this controller is instantiated
         * @memberOf salang.leaderboard.controllers.LeaderboardController
         */
        function activate() {
            //check if user is logged in
            if (Authentication.isAuthenticated()) {
                var username = Authentication.getAuthenticatedAccount().username;
                Authentication.checkProfile(username).then(profileSuccessFn, profileErrorFn);
            }
            else {
                $scope.authUser = false;
            }

            //get all users for the leaderboard
            Leaderboard.getAllUsers().then(getDataSuccessFn, getDataErrorFn);

            /**
             * find what rank the user is
             */
            function findUser(){
                for (var i = 0; i <  vm.all_users.length; i++){
                    if (vm.authUser.id === vm.all_users[i].id){
                        $scope.placement = i+1;
                        $scope.pageListed = Math.floor((i+1)/20)+1;
                        break;
                    }
                }
            }


            /**
             * @name getDataSuccessFn
             * @desc get the data on all users
             */
            function getDataSuccessFn(data, status, headers, config) {
                vm.all_users = data.data;
                // console.log(vm.all_users);
                $scope.no_users = vm.all_users.length;

                if($scope.authUser){
                    findUser();
                }

                var max = vm.all_users.length / 20;
                setPager(max);
                //console.log(max);

                var url = window.location.href;
                var params = url.split('?');

                setActivePager(params[1], max);
                //buildTable(params[1]);
                var mult = params[1];
                var start = 20 * (mult - 1);
                var end = (20 * mult);
                end = Math.min(vm.all_users.length, end);

                $scope.start = start+1;
                $scope.formData.users = vm.all_users.slice(start, end);


            }


            /**
             * @name getDataErrorFn
             * @desc Redirect to index and show error Notification
             */
            function getDataErrorFn(data, status, headers, config) {
                Notification.error('Leaderboard not reached.');
                $location.url('/');

            }

            /**
             * set the pager
             * @param max - max amount
             */
            function setPager(max) {
                var pager = document.getElementById('pager_leaderboard');
                var end = document.getElementById('pager_end_leaderboard');
                //loop through and set the pager
                for (var i = 0; i < max; i++) {
                    var li = document.createElement('li');
                    li.setAttribute('id', 'pager_leaderboard_' + (i + 1));
                    var a = document.createElement('a');
                    a.setAttribute('href', '/leaderboard?' + (i + 1));
                    a.appendChild(document.createTextNode((i + 1)));
                    li.appendChild(a);
                    pager.insertBefore(li, end);
                }
            }

            /**
             * set the active pager
             * @param number - active
             * @param max - max num
             */
            function setActivePager(number, max) {
                var start = document.getElementById('pager_start_leaderboard');
                var end = document.getElementById('pager_end_leaderboard');
                var start_url = document.getElementById('pager_start_a_leaderboard');
                var end_url = document.getElementById('pager_end_a_leaderboard');
                var nextPage = parseInt(number) + 1;
                var previousPage = parseInt(number) - 1;
                //set next and previous links
                start_url.setAttribute('href', 'leaderboard?' + previousPage);
                end_url.setAttribute('href', 'leaderboard?' + nextPage);

                if (max < 1) {
                    max = 1;
                }

                //disable next/prev if at end/beg
                if (number == max) {
                    end.setAttribute('class', 'disabled');
                    end_url.setAttribute('href', '#');
                }
                if (number == 1) {
                    start.setAttribute('class', 'disabled');
                    start_url.setAttribute('href', '#');
                }

                //set selected
                var selected = document.getElementById('pager_leaderboard_' + number);
                selected.setAttribute('class', 'active');
            }

            /**
             * Success function for fetching a profile
             */
            function profileSuccessFn(data, status, headers, config){
                vm.authUser = data.data;
                $scope.authUser = true;
                resolvePoints();
            }

            /**
             * error function for fetching a profile
             */
            function profileErrorFn(data, status, headers, config){
                vm.hide = true;
                Notification.error("Error fetching data");
            }

            /**
             * get all attributes from the profile for the progression part
             */
            function resolvePoints(){
                $scope.formDataStats.level = vm.authUser.level;
                $scope.formDataStats.capmax = vm.authUser.capMax;
                $scope.formDataStats.capmin = vm.authUser.capMin;
                $scope.formDataStats.points = vm.authUser.points;
                $scope.formDataStats.percentage = Math.ceil((vm.authUser.points/vm.authUser.capMax)*100);
                $scope.formDataStats.percentage = Math.ceil((vm.authUser.points/vm.authUser.capMax)*100);
                $scope.formDataStats.pointsLeft = vm.authUser.capMax - vm.authUser.points;
            }

        }
    }
})();
