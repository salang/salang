(function () {
    'use strict';

    angular.module('salang', [
        'salang.config',
        'salang.routes',
        'salang.authentication',
        'salang.layout',
        'salang.profiles',
        'salang.utils',
        'salang.leaderboard',
        'salang.upload',
        'salang.translation',
        'salang.search',
        'salang.document',
        'salang.review',
        'salang.tour',
        'salang.widgets',
        'salang.stats'

    ]);

    angular.module('salang.routes', ['ngRoute']);
    angular.module('salang.config', ['pascalprecht.translate', 'ngCookies', 'ngSanitize', 'facebook']);

})();

angular.module('salang').run(run);

run.$inject = ['$http', 'Tours'];

/**
 * @name run
 * @desc Update xsrf $http headers to align with Django's defaults
 */
function run($http,Tours) {
    $.material.init();
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';

    if (!Tours.checkMainTour()){
        Tours.startMainTour();
    }
}
