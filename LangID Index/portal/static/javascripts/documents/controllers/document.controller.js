/**
 * DocumentController
 * controls the document view
 * @namespace salang.document.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.document.controllers')
        .controller('DocumentController', DocumentController);

    DocumentController.$inject = ['$scope', 'Documents', 'Notification', 'ngDialog', 'Authentication', 'LanguageConverter'];

    /**
     * @namespace DocumentController
     */
    function DocumentController($scope, Documents, Notification, ngDialog, Authentication, LanguageConverter) {
        var vm = this;
        vm.document = undefined;
        vm.reportDoc = reportDoc;

        //get the document id from the url
        var docID = window.location.search.substring(1);
        Documents.getDocument(docID).then(getDocSuccessFn, getDocErrorFn);

        //get the colour for the language tag
        $scope.getLanguageColor = function (long){
            return LanguageConverter.fromLongToShort(long);
        };

        /**
         * reports a document to the backend and shows a notification that it worked
         * @param id
         */
        function reportDoc(id){
            //report the doument
            Documents.reportDoc(id, Authentication.getAuthenticatedAccount().email);
            //show success notification
            $scope.ngDia3 = ngDialog.open({ template : 'static/templates/documents/doc_reported.html' ,
                scope : $scope ,
                className: 'ngdialog-theme-default',
                plain: false,
                showClose: true,
                closeByDocument: true,
                closeByEscape: true,
                appendTo: false,
                disableAnimation: true});
        }



        /**
         * @name getDocSuccessFn
         * @desc got document
         * @memberOf salang.upload.controllers.DocumentController
         */
        function getDocSuccessFn(data, status, headers, config){
            // get all attributes and set them in the scope for the template
            vm.document = data.data;
            $scope.language = vm.document.language;
            $scope.languageName = vm.document.language;
            if ($scope.languageName === 'Sesotho sa Leboa'){
                $scope.languageName = 'Sepedi';
            }
            $scope.title = vm.document.title;
            $scope.author = vm.document.author.username;
            $scope.id = vm.document.id;

            var date = new Date(vm.document.created_at);
            $scope.uploadD = date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear();
            $scope.desc = vm.document.description;
            $scope.docUrl = vm.document.docfile;
            console.log(vm.document.docfile);
        }

        /**
         * @name getDocSuccessFn
         * @desc got doc
         * @memberOf salang.upload.controllers.DocumentController
         */
        function getDocErrorFn(data, status, headers, config){
            console.error('Could not retrieve doc');
            Notification.error("Could not retrieve doc");
        }

    }
})();
