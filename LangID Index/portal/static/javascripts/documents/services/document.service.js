/**
 * Documents
 * Service to offer functions for the documents
 * @namespace salang.document.services
 */
(function () {
    'use strict';

    angular
        .module('salang.document.services')
        .factory('Documents', Documents);

    Documents.$inject = ['$http'];

    /**
     * @namespace Documents
     * @returns {Factory}
     */
    function Documents($http) {
        var Documents = {
            getDocument : getDocument,
            updateViewCounter : updateViewCounter,
            deleteDoc : deleteDoc,
            reportDoc : reportDoc
        };

        return Documents;


        /**
         * @name getDocument
         * @desc get the document from id
         * @param {String} id id
         * @returns {Promise}
         * @memberOf salang.document.services.Documents
         */
        function getDocument(id) {
            return $http.get('/api/v1/documents/get_document?id='+id);
        }

        /**
         * @name updateViewCounter
         * @desc update the counter
         * @param {String} id of the doc
         * @returns {Promise}
         * @memberOf salang.document.services.Documents
         */
        function updateViewCounter(id) {
            return $http.get('/api/v1/documents/update_no_views?id='+id);
        }

        /**
         * @name deleteDoc
         * @desc delete specific document
         * @param {String} id of the doc
         * @returns {Promise}
         * @memberOf salang.document.services.Documents
         */
        function deleteDoc(id) {
            return $http.get('/api/v1/documents/delete_doc?id='+id);
        }

        /**
         * @name reportDoc
         * @desc report specific document
         * @param {String} id of the doc
         * @param {String} user of the doc
         * @returns {Promise}
         * @memberOf salang.document.services.Documents
         */
        function reportDoc(id, user) {
            return $http.get('/api/v1/documents/report_doc?id='+ id + "&username=" + user);
        }

    }
})();