(function () {
    'use strict';
    angular
        .module('salang.document', [
            'salang.document.controllers',
            'salang.document.services'
        ]);

    angular.module('salang.document.controllers', []);
    angular.module('salang.document.services', []);
})();
