/**
 * NavbarController
 * controls the navigation bar
 * @namespace salang.layout.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.layout.controllers')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$scope', '$rootScope', 'Authentication'];

    /**
     * @namespace NavbarController
     */
    function NavbarController($scope, $rootScope, Authentication) {
        var vm = this;
        vm.logout = logout;
        $scope.formData = {};

        //check if the user is authenticated
        if (Authentication.isAuthenticated()){
            vm.profile = Authentication.getAuthenticatedAccount();
            vm.authenticated = true;
        }
        else{
            vm.authenticated = false;
        }

        vm.callResize = callResize;
        window.onload = init();


        var timeoutID = window.setTimeout(callResize, 100);
        if (vm.authenticated){
            var timeoutID2 = window.setTimeout(assignValues, 100);
        }

        /**
         * assign the values to html
         */
        function assignValues() {
            document.getElementById('tour_profile').setAttribute('href', '/+'+vm.profile.username);
            document.getElementById('second_username').appendChild(document.createTextNode(vm.profile.username));
        }
        

        vm.smaller = false;

        /**
         * @name logout
         * @desc Log the user out
         * @memberOf salang.layout.controllers.NavbarController
         */
        function logout() {
            Authentication.logout();
        }


        /**
         * resize the element for the nav bar
         * @param id
         */
        function resizeElement(id){
            var spacer = 150;
            if (vm.smaller){
                spacer = 75;
            }
            var item = $(id);
            var length = item.show().height();
            var res = spacer - length;
            var cssprop = item.css('padding-top');
            var old = cssprop.replace("px", "");
            item.css('margin-top', ((res/2)-old) + 'px');
        }

        /**
         * initialise the scroll header
         */
        function init() {
            window.addEventListener('scroll', function(e){
                var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                    shrinkOn = 100,
                    part1 = $('#logo'),
                    part2 = $('#main_navbar_element');
                if (distanceY > shrinkOn) {
                    part1.addClass('smaller');
                    part2.addClass('smaller');
                    vm.smaller = true;
                    callResize();
                }
                else {
                    vm.smaller = false;
                    part1.removeClass('smaller');
                    part2.removeClass('smaller');
                    callResize();
                }
            });
        }

        /**
         * resizes the elements
         */
        function callResize(){
            resizeElement('#drop_down_1');
            resizeElement('#drop_down_2');
            if (vm.authenticated){
                resizeElement('#logout_btn');
                resizeElement('#tour_profile');
            }
            else{
                resizeElement('#tour_login');
            }
            resizeElement('#tour_language');
        }


    }
})();
