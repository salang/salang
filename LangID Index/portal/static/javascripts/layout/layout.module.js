(function () {
  'use strict';

  angular
    .module('salang.layout', [
      'salang.layout.controllers'
    ]);

  angular.module('salang.layout.controllers', []);
})();
