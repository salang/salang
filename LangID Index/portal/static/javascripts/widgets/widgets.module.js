(function () {
    'use strict';

    angular
        .module('salang.widgets', [
            'salang.widgets.controllers'
        ]);

    angular.module('salang.widgets.controllers', []);
})();
