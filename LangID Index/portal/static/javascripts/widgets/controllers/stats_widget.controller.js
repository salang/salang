/**
 * StatsWidgetController
 * Stats controller for widget
 * @namespace salang.widgets.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.widgets.controllers')
        .controller('StatsWidgetController', StatsWidgetController);

    StatsWidgetController.$inject = ['$scope', 'Notification', 'Statistics'];

    /**
     * @namespace StatsWidgetController
     */
    function StatsWidgetController($scope, Notification, Statistics) {
        var vm = this;

        vm.show = false;

        var handleCount = 0;
        // number of stats defined here
        var portal_stats = 2, language_stats = 3, language_choices = 9;

        //  get 2 portal stats
        Statistics.getRandomPortalStats(portal_stats, language_stats, language_choices).then(successFn, errorFn);

        /**
         *  success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function successFn(data, status, headers, config){
            vm.stats = data.data;
            handleRandomVars();
            //console.log(vm.stats);
        }

        /**
         * error function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function errorFn(data, status, headers, config){
            console.error("Could not get stats widget data!");
            Notification.error("Could not get stats widget data!");
        }

        /**
         * handle the click for languages
         * @param lang
         */
        $scope.handleClick = function (lang) {
            switch (lang){

            }
        };

        /**
         * handle random variables for selection
         */
        function handleRandomVars(){
            for (var i in vm.stats.rands){
                switch (vm.stats.rands[i]){
                    case 0:
                        Statistics.getLanguageCount('isiNdebele').then(ndSuccessFn, langErrorFn);
                        break;
                    case 1:
                        Statistics.getLanguageCount('Sesotho_sa_Leboa').then(slbSuccessFn, langErrorFn);
                        break;
                    case 2:
                        Statistics.getLanguageCount('Sesotho').then(seSuccessFn, langErrorFn);
                        break;
                    case 3:
                        Statistics.getLanguageCount('siSwati').then(siSuccessFn, langErrorFn);
                        break;
                    case 4:
                        Statistics.getLanguageCount('Xitsonga').then(xiSuccessFn, langErrorFn);
                        break;
                    case 5:
                        Statistics.getLanguageCount('Setswana').then(setSuccessFn, langErrorFn);
                        break;
                    case 6:
                        Statistics.getLanguageCount('Tshivenda').then(tshSuccessFn, langErrorFn);
                        break;
                    case 7:
                        Statistics.getLanguageCount('isiXhosa').then(xhSuccessFn, langErrorFn);
                        break;
                    case 8:
                        Statistics.getLanguageCount('isiZulu').then(zuSuccessFn, langErrorFn);
                        break;

                }
            }
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function ndSuccessFn(data, status, headers, config){
            handleResult('nd', data.data.response.numFound);
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function slbSuccessFn(data, status, headers, config){
            handleResult('sotho', data.data.response.numFound);
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function seSuccessFn(data, status, headers, config){
            handleResult('st', data.data.response.numFound);
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function siSuccessFn(data, status, headers, config){
            handleResult('ss', data.data.response.numFound);
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function xiSuccessFn(data, status, headers, config){
            handleResult('ts', data.data.response.numFound);
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function setSuccessFn(data, status, headers, config){
            handleResult('tn', data.data.response.numFound);
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function tshSuccessFn(data, status, headers, config){
            handleResult('ve', data.data.response.numFound);
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function xhSuccessFn(data, status, headers, config){
            handleResult('xh', data.data.response.numFound);
        }

        /**
         * success for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function zuSuccessFn(data, status, headers, config){
            handleResult('zu', data.data.response.numFound);
        }

        /**
         * error for language function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function langErrorFn(data, status, headers, config){
            console.error('cant get stats');
            Notification.error('Error occurred');
        }


        /**
         * handle the language success
         * @param language - language
         * @param amount - amount of docs
         */
        function handleResult(language, amount){
            vm.stats[language] = amount;
            handleCount++;
            if (handleCount == language_stats){
                vm.show  = true;
            }
        }

    }
})();