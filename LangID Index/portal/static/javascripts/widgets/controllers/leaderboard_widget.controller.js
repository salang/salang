/**
 * LeaderboardWidgetController
 * Leaderboard widget controller
 * @namespace salang.widgets.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.widgets.controllers')
        .controller('LeaderboardWidgetController', LeaderboardWidgetController);

    LeaderboardWidgetController.$inject = ['$scope', 'Leaderboard', 'Notification'];

    /**
     * @namespace LeaderboardWidgetController
     */
    function LeaderboardWidgetController($scope, Leaderboard, Notification) {
        var vm = this;

        //console.log(window.location);

        var no_users = 5;

        if (String(window.location).indexOf('/upload') > -1) {
            no_users = 10;
        }


        vm.all_users = undefined;

        //get users for leaderboard
        Leaderboard.getTopUsers().then(successFn, errorFn);

        /**
         * success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function successFn(data, status, headers, config){
            var val = Math.min(no_users,data.data.length);
            //add in all users and their data
            if (val > 0){
                vm.all_users = data.data.slice(0, val);
                vm.all_users[0].position = 1;
                if (val > 2){
                    vm.all_users[2].position = 3;
                    vm.all_users[1].position = 2;
                }
                else if (val > 1){
                    vm.all_users[1].position = 2;
                }
            }
            else{
                vm.empty = true;
            }

            //console.log(vm.all_users);
            vm.show = true;
        }

        /**
         * error function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function errorFn(data, status, headers, config){
            console.error("Could not get leaderboard widget users!");
            Notification.error("Could not get leaderboard widget users!");
        }

    }
})();