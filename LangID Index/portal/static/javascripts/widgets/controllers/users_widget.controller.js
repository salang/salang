/**
 * UserWidgetController
 * Controller for the latest widget user
 * @namespace salang.widgets.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.widgets.controllers')
        .controller('UserWidgetController', UserWidgetController);

    UserWidgetController.$inject = ['$scope', 'Notification', 'Authentication'];

    /**
     * @namespace UserWidgetController
     */
    function UserWidgetController($scope, Notification, Authentication) {
        var vm = this;

        var noUsers = 5;

        vm.users = undefined;

        //get all the new users
        Authentication.getNewUsers(noUsers).then(successFn, errorFn);

        /**
         * success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function successFn(data, status, headers, config){
            vm.users = data.data;
            // loop through users and get their data
            if (vm.users.length > 0){
                var val = Math.min(vm.users.length, noUsers);
                for (var i = 0; i < val; i++){
                    var date = new Date(vm.users[i].created_at);
                    vm.users[i].date = date.getDate() + "/" + (parseInt(date.getMonth())+1) + "/" + String(date.getFullYear()).substring(2,4);
                }
                vm.notempty = true;
            }
            else{
                vm.notempty = false;
            }

            vm.show = true;
        }

        /**
         * error function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function errorFn(data, status, headers, config){
            console.error("Could not get leaderboard widget users!");
            Notification.error("Could not get leaderboard widget users!");
        }

    }
})();