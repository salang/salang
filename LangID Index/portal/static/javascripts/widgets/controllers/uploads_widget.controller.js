/**
 * UploadsWidgetController
 * Upload widget controller
 * @namespace salang.widgets.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.widgets.controllers')
        .controller('UploadsWidgetController', UploadsWidgetController);

    UploadsWidgetController.$inject = ['$scope', 'Notification', 'Uploads', 'LanguageConverter'];

    /**
     * @namespace UploadsWidgetController
     */
    function UploadsWidgetController($scope, Notification, Uploads, LanguageConverter) {
        var vm = this;

        vm.uploads = undefined;
        var docAmount = 5;

        if (String(window.location).indexOf('/upload') > -1) {
            docAmount = 10;
        }

        //get all uploads
        Uploads.all().then(successFn, errorFn);

        /**
         * success for all uploads
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function successFn(data, status, headers, config){
            // console.log(data.data);
            var val = Math.min(data.data.length, docAmount);
            vm.uploads = data.data.slice(0,val);
            if (data.data.length === 0){
                vm.nodata = true;
            }
            else{
                //go through all documents and get the data from them
                for (var i = 0; i < val; i++){
                    if(vm.uploads[i].title.length > 15) {
                        vm.uploads[i].title = vm.uploads[i].title.substring(0,13) + "...";
                    }
                    if (vm.uploads[i].language === 'Sesotho sa Leboa'){
                        vm.uploads[i].languageName = 'Sepedi';
                    }
                    else{
                        vm.uploads[i].languageName = vm.uploads[i].language;
                    }
                }
            }
            vm.show = true;
        }

        /**
         * error function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function errorFn(data, status, headers, config){
            console.error("Could not get leaderboard widget users!");
            Notification.error("Could not get leaderboard widget users!");
        }

        //get the colour for the language
        $scope.getLanguageColor = function (long){
            return LanguageConverter.fromLongToShort(long);
        };

    }
})();