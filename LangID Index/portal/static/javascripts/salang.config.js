(function () {
    'use strict';

    angular.module('salang.config').config(config);

    config.$inject = ['$locationProvider', '$translateProvider', 'FacebookProvider'];

    /**
    * @name config
    * @desc Enable HTML5 routing
    */
    function config($locationProvider, $translateProvider, FacebookProvider) {
        //html5 routing and trailing /'s
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');

        //translations
        $translateProvider
            .useStaticFilesLoader({
                prefix: '/static/translations/',
                suffix: '.json'
            })
            .preferredLanguage('en')
            .useLocalStorage()
            .useMissingTranslationHandlerLog()
            .useSanitizeValueStrategy('sanitize');

        //init facebook with id
        FacebookProvider.init('278157815882807');


    }
})();
