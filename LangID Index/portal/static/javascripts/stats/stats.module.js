(function () {
    'use strict';

    angular
        .module('salang.stats', [
            'salang.stats.controllers',
            'salang.stats.service'
        ]);

    angular.module('salang.stats.controllers', []);
    angular.module('salang.stats.service', []);
})();
