/**
 * Statistics
 * Service for the statistics of the portal
 * @namespace salang.stats.services
 */
(function ($, _) {
    'use strict';

    angular
        .module('salang.utils.services')
        .factory('Statistics', Statistics);

    Statistics.$inject = ['$http'];

    /**
     * @namespace Statistics
     */
    function Statistics($http) {
        /**
         * @name Statistics
         * @desc The factory to be returned
         */
        var Statistics = {
            getPortalStats : getPortalStats,
            getRandomPortalStats : getRandomPortalStats,
            getLanguageCount : getLanguageCount,
            getAllLanguageCounts : getAllLanguageCounts
        };

        return Statistics;


        /**
         * @name getPortalStats
         * @desc report specific document
         * @returns {Promise}
         * @memberOf salang.document.services.Statistics
         */
        function getPortalStats() {
            return $http.get('/api/v1/stats/get_stats');
        }

        /**
         * @name getRandomPortalStats
         * @desc report specific document
         * @returns {Promise}
         * @memberOf salang.document.services.Statistics
         */
        function getRandomPortalStats(amount, rand, samples) {
            return $http.get('/api/v1/stats/get_random_stats?no='+ amount + "&no_random=" + rand + "&samples=" + samples);
        }


        /**
         * Get the count of resources for a language
         * @param lang - which language
         * @returns {the response from solr}
         */
        function getLanguageCount(lang){
            var solrHead = 'http://',
                // ip = 'localhost',
                ip = '137.158.60.198',
                port = ':8983',
                solrMid = '/solr/',
                solrTail = '/select';
            var name = undefined;

            return $http({
                method: 'JSONP',
                url: solrHead + ip + port + solrMid + lang + solrTail,
                params: {
                    'fl': 'title',
                    'rows': 1,
                    'json.wrf': 'JSON_CALLBACK',
                    'wt': 'json',
                    'q': '*:*'
                }
            });
        }

        /**
         * Get the count of resources for all language
         * @returns {{total: number}}
         */
        function getAllLanguageCounts(){
            var solrHead = 'http://',
                // ip = 'localhost',
                ip = '137.158.60.215',
                port = ':8983',
                solrMid = '/solr/',
                solrTail = '/select';

            var counters = {
                total : 0
            };

            var languages = ["isiNdebele", "Sesotho_sa_Leboa", "Sesotho",
                "siSwati", "Xitsonga", "Setswana", "Tshivenda", "isiXhosa", "isiZulu"];

            var total = 0;
            var i;
            for  (i in languages){

                $http({
                    method: 'JSONP',
                    url: solrHead + ip + port + solrMid + languages[i] + solrTail,
                    params: {
                        'fl': 'title',
                        'rows': 1,
                        'json.wrf': 'JSON_CALLBACK',
                        'wt': 'json',
                        'q': '*:*'
                    }
                }).success(function(data) {
                    console.log(data);
                    counters[languages[i]] = data.response.numFound;
                    total += data.response.numFound;
                });

            }
            counters.total = total;
            return counters;
        }
    }
})($, _);
