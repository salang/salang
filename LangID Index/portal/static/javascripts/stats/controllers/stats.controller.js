/**
 * StatsController
 * Controller for the portal statistsics
 * @namespace salang.stats.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.stats.controllers')
        .controller('StatsController', StatsController);

    StatsController.$inject = ['$scope', 'Notification', 'Statistics'];

    /**
     * @namespace StatsController
     */
    function StatsController($scope, Notification, Statistics) {
        var vm = this;

        vm.formData = {
            total: 0
        };

        var handleCount = 0;

        //get the stats from the backend and the language resources from solr
        Statistics.getPortalStats().then(statsSuccessFn, statsErrorFn);
        Statistics.getLanguageCount('isiNdebele').then(ndSuccessFn, langErrorFn);
        Statistics.getLanguageCount('Sesotho_sa_Leboa').then(slbSuccessFn, langErrorFn);
        Statistics.getLanguageCount('Sesotho').then(seSuccessFn, langErrorFn);
        Statistics.getLanguageCount('siSwati').then(siSuccessFn, langErrorFn);
        Statistics.getLanguageCount('Xitsonga').then(xiSuccessFn, langErrorFn);
        Statistics.getLanguageCount('Setswana').then(setSuccessFn, langErrorFn);
        Statistics.getLanguageCount('Tshivenda').then(tshSuccessFn, langErrorFn);
        Statistics.getLanguageCount('isiXhosa').then(xhSuccessFn, langErrorFn);
        Statistics.getLanguageCount('isiZulu').then(zuSuccessFn, langErrorFn);


        /**
         * success function for stats
         * @param data
         * @param status
         * @param headers
         * @param config
         */
       function statsSuccessFn(data, status, headers, config){
            // console.log(data.data);
            vm.portalStats = data.data;
        }

        /**
         * error function for stats
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function statsErrorFn(data, status, headers, config){
            console.error('cant get stats');
            Notification.error('Error occurred');
        }

        /**
         * language success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function ndSuccessFn(data, status, headers, config){
            handleResult('nd', data.data.response.numFound);
        }


        function slbSuccessFn(data, status, headers, config){
            handleResult('sotho', data.data.response.numFound);
        }

        /**
         * language success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function seSuccessFn(data, status, headers, config){
            handleResult('st', data.data.response.numFound);
        }

        /**
         * language success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function siSuccessFn(data, status, headers, config){
            handleResult('ss', data.data.response.numFound);
        }

        /**
         * language success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function xiSuccessFn(data, status, headers, config){
            handleResult('ts', data.data.response.numFound);
        }

        /**
         * language success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function setSuccessFn(data, status, headers, config){
            handleResult('tn', data.data.response.numFound);
        }

        /**
         * language success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function tshSuccessFn(data, status, headers, config){
            handleResult('ve', data.data.response.numFound);
        }

        /**
         * language success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function xhSuccessFn(data, status, headers, config){
            handleResult('xh', data.data.response.numFound);
        }

        /**
         * language success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function zuSuccessFn(data, status, headers, config){
            handleResult('zu', data.data.response.numFound);
        }

        /**
         * error for any of the languages
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function langErrorFn(data, status, headers, config){
            console.error('cant get stats');
            Notification.error('Error occurred');
        }

        /**
         * handle the result for a langauge and handle the stats
         * @param language
         * @param amount
         */
        function handleResult(language, amount){
            vm.formData[language] = amount;
            vm.formData.total += amount;
            handleCount++;
            if (handleCount == 8){
                vm.showPortalStats  = true;
                vm.showLanguageStats = true;
            }
        }

    }
})();