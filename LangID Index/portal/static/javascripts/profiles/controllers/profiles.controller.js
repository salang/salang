
/**
 * ProfileController
 * Controls the profile and the documents
 * @namespace salang.profiles.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.profiles.controllers')
        .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['$scope', '$location', '$routeParams', 'Authentication', 'Uploads', 'ngDialog', 'Documents', 'LanguageConverter', 'Notification', 'Leaderboard'];

    /**
     * @namespace ProfileController
     */
    function ProfileController($scope, $location, $routeParams, Authentication, Uploads, ngDialog, Documents, LanguageConverter, Notification, Leaderboard) {
        var vm = this;

        vm.profile = undefined;
        vm.uploads = undefined;

        vm.delete = false;

        //set scope variables
        $scope.clickToOpen = clickToOpen;
        $scope.deleteDoc = deleteDoc;
        $scope.updateDocDetails = updateDocDetails;
        $scope.requestChange = requestChange;
        $scope.changePassword = changePassword;


        var searchedUsername = $routeParams.username.substr(1);
        var currentUser = Authentication.getAuthenticatedAccount();
        var currentUserID = Authentication.getAuthenticatedAccount().id;

        $scope.formDataDocs = {};
        $scope.formDataPassword = {};
        $scope.formDataStats = {};





        //first we need to check if we are here from own route or searched
        if (currentUser.username == searchedUsername){
            Authentication.checkProfile(currentUser.username).then(profileSuccessFn, profileErrorFn);
        }
        else{
            $location.url('/profile/+' + searchedUsername);
        }

        /**
         * @name profileSuccessProfile
         * @desc Update `profile` on viewmodel
         */
        function profileSuccessFn(data, status, headers, config) {
            vm.profile = data.data;
            //console.log(vm.profile);
            $scope.formDataStats.views = vm.profile.views;
            $scope.formDataStats.uploads = vm.profile.doc_uploads;
            $scope.formDataStats.reviews = vm.profile.reviews;
            $scope.formDataStats.points = vm.profile.points;
            $scope.formDataStats.level = vm.profile.level;
            $scope.formDataStats.pointsLeft = vm.profile.capMax - vm.profile.points;
            $scope.formDataStats.capmax = vm.profile.capMax;
            $scope.formDataStats.capmin = vm.profile.capMin;
            $scope.formDataStats.percentage = Math.ceil((vm.profile.points/vm.profile.capMax)*100);
            //for rank
            Leaderboard.getAllUsers().then(getDataSuccessFn, getDataErrorFn);
            Uploads.getUserUploads(currentUserID).then(uploadsSuccessFn,uploadsErrorFn);
        }


        /**
         * @name profileErrorFn
         * @desc Redirect to index and show error Snackbar
         */
        function profileErrorFn(data, status, headers, config) {
            $location.url('/');
            Notification.error('That user does not exist.');
        }


        $scope.getLanguageColor = function (long){
            return LanguageConverter.fromLongToShort(long);
        };


        /**
         * @name uploadsSuccessFn
         * @desc Update `profile` on viewmodel
         */
        function uploadsSuccessFn(data, status, headers, config) {
            vm.hasDocs = true;
            vm.uploads = data.data;


            var content = [];

            for (var i = 0; i < vm.uploads.length; i++){
                var newitem = {
                    title: vm.uploads[i].title,
                    language: vm.uploads[i].language,
                    description: vm.uploads[i].description,
                    id: vm.uploads[i].id
                };
                content.push(newitem);
            }

            $scope.formDataDocs.uploads = content;

        }


        /**
         * @name uploadsErrorFn
         * @desc Redirect to index and show error Snackbar
         */
        function uploadsErrorFn(data, status, headers, config) {
            vm.hasDocs = false;
        }


        /**
         * handles the clicking of documents
         * @param choice - what was clicked
         * @param id - doc id
         */
        function clickToOpen(choice, id) {
            switch(choice){
                case 0:
                    if ($scope.editBtn || $scope.delBtn){
                        $scope.editBtn = false;
                        $scope.delBtn = false;
                        break;
                    }
                    $location.url('/docs?'+id);
                    break;
                case 1:
                    vm.editDocId = id;
                    handleEditCall();
                    break;
                case 2:
                    vm.deleteDocId = id;
                    handleDeleteCall();
                    break;
            }
        }


        /**
         * if the edit button was pressed
         */
        function handleEditCall(){
            //get the document and show window
            $scope.editBtn = true;
            Documents.getDocument(vm.editDocId).then(getDocSuccess, getDocError);

        }

        /**
         * if the doc was retrieved
         * @param data - document
         */
        function getDocSuccess(data, status, headers, config){
            $scope.editUpload = data.data;
            //show dialogue to edit the doc
            $scope.ngDia2 = ngDialog.open({ template : 'static/templates/documents/edit_doc.html' ,
                scope : $scope ,
                className: 'ngdialog-theme-default',
                plain: false,
                showClose: true,
                closeByDocument: true,
                closeByEscape: true,
                appendTo: false,
                disableAnimation: true});
        }

        /**'
         * error function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function getDocError(data, status, headers, config){
            console.error("can't edit doc");
            Notification.error("Unable to edit document");
        }


        /**
         * updates the document with the new values
         */
        function updateDocDetails(){
            //update th document with the new details
            Uploads.updateDocumentDetails($scope.editUpload.id, $scope.editUpload.language, $scope.editUpload.title, $scope.editUpload.description).then(editSuccessFn, editErrorFn);
        }

        /**
         * error function
         */
        function editErrorFn(data, status, headers, config){
            console.error('Could not edit document.');
            Notification.error('Could not edit document.');

        }

        /**
         * successfn
         */
        function editSuccessFn(data, status, headers, config){
            //close the dialogue
            $scope.ngDia2.close();
            Notification.success("Edit successful!");
            location.reload(false);
        }

        /**
         * if a doc is to be deleted
         */
        function handleDeleteCall(){
            $scope.delBtn = true;
            //show success message after confirmation messsage
            $scope.ngDia = ngDialog.open({ template : 'static/templates/documents/delete_doc.html' ,
                scope : $scope ,
                className: 'ngdialog-theme-default',
                plain: false,
                showClose: true,
                closeByDocument: true,
                closeByEscape: true,
                appendTo: false,
                disableAnimation: true});
        }


        /**
         * delete the document
         */
        function deleteDoc(){
            Documents.deleteDoc(vm.deleteDocId).then(onDeleteSuccessFn, onDeleteErrorFn);
        }

        /**
         * success function
         */
        function onDeleteSuccessFn(data, status, headers, config){
            $scope.ngDia.close();
            location.reload(false);
        }

        /**
         *  error function
         */
        function onDeleteErrorFn(data, status, headers, config){
            console.error('Could not delete document.');
            Notification.error('Could not delete document.');

        }

        /**
         * chnage the password
         */
        function changePassword(){
            //open dialogue for changing a password
            $scope.ngDia4 = ngDialog.open({ template : 'static/templates/profiles/change_password.html' ,
                scope : $scope ,
                className: 'ngdialog-theme-default',
                plain: false,
                showClose: true,
                closeByDocument: true,
                closeByEscape: true,
                appendTo: false,
                disableAnimation: true});
        }

        /**
         * password change request
         */
        function requestChange(){
            Authentication.changePassword(Authentication.getAuthenticatedAccount().id, $scope.formDataPassword.passwordOld, $scope.formDataPassword.password1, $scope.formDataPassword.password2).then(passwordSuccessFn, passwordErrorFn);
        }

        /**
         * success function
         */
        function passwordSuccessFn(data, status, headers, config){
            console.log('success');
        }

        /**
         * error function
         */
        function passwordErrorFn(data, status, headers, config){
            console.error(data.data.message);
            Notification.error("Error changing password.");
        }


        /**
         * find the users rank
         */
        function findUser(){
            for (var i = 0; i <  vm.all_users.length; i++){
                if (currentUserID === vm.all_users[i].id){
                    $scope.formDataStats.placement = i+1;
                    break;
                }
            }
        }


        /**
         * @name getDataSuccessFn
         * @desc get the data on all users
         */
        function getDataSuccessFn(data, status, headers, config) {
            vm.all_users = data.data;
            findUser();
        }


        /**
         * @name getDataErrorFn
         * @desc Redirect to index and show error Notification
         */
        function getDataErrorFn(data, status, headers, config) {
            Notification.error('Rank not reached.');

        }


    }

})();