/**
 * ProfileLanguageController
 * Controller for the languages on the profile
 * @namespace salang.profiles.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.profiles.controllers')
        .controller('ProfileLanguageController', ProfileLanguageController);

    ProfileLanguageController.$inject = ['$scope', 'Notification', 'Authentication'];

    /**
     * @namespace ProfileLanguageController
     */
    function ProfileLanguageController($scope, Notification, Authentication) {
        var vm = this;
        vm.updateLanguages = updateLanguages;
        vm.langs = undefined;
        var authenticatedAccount = Authentication.getAuthenticatedAccount();

        //get the languages for a profile
        Authentication.getLangs(authenticatedAccount.id).then(successFn, errorFn);

        /**
         * success function for getting languages
         */
        function successFn(data, status, headers, config){
            //console.log(data.data);
            vm.langs = data.data;
            getLanguages();
        }

        /**
         * success function for getting languages
         */
        function errorFn(data, status, headers, config){
            console.log("Error getting languages for user");
            Notification.error("Error getting languages for user");
        }

        /**
         * get all languages
         */
        function getLanguages(){
            var size = vm.langs.length;
            var id = undefined;
            for (var i = 0; i < size; i++){
                switch (vm.langs[i].short){
                    case 'zu':
                        $scope.zu_check = true;
                        break;
                    case 'xh':
                        $scope.xh_check = true;
                        break;
                    case 'nd':
                        $scope.nd_check = true;
                        break;
                    case 'ss':
                        $scope.si_check = true;
                        break;
                    case 'sotho':
                        $scope.so_check = true;
                        break;
                    case 'st':
                        $scope.sotho_check = true;
                        break;
                    case 'tn':
                        $scope.tswana_check = true;
                        break;
                    case 've':
                        $scope.ve_check = true;
                        break;
                    case 'ts':
                        $scope.ts_check = true;
                        break;
                }
            }
        }

        /**
         * update the languages
         */
        function updateLanguages(){
            var langs = '{';
            var counter = 0;
            if ($scope.zu_check){
                langs += '"' + String(counter) + '" : { "name": "isiZulu", "short": "zu" },';
                counter++;
            }
            if ($scope.xh_check){
                langs += '"' + String(counter) + '" : { "name": "isiXhosa", "short": "xh" },';
                counter++;
            }
            if ($scope.nd_check){
                langs += '"' + String(counter) + '" : { "name": "isiNdebele", "short": "nd" },';
                counter++;
            }
            if ($scope.si_check){
                langs += '"' + String(counter) + '" : { "name": "siSwati", "short": "ss" },';
                counter++;
            }
            if ($scope.so_check){
                langs += '"' + String(counter) + '" : { "name": "Sesotho", "short": "sotho" },';
                counter++;
            }
            if ($scope.sotho_check){
                langs += '"' + String(counter) + '" : { "name": "Sesotho sa Leboa", "short": "st" },';
                counter++;
            }
            if ($scope.tswana_check){
                langs += '"' + String(counter) + '" : { "name": "Setswana", "short": "tn" },';
                counter++;
            }
            if ($scope.ve_check){
                langs += '"' + String(counter) + '" : { "name": "Tshivenda", "short": "ve" },';
                counter++;
            }
            if ($scope.ts_check){
                langs += '"' + String(counter) + '" : { "name": "Xitsonga", "short": "ts" },';
                counter++;
            }
            langs = langs.substring(0,langs.length-1) +  '}';
            var obj = JSON.parse(langs);
            //send request to update languages
            Authentication.updateLangs(authenticatedAccount.id, counter, obj).then(langSuccessFn, langErrorFn);
        }

        /**
         * success function for setting languages
         */
        function langSuccessFn(data, status, headers, config){
            console.log("success");
            Notification.success("Language preferences updated!");
        }

        /**
         * error function for setting languages
         */
        function langErrorFn(data, status, headers, config){
            console.error("update failed");
            Notification.error("update failed!");
        }

    }
})();
