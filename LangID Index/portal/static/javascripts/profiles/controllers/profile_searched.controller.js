
/**
 * ProfileSearchController
 * Controller to handle a searched profile
 * @namespace salang.profiles.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.profiles.controllers')
        .controller('ProfileSearchController', ProfileSearchController);

    ProfileSearchController.$inject = ['$scope', '$location', '$routeParams', 'Notification', 'Authentication', 'Uploads', 'LanguageConverter', 'Leaderboard'];

    /**
     * @namespace ProfileSearchController
     */
    function ProfileSearchController($scope, $location, $routeParams, Notification, Authentication, Uploads, LanguageConverter, Leaderboard) {
        var vm = this;
        vm.hasDocs = true;
        vm.searchedUsername = $routeParams.username.substr(1);

        //check if profile exists
        Authentication.checkProfile(vm.searchedUsername).then(searchSuccessFn,searchErrorFn);
        $scope.formDataDocs = {};
        $scope.formDataStats = {};



        /**
         * @name searchSuccessFn
         * @desc Update `profile` on viewmodel
         */
        function searchSuccessFn(data, status, headers, config) {
            // console.log('here2');
            vm.profile = data.data;
            $scope.formDataStats.views = vm.profile.views;
            $scope.formDataStats.uploads = vm.profile.doc_uploads;
            $scope.formDataStats.reviews = vm.profile.reviews;
            $scope.formDataStats.points = vm.profile.points;
            $scope.formDataStats.level = vm.profile.level;
            //for rank
            Leaderboard.getAllUsers().then(getDataSuccessFn, getDataErrorFn);
            Uploads.getUserUploads(vm.profile.id).then(uploadSuccessFn, uploadErrorFn);
        }

        /**
         * get the colour for the language
         * @param long
         * @returns {*}
         */
        $scope.getLanguageColor = function (long){
            return LanguageConverter.fromLongToShort(long);
        };

        /**
         * @name searchErrorFn
         * @desc Redirect to index and show error Notification
         */
        function searchErrorFn(data, status, headers, config) {
            $location.url('/');
            Notification.error('User does not exist.');
        }



        /**
         * @name uploadSuccessFn
         * @desc Update `profile` on viewmodel
         */
        function uploadSuccessFn(data, status, headers, config) {
            vm.uploads = data.data;
            // console.log('here3');
            var content = [];

            for (var i = 0; i < vm.uploads.length; i++){
                var newitem = {
                    title: vm.uploads[i].title,
                    language: vm.uploads[i].language,
                    description: vm.uploads[i].description,
                    id: vm.uploads[i].id
                };
                content.push(newitem);
            }

            $scope.formDataDocs.uploads = content;
        }


        /**
         * @name uploadErrorFn
         * @desc Redirect to index and show error Notification
         */
        function uploadErrorFn(data, status, headers, config) {
            //$location.url('/');
            // console.log("upload error");
            //Notification.error('Data unreachable.');
            vm.hasDocs = false;
            $scope.$apply();
        }


        /**
         * find the rank for the user
         */
        function findUser(){
            for (var i = 0; i <  vm.all_users.length; i++){
                if (vm.profile.id === vm.all_users[i].id){
                    $scope.formDataStats.placement = i+1;
                    break;
                }
            }
        }


        /**
         * @name getDataSuccessFn
         * @desc get the data on all users
         */
        function getDataSuccessFn(data, status, headers, config) {
            vm.all_users = data.data;
            findUser();
        }


        /**
         * @name getDataErrorFn
         * @desc Redirect to index and show error Notification
         */
        function getDataErrorFn(data, status, headers, config) {
            Notification.error('Rank not reached.');

        }



    }
})();