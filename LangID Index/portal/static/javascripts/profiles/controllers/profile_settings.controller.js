/**
 * ProfileSettingsController
 * conrtoller for the settings tab on a profile
 * @namespace salang.profiles.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.profiles.controllers')
        .controller('ProfileSettingsController', ProfileSettingsController);

    ProfileSettingsController.$inject = ['$location', '$routeParams', 'Authentication', 'Profile', 'Notification'];

    /**
     * @namespace ProfileSettingsController
     */
    function ProfileSettingsController($location, $routeParams, Authentication, Profile, Notification) {
        var vm = this;

        vm.destroy = destroy;
        vm.update = update;


        vm.authenticatedAccount = Authentication.getAuthenticatedAccount();
        var username = $routeParams.username.substr(1);
        // Redirect if not logged in
        if (!vm.authenticatedAccount) {
            Notification.error("Please sign in");
            $location.url('/login');

        } else {
            // Redirect if logged in, but not the owner of this profile.
            if (vm.authenticatedAccount.username !== username) {
                Notification.error("Not authorized.");
                $location.url('/');
            }
        }



        /**
         * @name destroy
         * @desc Destroy this user's profile
         * @memberOf salang.profiles.controllers.ProfileSettingsController
         */
        function destroy() {
            Profile.destroy(vm.authenticatedAccount.username).then(profileSuccessFn, profileErrorFn);
            /**
             * @name profileSuccessFn
             * @desc Redirect to index and display success snackbar
             */
            function profileSuccessFn(data, status, headers, config) {
                Authentication.unauthenticate();
                Notification.success("Account deleted.");
                $location.url('/');
            }


            /**
             * @name profileErrorFn
             * @desc Display error snackbar
             */
            function profileErrorFn(data, status, headers, config) {
                Notification.error("Error deleting account.");
            }
        }


        /**
         * @name update
         * @desc Update this user's profile
         * @memberOf salang.profiles.controllers.ProfileSettingsController
         */
        function update() {
            Profile.update(vm.authenticatedAccount.id, vm.authenticatedAccount.name).then(profileSuccessFn, profileErrorFn);

            /**
             * @name profileSuccessFn
             * @desc Show success snackbar
             */
            function profileSuccessFn(data, status, headers, config) {
                Authentication.setAuthenticatedAccount(data.data);
                Notification.success("Profile updated.");
            }


            /**
             * @name profileErrorFn
             * @desc Show error snackbar
             */
            function profileErrorFn(data, status, headers, config) {
                Notification.error(data.error);
            }
        }
    }
})();
