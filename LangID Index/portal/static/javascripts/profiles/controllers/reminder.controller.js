/**
 * ReminderController
 * for the reminder banner on the front page
 * @namespace salang.profiles.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.profiles.controllers')
        .controller('ReminderController', ReminderController);


    ReminderController.$inject = ['$scope', 'Authentication', 'Notification'];

    /**
     * @namespace ProfileController
     */
    function ReminderController($scope, Authentication, Notification) {
        var vm = this;

        //rewards
        var REWARD_REVIEW = 15, REWARD_UPLOAD = 60;

        //check if the user is logged in
        if (Authentication.isAuthenticated()){
            var username = Authentication.getAuthenticatedAccount().username;
            Authentication.checkProfile(username).then(profileSuccessFn, profileErrorFn);
        }
        else{
            profileErrorFn();
        }


        /**
         * success function
         */
        function profileSuccessFn(data, status, headers, config){
            vm.profile = data.data;
            vm.hide = false;
            resolvePoints();
        }

        /**
         * error function
         */
        function profileErrorFn(data, status, headers, config){
            vm.hide = true;
        }

        /**
         * resolve the points that need to be earned for next level
         */
        function resolvePoints(){
            var diff = vm.profile.capMax - vm.profile.points;
            vm.pointsLeft = diff;
            vm.reviewNo = Math.ceil(diff/REWARD_REVIEW);
            vm.uploadNo = Math.ceil(diff/REWARD_UPLOAD);
            vm.nextLevel = vm.profile.level+1;
            console.log(vm.nextLevel);
            console.log(vm.pointsLeft);
        }

    }

})();