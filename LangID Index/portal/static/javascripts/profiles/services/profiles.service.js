/**
 * Profile
 * @namespace salang.profiles.services
 */
(function () {
    'use strict';

    angular
        .module('salang.profiles.services')
        .factory('Profile', Profile);

    Profile.$inject = ['$http'];

    /**
     * @namespace Profile
     */
    function Profile($http) {
        /**
         * @name Profile
         * @desc The factory to be returned
         * @memberOf salang.profiles.services.Profile
         */
        var Profile = {
            destroy: destroy,
            get: get,
            update: update
        };

        return Profile;

        /////////////////////

        /**
         * @name destroy
         * @desc Destroys the given profile
         * @param {Object} username The profile to be destroyed
         * @returns {Promise}
         * @memberOf salang.profiles.services.Profile
         */
        function destroy(username) {
            return $http.delete('/api/v1/accounts/' + username + '/');
        }

        /////////////////////

        /**
         * @name get
         * @desc Gets the profile for user with username `username`
         * @param {string} username The username of the user to fetch
         * @returns {Promise}
         * @memberOf salang.profiles.services.Profile
         */
        function get(username) {
            return $http.get('/api/v1/accounts/' + username + '/');
        }



        /////////////////////

        /**
         * @name update
         * @desc Update the given profile
         * @param {Object} id The profile to be updated
         * @param {Object} name The profile name to be updated
         * @returns {Promise}
         * @memberOf salang.profiles.services.Profile
         */
        function update(id, name) {
            return $http.get('/api/v1/accounts/changeName?id=' + id + "&name=" + name);

        }


    }
})();
