(function () {
    'use strict';
    angular
        .module('salang.profiles', [
            'salang.profiles.controllers',
            'salang.profiles.services'
        ]);

    angular.module('salang.profiles.controllers', ['ngDialog']);
    angular.module('salang.profiles.services', []);
})();
