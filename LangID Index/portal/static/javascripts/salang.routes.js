(function() {
    'use strict';

    angular.module('salang.routes').config(config);

    config.$inject = ['$routeProvider'];


    /**
     * @name config
     * routing table for the html docs and controllers
     * @desc Define valid application routes
     */
    function config($routeProvider) {
        $routeProvider
            .when('/review', {
                controller: 'ReviewLanguageController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/reviews/doc_review.html'
            })
            .when('/about', {
                templateUrl: '/static/templates/legal/about_us.html'
            })
            .when('/lang/nd', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/nd.html'
            })
            .when('/lang/sotho', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/sotho.html'
            })
            .when('/lang/ss', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/ss.html'
            })
            .when('/lang/st', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/st.html'
            })
            .when('/lang/tn', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/tn.html'
            })
            .when('/lang/ts', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/ts.html'
            })
            .when('/lang/ve', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/ve.html'
            })
            .when('/lang/xh', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/xh.html'
            })
            .when('/lang/zu', {
                controller: 'resultsController',
                templateUrl: '/static/templates/search_feature/zu.html'
            })
            .when('/register', {
                controller: 'LoginController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/authentication/register.html'
            })
            .when('/login', {
                controller: 'LoginController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/authentication/login.html'
            })
            .when('/leaderboard', {
                controller: 'LeaderboardController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/gamification/leaderboard.html'
            })
            .when('/results', {
                templateUrl: 'static/templates/search_feature/search_bar.html'
            })
            .when('/docs', {
                controller: 'DocumentController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/documents/doc_view.html'
            })
            .when('/upload', {
                controller: 'FileUploadController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/file_upload/upload_doc.html'
            })
            .when('/recent_uploads', {
                controller: 'RecentController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/file_upload/upload_recent.html'
            })
            .when('/browse/language', {
                templateUrl: '/static/templates/search_feature/language_search.html'
            })
            .when('/upload_success', {
                controller: 'FUSuccessController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/file_upload/upload_success.html'
            })
            .when('/stats', {
                controller: 'StatsController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/stats/stats.html'
            })
            .when('/tos', {
                templateUrl: '/static/templates/legal/ToS.html'
            })
            .when('/pp', {
                templateUrl: '/static/templates/legal/privacy.html'
            })
            .when('/review_success',{
                controller:'FUSuccessController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/reviews/review_success.html'
            })
            .when('/profiles/+:username', {
                controller: "ProfileSearchController",
                controllerAs: 'vm',
                templateUrl: '/static/templates/profiles/profile_searched.html'
            })
            .when('/+:username', {
                controller: 'ProfileController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/profiles/profile.html'
            })
            .when('/', {
                templateUrl: 'static/templates/search_feature/search_bar.html'
            })
            .otherwise('/');
        //NOTE: make sure that the username link is last before /
    }
})();

