/**
 * HelpButtonController
 * Controller fro the help button
 * @namespace salang.tour.controllers
 */
(function() {
    'use strict';

    angular
        .module('salang.tour.controllers')
        .controller('HelpButtonController', HelpButtonController);

    HelpButtonController.$inject = ['$scope', 'Tours'];

    /**
     * @namespace HelpButtonController
     */
    function HelpButtonController($scope, Tours) {
        var vm = this;

        vm.help = help;
        /**
         * start the main tour
         */
        function help(){
            $('#drop_down_1').dropdown();
            Tours.startMainTour();
        }


    }
})();