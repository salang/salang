/**
 * Tours
 * Service for the tours
 * @namespace salang.tour.services
 */
(function ($, _) {
    'use strict';

    angular
        .module('salang.tour.services')
        .factory('Tours', Tours);

    Tours.$inject = ['$http'];

    /**
     * @namespace Tours
     */
    function Tours($http) {
        /**
         * @name Tours
         * @desc The factory to be returned
         */
        var Tours = {
            getTourLanguage: getTourLanguage,
            checkMainTour: checkMainTour,
            checkUploadTour: checkUploadTour,
            checkReviewTour: checkReviewTour,
            setTourValue: setTourValue,
            getTourValue: getTourValue,
            startMainTour: startMainTour,
            startUploadP1Tour: startUploadP1Tour,
            startUploadP2Tour: startUploadP2Tour,
            startReviewTour: startReviewTour
        };

        return Tours;


        /**
         * checks the main tour variable
         * @returns {variable}
         */
        function checkMainTour(){
            return checkTour('tour_main');
        }

        /**
         * checks the upload tour variable
         * @returns {variable}
         */
        function checkUploadTour(){
            return checkTour('tour_upload');
        }

        /**
         * checks the review tour variable
         * @returns {variable}
         */
        function checkReviewTour(){
            return checkTour('tour_review');
        }

        /**
         * checks the tour for a certain variable
         * @returns {variable}
         */
        function checkTour(name){
            return localStorage.getItem(name);
        }

        /**
         * checks the language for a tour
         * @returns {variable}
         */
        function getTourLanguage(){
            return localStorage.getItem('NG_TRANSLATE_LANG_KEY');
        }

        /**
         * set the language for a tour to start
         * @returns {variable}
         */
        function setTourValue(name, value){
            localStorage.setItem(name, value);
        }

        /**
         * get a value for a tour
         * @returns {variable}
         */
        function getTourValue(name){
            return localStorage.getItem(name);
        }

        /**
         * start the main tour
         */
        function startMainTour(){
            var lang = getTourLanguage();
            switch (lang){
                case 'en':
                    hopscotch.startTour(tour_main_en);
                    break;
                case 'zu':
                    hopscotch.startTour(tour_main_zu);
                    break;
            }
            setTourValue('tour_main', true);
        }

        /**
         * start the upload tour part 1
         */
        function startUploadP1Tour(){
            var lang = getTourLanguage();
            switch (lang){
                case 'en':
                    hopscotch.startTour(tour_upload_p1_en);
                    break;
            }
        }

        /**
         * start the upload tour 2
         */
        function startUploadP2Tour(){
            var lang = getTourLanguage();
            switch (lang){
                case 'en':
                    hopscotch.startTour(tour_upload_p2_en);
                    break;
            }
            setTourValue('tour_upload', true);
        }

        /**
         * start the review tour
         */
        function startReviewTour(){
            var lang = getTourLanguage();
            switch (lang){
                case 'en':
                    hopscotch.startTour(tour_review_en);
                    break;

            }
            setTourValue('tour_review', true);
        }
    }
})($, _);
