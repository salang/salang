(function () {
    'use strict';

    angular
        .module('salang.tour', [
            'salang.tour.controllers',
            'salang.tour.services'
        ]);

    angular.module('salang.tour.controllers', []);
    angular.module('salang.tour.services', []);
})();
