/**
 * first upload tour part
 * @type {{id: string, steps: *[]}}
 */
var tour_upload_p1_en = {
    id: "upload-page-p1-en",
    steps: [
        {
            title: "Upload Button",
            content: "Click here to select the file or files you want to upload.",
            target: "fileuploadicon",
            placement: "right"
        }
    ]
};


/**
 * second part of the upload tour
 * @type {{id: string, steps: *[]}}
 */
var tour_upload_p2_en = {
    id: "upload-page-p2-en",
    steps: [
        {
            delay: 3000,
            title: "Uploaded Content",
            content: "Each uploaded document will be listed here with fields to fill out.",
            target: "the_object",
            placement: "top"
        },
        {
            title: "Document Title",
            content: "For each document fill in a appropriate title.",
            target: "tour_title_0",
            placement: "right"
        },
        {
            title: "Document Description",
            content: "For each document fill in a appropriate description.",
            target: "tour_desc_0",
            placement: "right"
        },
        {
            title: "Document Language",
            content: "For each document ensure that the language was correctly identified.",
            target: "tour_language_0",
            placement: "right"
        },
        {
            title: "Finalise Upload",
            content: "Once all fields are filled in you can submit the uploads with their details.",
            target: "tour_upload_btn",
            placement: "right"
        }
    ]
};



