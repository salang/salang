/**
 * the main tour for the main page
 * @type {{id: string, steps: *[]}}
 */
var tour_main_en = {
    id: "main-page-en",
    steps: [
        {
            title: "Interface Language",
            content: "Choose an interface language here.",
            target: "tour_language",
            placement: "left"
        },
        {
            title: "Member Login",
            content: "Log in to the portal here or create an account.",
            target: "tour_login",
            placement: "left"
        },
        {
            title: "Your Profile",
            content: "Change the settings of your profile and see all your uploaded documents.",
            target: "tour_profile",
            placement: "left"

        },
        {
            title: "Search Bar",
            content: "Use this to search for content.",
            target: "tour_search",
            placement: "bottom"
        },
        {
            title: "Have nothing to search? Browse content here.",
            content: "Here you can browse for content by language, by most recent and all resources.",
            target: "first_dd",
            placement: "bottom"
        },
        {
            title: "Contribute Content",
            content: "Here registered users can upload content or review content to earn points. The leaderboard can also be accessed here",
            target: "second_dd",
            placement: "bottom"
        },
        {
            title: "Help Button",
            content: "To restart this tour at any time use this Help button.",
            target: "tour_help_btn",
            placement: "top"
        }
    ]
};




//example tour for another language
var tour_main_zu = {
    id: "main-page-zu",
    i18n: {
        nextBtn: "nxt",
        prevBtn: "prv",
        doneBtn: "dne",
        skipBtn: "skp",
        closeTooltip: "cls"
    },
    steps: [
        {
            title: "Interface Language",
            content: "Choose an interface language here.",
            target: "tour_language",
            placement: "top"
        },
        {
            title: "Search Bar",
            content: "Use this to search for content.",
            target: "tour_search",
            placement: "bottom"
        },
        {
            title: "Member Login",
            content: "Log in to the portal here or create an account.",
            target: "tour_login",
            placement: "bottom"
        },
        {
            title: "Upload Content",
            content: "Upload your own content to earn points.",
            target: "tour_upload",
            placement: "bottom"
        },
        {
            title: "Review Content",
            content: "Review content in your familiar languages to earn points.",
            target: "tour_review",
            placement: "bottom"
        },
        {
            title: "Leaderboard",
            content: "See where you are placed on the leaderboard compared to the other users.",
            target: "tour_leaderboard",
            placement: "bottom"
        },
        {
            title: "Help Button",
            content: "To restart this tour at any time use this Help button.",
            target: "tour_help_btn",
            placement: "top"
        }
    ]
};

/**
 * global settings
 * @type {{showCloseButton: boolean, showNextButton: boolean, showPrevButton: boolean}}
 */
var global_settings = {
    showCloseButton: true,
    showNextButton: true,
    showPrevButton: true
};

//set global settings
hopscotch.configure(global_settings);

