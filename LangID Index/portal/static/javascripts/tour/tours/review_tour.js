/**
 * review tour
 * @type {{id: string, steps: *[]}}
 */
var tour_review_en = {
    id: "review-page-en",
    steps: [
        {
            title: "Document to review",
            content: "Read the content shown below.",
            target: "tour_doc",
            placement: "top"
        },
        {
            title: "Language Review Question",
            content: "Based on the document's content indicate if the document is in the above specified language.",
            target: "radio2",
            placement: "right"
        },
        {
            title: "Finishing up",
            content: "Submit your review and earn points!.",
            target: "tour_review_submit",
            placement: "right"
        }
    ]
};
