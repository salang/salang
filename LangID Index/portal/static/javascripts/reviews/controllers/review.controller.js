/**
 * ReviewLanguageController
 * Controller for the reviews
 * @namespace salang.review.controllers
 */
(function () {
    'use strict';

    angular
        .module('salang.review.controllers')
        .controller('ReviewLanguageController', ReviewLanguageController);

    ReviewLanguageController.$inject = ['$scope', 'Reviews', 'Authentication', '$location', 'Notification', 'Tours', 'Documents', 'ngDialog'];

    /**
     * @namespace ReviewLanguageController
     */
    function ReviewLanguageController($scope, Reviews, Authentication, $location, Notification, Tours, Documents, ngDialog) {
        var vm = this;
        vm.postReview = postReview;
        vm.nextDoc = nextDoc;
        vm.reportDoc = reportDoc;
        vm.alldocs = undefined;


        //check if the user is authenticated as this is a members only section
        if (!Authentication.isAuthenticated()){
            Notification.error("Please log in.");
            $location.url('/login');
        }

        //check if the review tour has been started
        if (!Tours.checkReviewTour()){
            Tours.startReviewTour();
        }


        //variables
        vm.profile = Authentication.getAuthenticatedAccount();
        vm.imageUrl = undefined;
        vm.originalLanguage = undefined;
        vm.upload = undefined;
        vm.review = undefined;
        vm.default = true;

        $scope.status1 = 'option1';

        var index = 0;

        //get reviews for the user
        Reviews.getDocumentToReview(vm.profile.id).then(docSuccessFn, docErrorFn);

        /**
         * success function for reviews
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function docSuccessFn(data, status, headers, config){
            vm.alldocs = data.data;
            if (vm.alldocs.length == 0){
                vm.nodocs = true;
            }
            else{
                nextDoc();
            }
        }


        /**
         * next document to review
         */
        function nextDoc(){

            if (index == vm.alldocs.length){
                reviewCompletion();
            }
            vm.imageUrl = vm.alldocs[index].docfile;
            //replace language
            if (vm.alldocs[index].language === 'Sesotho sa Leboa'){
                vm.originalLanguage = 'Sepedi';
            }
            else{
                vm.originalLanguage = vm.alldocs[index].language;
            }
            vm.id = vm.alldocs[index].id;
            vm.upload = vm.alldocs[index];
            index++;
        }


        /**
         * error function for reviews
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function docErrorFn(data, status, headers, config){
            console.log("doc fetch failed");
            Notification.error("Could not fetch documents for selected language.");
        }

        /**
         * send the review to the backend
         */
        function postReview(){
            //post the review
            var var1 = true;
            if (vm.status1 == 'option1'){
                var1 = true;
            }
            else if (vm.status1 == 'option2'){
                var1 = false;
            }
            //use service with info provided
            Reviews.createReview(var1,vm.profile.username, vm.upload.id).then(uploadSuccessFn, uploadErrorFn);

        }

        /**
         * show notification for rewarding user
         */
        function rewardNotification(){
            Notification.success("Review Successful!");
            Notification.info({message: 'Congratulations! You earned <b style="color: red;">15</b> points!'});
        }

        /**
         * success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function uploadSuccessFn(data, status, headers, config){
            vm.review = data.data;
            Reviews.postProcess(vm.review.id).then(postProcessSuccessFn, postProcessErrorFn);
        }

        /**
         * error function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function uploadErrorFn(data, status, headers, config){
            Notification.error("Could not submit review.");
            console.log("Could not submit review to BE.");
        }

        /**
         * post success function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function postProcessSuccessFn(data, status, headers, config) {
            rewardNotification();
            nextDoc();
        }

        /**
         * post error function
         * @param data
         * @param status
         * @param headers
         * @param config
         */
        function postProcessErrorFn(data, status, headers, config){
            Notification.error("Could not process review.");
            console.log("Could not process review.");
        }

        /**
         * reroute if the review was successful
         */
        function reviewCompletion() {
            //now we need to reroute user to success page
            $location.url("/review_success");
        }

        /**
         * report the document if there is a problem
         * @param id- id of doc
         */
        function reportDoc(id){
            //send report int
            Documents.reportDoc(id, Authentication.getAuthenticatedAccount().email);
            //show success message
            $scope.ngDia3 = ngDialog.open({ template : 'static/templates/documents/doc_reported.html' ,
                scope : $scope ,
                className: 'ngdialog-theme-default',
                plain: false,
                showClose: true,
                closeByDocument: true,
                closeByEscape: true,
                appendTo: false,
                disableAnimation: true});
        }
        
    }
})();

