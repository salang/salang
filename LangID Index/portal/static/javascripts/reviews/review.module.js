(function () {
    'use strict';
    angular
        .module('salang.review', [
            'salang.review.controllers',
            'salang.review.services'
        ]);

    angular.module('salang.review.controllers', []);
    angular.module('salang.review.services', []);
})();
