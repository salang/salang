/**
 * Reviews
 * service for the reviews
 * @namespace salang.review.services
 */
(function () {
    'use strict';

    angular
        .module('salang.review.services')
        .factory('Reviews', Reviews);

    Reviews.$inject = ['$http'];

    /**
     * @namespace Reviews
     */
    function Reviews($http) {
        /**
         * @name Reviews
         * @desc The factory to be returned
         * @memberOf salang.review.services.Reviews
         */
        var Reviews = {
            getDocumentToReview : getDocumentToReview,
            createReview : createReview,
            postProcess : postProcess,
            cleanDB : cleanDB
        };

        return Reviews;

        /////////////////////

        /**
         * @name getDocumentToReview
         * @desc get a doc to review
         * @returns {Promise}
         * @memberOf salang.review.services.Reviews
         */
        function getDocumentToReview(id) {
            return $http.get('/api/v1/documents/get_review_doc?id=' + id);
        }

        /////////////////////

        /**
         * @name createReview
         * @desc create a document reviews
         * @returns {Promise}
         * @memberOf salang.review.services.Reviews
         */
        function createReview(language, author, upload){
            return $http.post('/api/v1/reviews/', {
                correctLang: language,
                author: author,
                upload: upload
            });
        }

        /**
         * @name postProcess
         * @desc analyse results
         * @returns {Promise}
         * @memberOf salang.review.services.Reviews
         */
        function postProcess(id){
            return $http.get('api/v1/reviews/postReviewProcess?id='+id);
        }

        /**
         * @name postProcess
         * @desc analyse results
         * @returns {Promise}
         * @memberOf salang.review.services.Reviews
         */
        function cleanDB(){
            return $http.get('api/v1/cleadb/');
        }



    }
})();

