from django.conf.urls import url, include
from portal.views import IndexView
from rest_framework_nested import routers
from authentication.views import AccountViewSet, LoginView, LogoutView, LeaderboardView, checkAccount, getLanguages, \
    setLanguages, updateLanguages, getNewUsers, changePassword, changeName
from django.conf import settings
from django.conf.urls.static import static
from uploads.views import UploadViewSet, AccountUploadsViewSet, get_document,  update_no_views, \
    call_post_processes, identify_language, clean_fail_upload, fetch_last_doc, get_review_doc, get_user_docs, \
    delete_doc, report_doc, get_random_stats, get_stats
from search.views import increment_counter, relevance_Vote, submit_Evaluation, report_Language, clear_Data
from review.views import ReviewViewSet, AccountReviewsViewSet, postReviewProcess



# create and register routers for the view sets
router = routers.SimpleRouter()
router.register(r'accounts', AccountViewSet)
router.register(r'leaderboard', LeaderboardView)
router.register(r'uploads', UploadViewSet)
router.register(r'reviews', ReviewViewSet)

accounts_router = routers.NestedSimpleRouter(
    router, r'accounts', lookup='account'
)
accounts_router.register(r'uploads', AccountUploadsViewSet)
accounts_router.register(r'reviews', AccountReviewsViewSet)

# add the media route for media items
urlpatterns = []
if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# add in all other request patterns
urlpatterns += [
    url(r'^api/v1/accounts/checkAccount$', view=checkAccount, name='checkAccount'),
    url(r'^api/v1/accounts/changePassword', view=changePassword, name='changePassword'),
    url(r'^api/v1/accounts/getLanguages$', view=getLanguages, name='getLanguages'),
    url(r'^api/v1/accounts/updateLanguages', view=updateLanguages, name='updateLanguages'),
    url(r'^api/v1/accounts/setLanguages', view=setLanguages, name='setLanguages'),
    url(r'^api/v1/accounts/getNewUsers$', view=getNewUsers, name='getNewUsers'),
    url(r'^api/v1/accounts/changeName$', view=changeName, name='changeName'),
    url(r'^api/v1/documents/get_user_docs$', view=get_user_docs, name='get_user_docs'),
    url(r'^api/v1/documents/get_document$', view=get_document, name='get_document'),
    url(r'^api/v1/documents/update_no_views$', view=update_no_views, name='update_no_views'),
    url(r'^api/v1/documents/call_post_processes$', view=call_post_processes, name='call_post_processes'),
    url(r'^api/v1/documents/identify_language$', view=identify_language, name='identify_language'),
    url(r'^api/v1/documents/clean_fail_upload$', view=clean_fail_upload, name='clean_fail_upload'),
    url(r'^api/v1/documents/fetch_last_doc$', view=fetch_last_doc, name='fetch_last_doc'),
    url(r'^api/v1/documents/get_review_doc$', view=get_review_doc, name='get_review_doc'),
    url(r'^api/v1/documents/delete_doc$', view=delete_doc, name='delete_doc'),
    url(r'^api/v1/documents/report_doc$', view=report_doc, name='report_doc'),
    url(r'^api/v1/stats/get_stats', view=get_stats, name='get_stats'),
    url(r'^api/v1/stats/get_random_stats$', view=get_random_stats, name='get_random_stats'),
    url(r'^api/v1/reviews/postReviewProcess$', view=postReviewProcess, name='postReviewProcess'),
    url(r'^api/v1/search/increment_counter$', view=increment_counter, name='increment_counter'),
    url(r'^api/v1/search/relevance_Vote$', view=relevance_Vote, name='relevance_Vote'),
    url(r'^api/v1/search/submit_Evaluation$', view=submit_Evaluation, name='submit_Evaluation'),
    url(r'^api/v1/search/report_Language$', view=report_Language, name='report_Language'),
    url(r'^api/v1/search/clear_Data$', view=clear_Data, name='clear_Data'),
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/', include(accounts_router.urls)),
    url(r'^api/v1/auth/login/$', LoginView.as_view(), name='login'),
    url(r'^api/v1/auth/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^.*$', IndexView.as_view(), name="index"),
]


