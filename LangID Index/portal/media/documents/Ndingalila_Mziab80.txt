kuthe kwakuthi cwaka ndathi "ntomb' entle, yiza nothando, ndinik' uthando." Intomb' emhlotshazana ubus' obuhle (Ndy'abulela kuwe Nkosi yamu). Kutsho kwakhanya empilweni yami, ndohlala ndonwabile njalo. Ndohlala ndizibuza bawo ngeli thamsanwa ondinike lona.

Sekudala ndimlindile ndimfumene, umama wekhaya. Andisoze ndimphuncule, andisoze ndimyeke, umam' 'ekhaya. Sekudala ndimlindile ndimfumene. Masihlalisane, sibonisane sakh' ikhaya. Sohlala sobabini sithanda.

Kudala ndizula ndil' fumene, uthando lwangenene, wena ulubambo lwam. Ubom' bam' butshintshile, ndizohlala ndinkowakho ngokunaphakade. Andizange ndambona umntu onje, andizange ndayibona into, intliziyo ixolile.  