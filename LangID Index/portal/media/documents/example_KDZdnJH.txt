1. Uluvo lweKhabhinethi kwimiba ebalulekileyo kwiimeko ezigqubayo nezingundabamlonyeni

1.1 IKhabhinethi yamkele ingxelo kaGoldman Sachs, yaMashumi amaBini emiNyaka eNkululeko, eyingcaciso yemimandla elishumi apho uMzantsi Afrika uthe wenza inkqubela phambili yeziseko ezingundoqo ukusukela ngowe-1994 nemimandla elishumi apho kuye kwafuneka ilizwe lithathe inyathelo elibalulekileyo.

Le ngxelo sisiqinisekiso esamkelekileyo sohlolo lukarhulumente ngokwakhe lokusebenza kwelizwe kwaye ingqina imiba engundoqo nephambili yekamva elicwangciswe kwisiCwangciso soPhuhliso seSizwe.

Le ngxelo ngumthombo wethemba nemvuselelo kwaye iligalelo elibalulekileyo kwingxongxo phakathi koluntu ngexesha apho uMzantsi Afrika uqalise ukubhiyozela imiNyaka  engama-20 yeNkululeko.

Urhulumente ukhuthaza abemi boMzantsi Afrika ukuba basebenzisane basebenze ngamandla ukwenza uMzantsi Afrika uphumelele ngakumbi baze banikele amandla wabo neencoko zabo zigxile kule miba.

1.2 IKhabhinethi iyakwamkela ukusungulwa kweedyunivesithi ezimbini eMntla Koloni naseMpumalanga, ezizokwakhiwa okokuqala ukusukela sathi safumana ulawulo lwentando yesininzi.

IDyunivesithi yaseMpumalanga iza kuvulwa kweyoMqungu yowama-2014. Le mpumelelo ibonisa ukubaluleka okubekwa ngurhulumente nabo bonke abemi boMzantsi Afrika kwezemfundo njengenxalenye yophuhliso lwesizwe. Le khampasi entsha iza kwenza ukuba abafundi baseMpumalanga bakwazi ukufunda kufutshane namakhaya wabo kwaye iza kuvelisa iintlobo zemisebenzi yezoqoqosho ekhokelisa ekusebenzeleni idyunivesithi noluntu lwayo.

UMzantsi Afrika wenze inkqubela phambili enkulu ekuboneleleni ngeenkonzo namaziko amatsha  kuluntu olungathathi ntweni nolusokolayo.

Ukutshatyalaliswa kwala maziko kubuyisela umva okubalulekileyo esisele sikuzizile ngexesha lolawulo lwentando yesininzi lwethu olutsha.

1.3  IKhabhinethi kananjalo isamkela isibhengezo sokuba izitishi zombane zakwa-Eskom iKusile neMedupi ziza kuqalisa ukuvelisa umbane ukususela ngowama-2014. 

Ukuveliswa kombane kulo nyaka uzayo zizitishi zombane iKusile neMedupi kuza kuba sesinye isiganeko esikhulu kuphuhliso lweli lizwe kwaye siza kubonisa impumelelo karhulumente ekufezekiseni inkqubo enkulu yophuhliso lweziseko ezingundoqo equlunkqa ngokutsha uqoqosho noluntu lwethu.

Umbane oza kuhlala ukhona ubalulekile kubathengi basekhaya nabamashishini amakhulu kwaye uvulela amathuba okuhlumisa uqoqosho – into ebikade ingumba ophambili  nebalulekileyo kolu lawulo ukusukela kowama-2009. Ngeeprojekthi ezininzi ezifana nale kwilizwe ngokubanzi, uMzantsi Afrika uthathe amanyathelo amaninzi abalulekileyo kunye nendlela entsha yohlumo lwezoqoqosho, kwaye bonke abemi boMzantsi Afrika kufanele babhiyozele iimpumelelo zethu.

1.4  IKhabhinethi incoma ukutyikitywa kweSivumelwano seProjekthi yaMandla wamanzi ye-Grand Inga, ngexesha loTyelelo lwasebuRhulumenteni lwakutsha nje lukaMongameli uJacob Zuma kwi-Democratic Republic of Congo. Le projekthi iza kwandisa ufikelelo lombane e-Afrika nezinto eziluncedo  ezininzi zezolimo, ezemigodi namanye amacandelo kummandla woLuntu oluPhuhlileyo lwe-Afrika eseMazantsi (i-SADC).

Xa sele zigqitywe zonke izigaba, le projekthi iza kuza nombane ovuselelweyo kwisiqingatha selizwekazi le-Afrika. Le projekthi iza kuvuselela ukudalwa kwamathuba emisebenzi, uphuhliso lwezakhono nokudluliselwa kobuchwephetshe kumazwe athatha inxaxheba kwaye kuza kuxhamla abasebenzisi basemakhaya nabamashishini amakhulu. Oku kubalulekile kuhlumo lwezoqoqosho nokhuphiswano kummandla we-SADC ngokubanzi.

1.5  IKhabhinethi ikhuthaza bonke abemi boMzantsi Afrika, ingakumbi abavoti abalulutsha nabaqalayo, ukuba basebenzise ilungelo labo lolawulo lwentando yesininzi ngokuthatha inyathelo lokuqala lokubhalisela ukuvota ngomhla we-09 nowe-10 kweyeNkanga yowama-2013.

Umzabalazo wethu wenkululeko wenza ukuba wonke ummi woMzantsi Afrika afumane ilungelo lokuba ngumthathi-nxaxheba odlala indima kwinkqubo yethu yolawulo lwentando yesininzi. Sihlaba ikhwelo kubo bonke abemi boMzantsi Afrika ukuba bahloniphe imbali yethu baqhubekeke nokomeleza ukuba semthethweni kolawulo lwethu lwentando yesininzi.

IKhabhinethi ihlaba ikhwelo kubo bonke abavoti ukuqinisekisa ukuba unyulo lowama-2014 luza kuba nabavoti abaninzi, njengoko besibonile kunyulo ngalunye ukusukela kowe-1994. Zonke ii-ofisi zeSebe leMicimbi yezeKhaya ziza kuvulwa ngemihla yokubhalisela ukuvota ukuze kuququzelelwe inkqubo yokubhalisa ngezincwadi ezifanelekileyo.

1.6  IKhabhinethi incoma abahlawuli berhafu abangaphezu kwezigidi ezi-3,6 abathe bafaka amaxwebhu wabo werhafu ukuzokufika ngoku, abancede urhulumente ukuba abonelele ngeenkonzo ezifuneka kakhulu eziphucula ubomi babo bonke abemi boMzantsi Afrika. UMzantsi Afrika yindawo ephucuke kakhulu namhlanje kunangowe-1994, ngenxa yokukhula kwenani labantu neenkampani ezizimiseleyo nezikwaziyo ukuba negalelo kwingxowa yoluntu exhasa ngeenkonzo nangezinto eziluncedo.

IKhabhinethi ikhumbuza abahlawuli berhafu abangekafaki amaxwebhu wabo werhafu ukuba umhla wama-22 kweyeNkanga kowama-2013 ngumhla wokugqibela wokufakwa kwamaxwebhu wabantu nge-intanethi kunyaka werhafu wama-2012/13 kwaye uyasondela.

1.7 IKhabhinethi igxeka izenzo zobundlobongela zakutsha nje, ukutshatyalaliswa kwempahla yabucala nekawonkewonke nokuzityhuthulela eGauteng, eMpuma Koloni naseNtshona Koloni. IKhabhinethi incoma amapolisa ngokungenelela nangokudambisa imeko.

Ukutshatyalaliswa kwempahla kawonkewonke ngabantu kuvalela uluntu iinkonzo ezidingeka kakhulu kuze kudale umthwalo owongezelelekileyo wemithombo ethi endaweni yokuba kuqhutyekelwe phambili kufuneke ukuba kulungiswe okanye kutshintshwe iinkonzo ebesezikhona.

Urhulumente akazukubavumela abophuli-mthetho ukuba batshabalalise iziseko nezinto eziluncedo apho kunikezelwa khona ngeenkonzo eziluncedo. Ukonakalisa izinto akuncedi mntu, kwaye ukutshabalalisa inkonzo ethile kuba ufuna enye akunangqiqo.

Uluntu kufanele lulwe zonke iinzame ezenziwa ngabophuli-mthetho zokubamba imfundo yabantwana bethu ngokoyikisa ngokuphazamisa iimviwo.
Kufanele sakhe uluntu apho sizokuncedana size sihloniphane, kunokuba soyikane.

1.8  IKhabhinethi ivakalise inkxalabo ngeengxelo ezenziwe esidlangalaleni malunga  namaJuda oMzantsi Afrika. IKhabhinethi iyaliqonda ilungelo labantu basePalestina   nokuzimisela nelungelo likaSirayeli lokuba libekho ndawonye nelizwe lasePalestina. Iinzame zabo zokufumana isisombululo kwingxubakaxaka ezakuba nobulungisa kufanele ziqhubeke. Uluntu lwamaJuda oMzantsi Afrika kufuneka lungoyiki kwanto.  Urhulumente khange anyanzelise ukuvalwa kokuhambela kwamagosa karhulumente kwaSirayeli.

1.9 IKhabhinethi iyothulela umnqwazi iimpumelelo yeembaleki zoMzantsi Afrika ezithe kutsha nje zangenela ukhuphiswano loMdyarho womgama omde lwaseNew York City, apho okhubazekileyo obengenele ukhuphiswano lwama-Olympia u-Ernst van Dyk aye waphumelela indawo yesibini kwicandelo lesitulo esinamavili, ngeli xesha uLusapho April eye waphumelela kwindawo yesithathu kudidi lwamadoda lwabakhethiweyo. Ukongeza apho, uDon Oliver no-Abraham Mouton babe kubantu abangama-60 abathe bagqiba kuqala kwicandelo lebhayisikile eqhutywa ngesandla.

2. Iingxoxo kunye nezigqibo zeKhabhinethi ezingundoqo

2.1 IKhabhinethi ivumile ukuba uGunyaziwe weeNqwelo-moya woMzantsi Afrika obizwa i-South African Civil Aviation Authority (i-SACAA) asingathe ingqungquthela yokuQala yaMayeza oMda waseMoyeni wase-Afrika noMbutho weeNqwelo-moya waMazwe-ngamazwe (i-ICAO) kweyoMdumba yowama-2014. Iza kuhlanganisa oogunyaziwe abalawulayo namaGgcisa ezoNyango kuMda waseMoyeni kuxoxwe nemingeni ejongene neli candelo.

Ukusingatha lo msitho kuza kunceda ekukhuthazeni ukwazisa nokukhuthaza ooRhulumente base-Afrika ukuba babelane ngolwazi maleyana nokusebenza kwabasebenzi basemoyeni, ukhuseleko lwabasebenzi basemoyeni nabakhweli kwinqwelo-moya kwakunye nolawulo lokhuseleko.

3. Izikhundla
IKhabhinethi ikuphumezile ukuqeshwa kwaba bantu balandelayo kwezi zikhundla:

3.1 uNksz. Kholofelo Glorious Sedibe kwisikhundla sikaSekela Mlawuli Jikelele: Imisebenzi yobuKhokeli noLawulo kwi-Ofisi yeKomishini yeeNkonzo zoLuntu (i-OPSC).
