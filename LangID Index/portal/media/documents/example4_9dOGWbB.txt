Biza umhlangano nalabo bantu  abakufaneleko abanekareko
Nangabe ukhona umsebenzi oqalene nomlilo, kufanele unikele inothisi etlolwe phasi kuhlangano leyo ofuna ukuyibumba okungenye eyihlangano yokuvikela umlilo bese umema isiPhathiswa sabo esiPhezulu ukobana sikhambele umhlangano.
Tlola amaminidi womhlangano, azakuveza lokhu:

Ukobana iye nanyana awa kube khona isekelo elibumbeneko mayelana nokubumba ihlangano yokuvikela umlilo
Inani lamavowudu  akhambisana nokubumba ihlangano
Inani lamavowudu elingakhambisaniko nokubunjwa kwehlangano; begodu
Inani labantu abagodle amavowudwabo mayelana nokubumba ihlangano.
Tlola bewutlikitle isivumelwano esitlolwe phasi esitjhoko bona uyingcenye yehlangano
Khetha usihlalo nesigungu sekomidi
Nikela ibizo lehlangano mayelana nokuvikela umlilo kanye nehlathululo yendawakho
Iba nerejista yabakhambele umhlanganio kanye nemininingwana abangathintwa kiyo kufakwe hlangana iimphande zeemposo zabo boke abantu
Khupha bese uthumela iForomo 1 ngaphasi kwamaforomo ukobana ulizalise, mayelana nokuphunyeleliswa yi-DAFF. Nangabe i-DAFF iphumelelisa isibawo sakho,  yeke kufanele ukhuphe bewuthumele iforomo 2 bese ufaka lokhu okulandelako:
amaminidi womhlangano
umthethosisekelo we-FPA begodu
Nomtlamo webhizinisi we-FPA, ofaka hlangana amaqhinga wokuphatha imililo yommango kanye nemithetho.
