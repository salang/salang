Cape Town – Ubucwebe benani lika-R100 000 kuthiwa abutholakali kulandela ukuthi bukhunyulwe esidunjini sowesifazane emakhazeni aseSalt River, eKapa.

UShavonne Miller utshele umsakazo iCapeTalk ngoLwesibili ukuthi udinwe ukhahlela izidindi kulandela ukwenzeka kwalesi sigameko.

La makhaza amukele isidumbu sikamama wakhe, uBarbara Carolus‚ emasontweni ambalwa edlule kanti kuthiwa kusukela lapho iyaphunza imizamo yokuthola lobu bucwebe ashona ebugqokile.

OLUNYE UDABA: Kugwilize ababili bethungatha ubucwebe emapayipini eThekwini

“Ngesikhathi kufika amalungu omndeni ezohlonza isidumbu sikaBarbara, zonke izinto zakhe okubalwa nobucwebe kwase kugcinwe yizisebenzi zasemakhazeni,” esho.

Uthi kwadingeka ukuba abuyele emuva ngolunye usuku azobulanda, wathi uma efika izisebenzi zalokhu zibamba ziyeka.

“Kwagcina sekuthiwa bonke ubucwebe bakhe bulahlekile,” esho.

UMiller uthe sebevule icala emaphoyiseni, kanti nezikhulu zamakhaza zithi kunophenyo lwangaphakathi oluqhubekayo.

- Times Live
