1. Xiyimo xa Khabinete mayelana na timhaka ta nkoka eka nkarhi wa sweswi

1.1 Khabinete yi amukerile xiviko xa Goldman Sachs, Makumembirhi wa Malembe ya Ntshuxeko, lawa ya kombisaka tindzhawu ta 10 laha Afrika-Dzonga ri nga humelela ku sukela hi 1994 na tindzhawu ta 10 laha tiko ri nga fanela hi ku teka xiendlo eka swiboho.

Xiviko i xitiyisiso xo amukeriwa hi mfumu eka xikambelwana xa matirhele ya tiko no yimela swikongomiso swa nkarhi lowu taka leswi nga boxiwa eka Pulani ya Nhluvukiso wa Rixaka.

Xiviko i xihlovo xa ntshembho na ntlakuso naswona i ku hoxa xandla ka nkoka eka vanhu hi nkarhi lowu Afrika-Dzonga a ri sungurile ku tlangela Malembe ya 20 Ya Ntshuxeko.

Mfumu wu kombela Vaaki va Afrika-Dzonga ku tirha kun’we no tirha hi matimba ku endla leswaku Afrika-Dzonga ri humelela swinene no kongomisa eka matimba na mimbhurisano eka timhaka leti.

1.2 Khabinete yi amukela ku tumbuluxiwa ka tiyunivhesiti tintshwa eKapa N’walungu na le Mpumalanga, leti nga ta akiwa ku sukela ku sungurile xidemokirasi.

Yunivhesiti ya Mpumalanga yi ta pfula hi Sunguti 2014. Ku humelela loku ku kombisa nkoka wa mfumu naswona Vaaki va Afrika-Dzonga va fanele ku tikhomanisa na dyondzo tani hi xiphemu xa rixaka xa nhluvukiso. Khamphasi leyintshwa yi ta endla leswaku swichudeni swa Mpumalanga swi dyondza kusuhi na le kaya naswona swi ta tisa migingiriko yo tala ya ikhonomi leyi nga kongomisiwa eku korhokeleni ka yunivhesiti na vanhu va yona.

Afrika-Dzonga ri endlile ku humelela loku kulu eku nyikeni ka vukorhokeri eka
swikolo swa le henhla leswintshwa. Ku herisiwa ka swikolo leswi ku tlherisela
endzhaku mimbuyelo leyi hi nga yi kuma eka xidemokirasi xa hina lexintshwa.

1.3  Khabinete yi amukela xitiviso xa Eskom eka switici swa gezi swa Kusile na Medupi leswi nga ta hangalasa gezi hi 2014. 

Ku endliwa ka gezi lembe leri taka hi switici swa gezi swa Kusile na Medupi ku ta va goza rin’wana lerikulu eka nhluvukiso wa tiko naswona swi ta kombisa ku humelela ka mfumu eku tirhiseni ka phurogireme ya nhluvukiso wa switirhisiwa hi ntalo leswi nga ta aka hi vuntshwa ikhonomi ya hina na vaaki.

Gezi leri hlayisiwaka ri na nkoka eka vaxavi va le makaya na va swimakiwa – leswi ku nga xirhangana eka eka vulawuri lebyi ku sukela hi 2009. Hi tiphurojeke leti to hlaya ta muxaka lowu leti nga ku humeleleni laha tikweni, Afrika-Dzonga ri tekile magoza yo hlaya eka godzo ra rona lerintshwa, naswona Vaaki va Afrika-Dzonga va fanele ku tlangela mimbuyelo ya hina.

1.4 Khabinete yi hoyozela ku sayiniwa ka Ntwanano eka Phurojeke ya Grand Inga ya Gezi ro suka ematini, hi nkarhi wa Riendzo ra Mfumu sweswinyana hi  Phuresidente Jacob Zuma ku ya eRhiphabuliki ra Xidemokirasi ra Congo. Phurojeke leyi yi ta engetelela mfikelelo wa eneji eAfrika no kombisa mimbuyelo ya nkoka eka vurimi migodi na mabindzu yan’wana eka xivandla xa SADC.
       
Endzhaku ka loko swiphemu leswi swi hetisekile, phurojeke yi ta tisa eneji leyi pfuxetiwaka eka hafu ya khonthinete ya Afrika. Phurojeke leyi yi ta hundzula ku tumbuluxiwa ka mintirho, nhluvukiso wa vuswikoti na ncicano wa thekinoloji eka matiko lawa ya nga tinghenelerisa naswona lawa ya nga ta vuyerisa vatirhisi va laha kaya na vatirhisi eka vumaki. Leswi i nkulo wa ikhonomi wa nkoka na ku va na mphikizano eka xivandla xa SADC.

1.5 Khabinete yi hlohlotela Vaaki hinkwavo va Afrika-Dzonga, ngopfu ngopfu lavantsongo na lava nga ta langha ro sungula, ku tirhisa mfanelo ya vona ya xidemokirasi hi ku teka goza ro sungula ro titsarisela ku langha hi siku ra vunkaye na hi ti 10 Hukuri 2013.

Nyimpi ya hina ya ntshuxeko yi hlayisile mfanelo ya leswaku Muaki wun’wana ni wun’wana wa Afrika-Dzonga a va mungheneleri eka maendlele ya hina ya xidemokirasi. Hi kombela Vaaki hinkwavo va Afrika-Dzonga ku xixima matimu ya hina no ya emahlweni no tiyisa ndzhuti wa xidemokirasi xa hina.

Khabinete yi kombela valanghi hinkwavo ku tiyisisa leswaku ku hlawula ka 2014 ku tisa valanghi vo tala, tani hi leswi hi nga swi vona eka nhlawulo wun’wana ni wun’wana ku sukela hi 1994. Tihofisi hinkwato ta Xikaya ti ta pfuriwa eka masiku yo titsarisela ku fambisa maendlele ya ku titsarisela maphepha lawa ya faneleke.

1.6  Khabinete yi khensa vahakeri va xibalo lava tlulaka mamiliyoni ya 3.6 lava va nga tlherisa timali ta xibalo ku fikela sweswi, ku pfuna mfumu ku endla vukorhokeri lebyi lavekaka ku antswisa vutomi bya Vaaki va Afrika-Dzonga.

Afrika-Dzonga i ndzhawu yo antswa swinene namuntlha ku tlula leswi a yi ri xiswona hi 1994, hi ku ya hi nhlayo ya vanhu na tikhamphani leti nga tiyimisela ku hoxa xandla eka nkwama wa vanhu laha migingiriko na mimbuyelo swi hakeriwaka ku suka kona.

Khabinete yi tsundzuxa vahakeri va xibalo lava va ha faneleke ku yisa timali ta xibalo leswaku siku ro pfala hi ku rhumela hi xitironiki hi munhu wun’we i ti 22 Hukuri 2013 na leswaku lembe xibalo ra 2012/13 ri le ku tshuneleni hi matimba.

1.7  Khabinete yi alela madzolonga lawa ya nga humelela sweswinyana, ku onhiwa ka tinhundzu ta vanhu na ta mfumu no teka tinhundzu swi nga ri enawini eGauteng, Kapa Vuxa na Kapa Vupeladyambu. Khabinete yi khensa maphorisa eku ngheneleleni no lawula xiyimo.

Ku onhiwa ka nhundzu ya vanhu ku karisa migingiriko leyi lavekaka swinene eka vaaki no endla ndzhwalo wo engetelela eka switirhisiwa leswi susiwaka ku ya engetela vukorhokeri lebyi.

Mfumu a wu nge pfumeleli swigevenga ku onha vuako na nhundzu laha vukorhokeri bya nkoka byi nyikiwaka ku suka kona. Ku onha a ku pfuni munhu nchumu, naswona a ku na miehleketo yo vupfa eku onheni ka vukorhokeri byin’we loko ku ve ku laveka byin’wana.

Vaaki va fanele ku lwa na ku ringeta hinkwako ka maendlele ya vugevenga yo teka dyondzo ya vana va hina ya byi khoma nkuzi hi ku chavisela ku kavanyeta swikambelo.

Hi fanele ku aka matiko laha hi hloniphaka no pfunana, handle ka ku va hi chavana.

1.8 Khabinete yi kombisile ku vilela hi marito evanhwini lawa ya kongomaneke na Majuda ya le Afrika-Dzonga. Khabinete yi tsundzuka mfanelo ya vanhu va le Palestine leswaku va ti kambela na mfanelo ya Israel ku tshama emfun’wini wa Palestine. Xikongomelo xa vona xo kuma xintshuxo eka ntlimbo lowu ya ka emahlweni. Vaaki va Majuda va Afrika-Dzonga va fanele va nga vi na nchumu xo xi chava. Mfumu a wu se kombisa ku alela ra makumu ku endza ka varhumiwa  vamfumu eIsrael.

1.9 Khabinete yi khensa ku humela ka switsutsumi swa Afrika-Dzonga leswi a swi tsutsuma sweswinyana eka Marantoni ya le Dorobeninkulu ra New York, laha Mutsoniwa wa xitsutsumi xa tiolimpiki Ernst van Dyk a nga kuma xiyimo xa vumbirhi eka xiyenge xa vatsutsumi hi swituli swa vatsoniwa, laha Lusapho April a nga kuma xiyimo xa vunharhu eka xiyenge xa vavanuna xa marantoni ya mpfhuka wo leha swinene. Ku engetelela, Don Oliver na Abraham Mouton va hetelerile va ri eka lava va nga henhla va 60 eka xiyenge xo fambisa basikiri hi mavoko.

2. Mimbhurisano ya nkoka na swiboho swa Khabinete

2.1 Khabinete yi amukerile leswaku Vulawuri bya Vuhahisi bya Vanhu eAfrika-Dzonga (SACAA) na Nhlangano wa Vuhahisi bya Vanhu (ICAO) va khoma swin’we nhlengeletano yo sungula ya swa Mirhi eka Vuhahisi bya Vanhu eAfrika hi Nyenyenyani 2014. Swi ta tisa kun’we milawu yo lawula naswona Vatirhi va Vatshunguri exibakakabakeni va ta kota ku burisana hi swiphiqo leswi va hlanganaka na swona.

Ku khoma ntlangu lowu swi ta pfuna ku kurisa lemukiso no hlohlotela Matiko ya Afrika ku nyika mahungu ya matirhelo ya vatirhi va le xibakabakeni, nhlayiseko wa vatirhi na vakhandziyi eka swihahampfhuka ku katsa na mafambisele ya vuhlayiseki.

3. Ku thoriwa

Khabinete yi amukerile ku thoriwa loku landzelaka:

3.1 Manana Kholofelo Glorious Sedibe eka ntirho wa Xandla xa Mufambisinkulu-Jenerala: Vurhangeri na Maendlele ya Vufambisi eka Hofisi ya Khomixini ya Vukorhokeri bya Vanhu (OPSC).
