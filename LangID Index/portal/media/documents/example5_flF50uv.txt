1.7  IKhabhinethi igceka loluchungechunge lwebudlova lolusandza kwenteka, kubhidlitwa kwemphahla yebantfu bangasense neyahulumende nekwebiwa kwemphahla eGauteng, eMphumalanga Kapa naseNshonalanga Kapa. IKhabhinethi incoma emaphoyisa ngekutsi angenelele aphindze acedze lesimo.

Kubhidlitwa kwemphahla yemmango kuncisha imimango tinsita temmango letidzingeka kakhulu kubuye futsi kwente umtfwalo lowengetiwe wekutsi kutsatfwe leminye imitfombolusito isetjentiselwe kubuyisela esimeni futsi ime esikhundleni saletinsita.

Hulumende angeke avumele tigebengu tibhidlite takhiwo netakhiwonchanti netisetjentiswa lapho kuniketwa khona tinsita letibalulekile. Kubhidlitwa nekulinyatwa kwemphahla akusiti muntfu, futsi kuyintfo lengenangcondvo kutsi ubhidlite yinye insita nawufuna lenye.

Imimango ifanele ilwe nako konkhe lokukulingwa kwebugebengu lokutfumba kugodle imfundvo yebantfwabetfu ngekutsi kwesabise ngekuphatamisa luhlolo lwebafundzi lwekuphela kwemnyaka.
