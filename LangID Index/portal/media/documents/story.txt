UNeo nephasi elikhulu, elibanzi
Author  Vianne Venter
Illustrator  Rico
Translator  Itjhugululwe nguNomsa Mtsweni
This story is also available in: English / Afrikaans / isiXhosa / isiZulu / Sepedi / Sesotho / Setswana / Siswati / Tshivenda / Xitsonga
UNeo uhlola ngefesdere lelawu lakhe ubona isitrada esitshetlha esinabantu abatshetlha abanethiweko, barhabile ezulwini elitshetlha elina khulu. Gade abhalelwa kuphuma ngendlini begodu besele aqedile ukufundela uMbali zoke iincwadi zakhe.

Ngaleso sikhathi, kwangena ugogo iinhluthu zakhe zihlangahlanganiswe mumoya ngaphandle. Gade aphethe into ethileko. UNeo wayibona kobana yinto esipara, esasikwere, begodu inemibalabala � begodu iyavuleka � njengebhokisi lamagugu!

�Ngangiyithanda khulu incwadi le lokha nangisesengangawe,� uGogo utjela uNeo. �Beyimunyango wami ongingenisa ephasini elikhulu, elibanzi.�

Khonokho wavula incwadi.

Ekhasini lokuthoma bekunesithombe sendawo esimanga, ekude nobutshetlha belanga. Isiganga sasilihlaza-satjani, sisagolide begodu sizotho, senganyelwe mkayi omkhulu, nelanga elisarulani elifuthumeleko, elitjhisa khulu.

�Madekhethu! Ingabe liqiniso lelo?� kutjho uNeo adosa ummoya.

UGogo wamomotheka. �Awazi? Zoke iindaba ziliqiniso, lokha nawuzikholwako.� kwatjho uGogo. Wase ukhomba indawo ethileko ekhasini lapho kunomsanyana olingana noNeo, owabe akhamba esigangeni.

Lokha uGogo nakasafundako, uNeo wavala amehlo wase uyalahleka ngengqondo, wakhwela iintajana � weqa iphasi elikhulu, elizotho � weqela ngephasini elikhulu nelibanzi.
neo-3-col-flat-lores

Wezwa amaphimbo wesiganga.

�Phuma! Phuma!� kubhina inyoni encani.

�Lilanga elihle!� kutswitswiza intethe.

�Phumela ebaleni, yiza sizokudlala,� kuhleba ummoya etjanini obude.

UNeo wakhumbula izulu elitshetlha, elina khulu, wazibuza bonyana bekungaba kuhle nange angakghona ukuba ngaphandlapha. Kodwana endabeni, ungenza nanyana khuyini. Bekunganazulu lapha. Ngalokho-ke uNeo wakhamba waya ngaphetjheya kwesiganga.

Into yokuthoma ayibonako yayiyide, izotho begodu iqinile, inomzimba wesigodo. Inemikhono emide ezotho efikelela emkayini, inehloko ekulu eliqwatjhana esanhluthu ezihlaza-satjani egade iphetjhulwa mumoya ofuthumeleko.

�Lotjha,� kutjho uNeo, ahlahle amehlo. �Uyini wena?�

�Ngimumuthi. Ngikghona ukubona ngale kommango osagolide. Yizapha, Sizokubona sobabili.� Umuthi walula umkhono, uNeo wakhwelela.
neo-6col-flat-lores

Ukusuka phezulu emagatjeni, uNeo wakghona ukubona emaphelelweni wephasi. Bekunezinto ezinengi ngaphandle le, lokho kwapheze kwamthusa lokha nakacabanga ngakho.

Kodwana umuthi wambamba wamphephisa, wase uyamhlebela, �Khamba uyokubona. Ungasabi. Liphasi elihle, elikhulu, nelibanzi ngaphandle le.�

Ngalokho-ke, uNeo wehlela phasi wakhamba waya ngale kommango.

Msinyana, Wafika entatjaneni yesanda eqinileko eneembotjana ezincani, ezinjengeminyango emincazana. Bekakghona ukuzwa amezwi anomdlandla aziinkulungwana ngaphakathapho, begodu negido leenyawana eziziingidi ezisithandathu zokugijigijima kweenyawana ezimatsikana.

�Lotjhani! Nibobani nina?� kuzwakala uNeo abiza komunye weminyango.

�Lotjhani!� kuphendula ilizwi elincazana. Thina sibutjhontjhwani. Sicoca ngeendatjana zephasi ngaphakathapha. Khgani uyafuna ukuzwa ezinye?

UNeo wayezithanda iindaba, ngakho-ke, wahlala phasi walalela. Abotjhontjhwani bacoca ngeendaba zabo zesiganga nehlathi, kunye neentaba namadorobho nokhunye.

�Iindaba ezinengi kangaka?� kubuza uNeo.

�Kuneendaba ezinengi ezilingana neenkwekwezi emkayini,� kuphendula abotjhontjhwani.

UNeo waphephezelisa isandla avalelisa, wase ubamba indlela yakhe eya esigangeni.

Khonokho, uNeo Wafika emanzini amanengi agijima emrhotjheni ukusukela ekuseni bekube santambama. UNeo wangena ngaphakathi ukupholisa iinyawo zakhe.

Amanzi aphahlazela iinyawo zakhe ahleka, �Ngimlambo. Ngikhamba hlangana neentaba ngiye elwandle. Yiza, ngilandela. Ngizakusa ekhaya.�
neo-11-col-lores

UNeo wacabanga bonyana lokho kungaba mkhumbumbulo omuhle. Ngalokho-ke walandela umlambo wakhamba emrhobeni naphakathi kweentaba. Ndawonye, bazulazula ngemvakwamadina pheze kwaba sebusuku, ekugcineni uNeo wafika esiqongweni sombundu.

Ukusukela lapho, wakghona ukubona idorojana elincani, elihlwengiswe lizulu begodu belidzegema emisebeni yelanga elitjhingako.
neo-13-col2b-lores

Ilwandle laselikhamba ngokuzigedla. �Khamba, iya ekhaya. Kunabantu abakuthandako lapho, abalindele ukwabelana nawe iindaba.�

UNeo wehlela phasi, akhamba phakathi kwedorobho. Wabona iintarada eziphithizelako ezikhamba phakathi kwedorobho, njengamanzi. Wabona izindlu, ezifuthumeleko emkhanyweni welanga lantambana. Ngaphakathi kwazo, abantu bebamatasatasa, njengabontjhontjhwani abamatsikani.

Ekugcineni, uNeo wahlola efasidereni lapho ugogo, onemikhono enamandla neenhluthu eziimyekwana njengamagaja womuthi omkhulu, avala incwadi, agoba afuna ukwanga avalelise umsanyanakhe alale.
neo-15-col-lores

UNeo wacabanga ngesiganga, umuthi, abonotjhontjhwani nomlambo. Lokha asaqale ugogo, umbani wakhanyisa indlwanayana kagogo enemibalabala yakhanya yafana nesithombe esisencwadini yeendaba. UNeo wacabanga ngekhambo lakhe elikhulu elingemakhasini wencwadi kagogo yeendaba ayithandako, wase ucabanga ngogogo nangoMbali nekhaya.

Ngalokho-ke, uNeo wathatha incwadi kagogo wayifihla embhedenakhe ofuthumeleko, elawini lakhe elihle, endlini yakhe encazana.

Kungalokho-ke, lokha iphasi nangabe libonakala litshetlha khulu, ilawu lakhe libonakala kwangathi lincani khulu, uNeo uvula incwadi. Ungena emnyango phakathi kwamakhasi, bese uya ephasini elikhulu, elibanzi.

 

Veza ubukghwari bakho!

Ngokusebenza ndawonye cabangani bese nakhe isenzeko esisendabeni ngokusebenzisa

izinto zebhoduluko lakho. Yakha ubujamo bepilo obufanelekileko besenzeko, nanyana nakhe isenzeko esincani sebhoksi lamanyathelo.