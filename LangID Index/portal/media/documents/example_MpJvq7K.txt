1. Maemo a Kabinete mabapi le dintlha tsa bohlokwa maemong a ha jwale

1.1 Kabinete e amohetse tlaleho ya Goldman Sachs, Dilemoshome tse Pedi tsa Tokoloho, e leng tlhakisetso e tebileng ya dibaka tse leshome tseo Aforika Borwa e entseng dikgatelopele tse bonahalang haesale ho tloha ka 1994, esita le dikarolo tse 10 moo naha e tshwanelang ho nka dikgato ho tsona.

Tlaleho ena ke tiiso ya kamohelo ya boitekolo ba mmuso ba tshebetso ya naha ebile e ananela dintlha tse ka sehloohong bakeng sa bokamoso, bo tekilweng Morerong wa Ntshetsopele ya Naha.

Tlaleho ena ke mohlodi wa tshepo le kgothatso mme ke seabo sa bohlokwa puisanong le setjhaba ka nako ena eo Aforika Borwa e seng e qadile ho keteka Dilemo tse 20 tsa Tokoloho.

Mmuso o kopa maAforika Borwa ho sebetsa mmoho le ho sebetsa ka thata ho etsa hore Aforika Borwa e atlehe le ho feta, esita le ho tsepamiso maikutlo le dipuisano tsa bona mehopolong ena.

1.2 Kabinete e ananela ho thewa ha diyunivesithi tse ntjha tse pedi Kapa Leboya le Mpumalanga, tsa pele tsa ho ahwa haesale ho thewa demokrasi.

Yunivesithi ya Mpumalanga e tla bulwa ka Pherekgong ya 2014. Katleho ena e bontsha bohlokwa boo mmuso le maAforika Borwa ohle a e beang thutong e le karolo ya ntshetsopele ya naha. Khamphase ena e ntjha e tla thusa baithuti ba Mpumalanga ka ho ithuta haufi le mahabo bona mme e tla hlahisa menyetla e mengatanyana ya moruo e tla be e reretswe ho sebeletsa yunivesithi le setjhaba sa yona.

Aforika Borwa e entse dikgatelopele tse kgolo phanong ya ditshebeletso le ditheo tse ntjha setjhabeng se kotetsweng ebile e le se hlokang haholo. Tshenyo ya ditheo tsena e kgutlisetsa morao dikatleho tsa bohlokwa tseo bohle re di fihlelletseng nakong ya demokrasi ya rona e ntjha.

1.3  Kabinete e boela e ananela tsebiso ya hore ditsi tsa motlakase tsa Eskom, e leng Kusile le Medupi, di tla fepela motlakase ka 2014.

Ho fehlwa ha motlakase ke ditsi tsa motlakase tsa Kusile le Medupi isao e tla ba katleho e kgolo ntshetsopeleng ya naha mme e tla bontsha katleho ya mmuso ya ho kenya tshebetsong lenaneo la kaho ya meralo ya motheo e meholo, e bopang botjha moruo le setjhaba sa rona.

Motlakase o itjarang ka nako e telele o bohlokwa bakeng sa bareki ba malapeng le ba diindasteri mme o bula menyetla bakeng sa ho hodisa moruo – e leng ntlha e bileng ka sehloohong haesale ho tloha ka 2009. Ka diprojeke tse ngata hakaalo tsa mofuta ona tse ntseng di tswelapele naha ka bophara, Aforika Borwa e se e nkile dikgato tse ngatanyana tsa bohlokwa tseleng ya yona e ntjha ya kgolo, kahoo maAforika Borwa ohle a tshwanela ho keteka dikatleho tsa rona.

1.4  Kabinete e thoholetsa ho tekenwa ha Selekane sa Projeke e Kgolo ya Inga ya Phehlo ya Motlakase, nakong ya Ketelo ya moraorao ya Mopresidente Jacob Zuma Rephaboliking ya Demokrasi ya Congo. Projeke ena e tla atolla phihlello ya motlakase Aforika e be e fane ka menyetla e metle haholo ya temo, merafo le Makala a mang lebatoweng la Dinaha tse Tswelang pele tsa Aforika e ka Borwa (SADC).

Hang ha ho se ho phethetswe mekgahlelo yohle, projeke ena e tla tlisa phepelo e ntjhafatsehang ya motlakase kontinenteng ya Aforika. Projeke ena e tla qholotsa tlhahiso ya mesebetsi, ntshetsopele ya bokgoni le phetisetso ya theknoloji dinaheng tse nang le seabo mme e tla thusa basebedisi ba malapeng le ba diindasteri. Sena se bohlokwa haholo kgolong ya moruo le qothisano ya lehlokwa lebatoweng la SADC ka bophara.

1.5 Kabinete e kgothaletsa maAforika Borwa ohle, haholoholo batjha le bao ba tla be ba vouta kgetlo la pele, ho sebedisa tokelo ya bona ya demokrasi ka ho nka kgato ya pele ya ho ingodisetsa ho vouta ka la 9 le la 10 Pudungwana 2013.

Ntwa ya rona ya tokoloho e tshireleditse tokelo ya moAforika Borwa e mong le e mong ya ho ba mahlahahlaha seabong sa tsamaiso ya demokrasi. Re etsa kgoeletso ho maAforika Borwa ohle ho tlotla nalane ya rona le ho tswelapele ho matlafatsa nepahalo ya demokrasi ya rona.

Kabinete e etsa kgoeletso ho bakgethi bohle ho netefatsa hore dikgetho tsa 2014 di fihlella lenane le leholo la bakgethi, jwaloka ha re bone dikgethong tse ding le tse ding haesale ho tloha ka 1994. Dikantoro tsohle tsa Lefapha la Merero ya Lehae di tla be di butswe ka matsatsi a boingodiso ho nolofatsa mosebetsi wa boingodiso ka ditokomane tse hlokehang.

1.6  Kabinete e thoholetsa balefi ba lekgetho ba fetang dimilione tse 3.6 ba ileng ba kenya dipusetso tsa bona tsa Lekgetho, ba thusang mmuso ho fana ka ditshebeletso tse hlokehang haholo tsa ho ntlafatsa maphelo a maAforika Borwa ohle. Kajeno Aforika Borwa ke naha e betere ho feta seo e neng e le sona ka 1994, ka lebaka la lenane le holang la dikhamphane le batho ba ikemiseditseng ho kenya letsoho kotleng ya mmuso moo ditshebeletso le disebediswa di lefellwang ho tswa ho yona.

Kabinete e hopotsa balefi ba lekgetho ba ntseng ba lokela ho kenya dipusetso tsa bona tsa lekgetho hore mohla la 22 Pudungwana 2013 ke letsatsi la ho qetela la ho kenya dipusetso ka mokgwa wa elektroniki bakeng sa pusetso ya motho ka mong ya selemo sa ditjhelete sa 2012/13, mme letsatsi leo le ntse le atamela ka potlako.

1.7  Kabinete e tshwela ka mathe sewa sa moraorao sa merusu, tshenyo ya thepa ya poraefete le ya setjhaba esita le ho hapa thepa ka mahahapa Gauteng, Kapa Botjhabela le Kapa Bophirima. Kabinete e thoholetsa sepolesa ka ho kena dipakeng le ho tsitsisa maemo.

Tshenyo ya thepa ya setjhaba e tima dibaka ditshebeletso tsa setjhaba tse hlokehang haholo ebile e thea keketseho ya morwalo wa disebediswa tse tshwepholelwang ho lokisweng ha ditshebeletso tsena.

Mmuso o ke ke wa dumella batlodi ba molao ho senya meaho le disebediswa moo ditshebeletso tsa bohlokwa di etsetswang teng. Tshenyo e mpempe ha e thuse mang kapa mang, ebile ha ho moelelo tabeng ya ho senya tshebeletso enngwe ha ho tsekwa enngwe.

Setjhaba se tshwanela ho lwantsha matsapa ohle a diketso tsa ditlolo tsa molao tsa ho thopa thuto ya bana ba rona ka ditshoso tsa ho sitisa ditlhahlobo.
Re tshwanela ho aha dibaka moo re hlomphanang re bile re thusana, ho e na le ho tshabana.

1.8 Kabinete e hlahisitse ngongoreho ya yona ka dipolelo tse etswang phatlalatsa mabapi le Majuda a Aforika Borwa. Kabinete e hlompha tokelo ya batho ba Palestina ya boipuso esita le tokelo ya Isiraele ya ho phedisana le naha ya Palestina. Matsapa a bona a toka a ho fumana tharollo kgohlanong eo a tshwanela ho tswelapele. Setjhaba sa Majuda a Aforika Borwa ha se a tshwanela ho tshoha letho. Mmuso ha o a qobella thibelo ya bahlanka ba mmuso ya ho ya Isiraele.

1.9 Kabinete e thoholetsa dikatleho tsa baatlelete ba Aforika Borwa ba sa tswa kena tlhodisanong ya Lebelo La Mokoka la Toropokgolo ya New York, moo Moatlelete ya Holofetseng, Ernst van Dyk, a tswileng boemong ba bobedi karolong ya ba sebedisang ditulo tse nang le mabidi, ha Lusapho April a tswile borarong mokgahlelong wa banna ba sa holofalang. Ho feta moo, Don Oliver le Abraham Mouton ba qetile ba le ho ba 60 ba kahodimo karolong ya dibaesekele tsa matsoho.

2. Dipuisano le diqeto tsa bohlokwa tsa Kabinete

2.1 Kabinete e ananetse hore Lekgotla la Bofofisi ba Difofane la Aforika Borwa (SACAA) le tla tshwara seboka sa pele sa Bongaka ba Difofaneng mmoho le Mokgatlo wa Matjhaba wa Bofofisi ba Difofane (ICAO) ka Hlakola 2014. Seboka se tla kgobokanya bahlanka ba taolo le Basebetsi ba tsa Bophelo bo Botle tikolohong ya difofaneng, ho tshohla diphephetso tseo lekala le tobaneng le tsona.

Ho tshwara seboka ho tla thusa ho phahamisa tlhokomediso le ho kgothaletsa Dinaha tsa Aforika ho thakelana tsebo mabapi le tshebetso ya setlamo sa basebetsi ba difofaneng, polokeho ya bona le ya bapalami difofaneng esita le bolaodi ba polokeho.

3 Dithonyo

Kabinete e ananetse dithonyo tse latelang:

3.1 Mme Kholofelo Glorious Sedibe mosebetsing wa Motlatsi wa Molaodikakaretso: Mesebetsing ya Boetapele le Bolaodi Kantorong ya Khomishene ya Tshebeletso ya Setjhaba (OPSC).
