1. Vhuimo ha Khabinethe nga ha mafhungo a ndeme kha nyimele ya zwino

1.1. Khabinethe yo ṱanganedza muvhigo wa Goldman Sachs, wa Fumbili ya Miṅwaha ya  Mbofholowo, une wa sumbedzisa masia a fumi nga vhuḓalo ane nga khao Afurika Tshipembe ḽo vha na mvelaphanḓa u bva nga 1994 na masia a fumi ane nga khao shango ḽa khou tea u dzhia maga a tsheo.

Muvhigo uyu ndi thendelo ya u ṱanganedza ndingo ya muvhuso wone uṋe kha kushumele kwa shango, u dovha wa khwaṱhisa zwi dzhielwaho nṱha zwa vhumatshelo zwo tiwaho kha Puḽane ya Mveledziso ya Lushaka.

Muvhigo uyu ndi tshiko tsha fulufhelo na ṱhuṱhuwedzo wa dovha wa shela mulenzhe zwihulwane kha tshitshavha kha tshifhinga tsha musi Afurika Tshipembe ḽo thoma u pembelela Miṅwaha ya 20 ya Mbofholowo.

Muvhuso u khou ṱuṱuwedza maAfurika Tshipembe uri vha shumisane nahone vho khwaṱhisa uri Afurika Tshipembe ḽi bvelele na uri vha ṱanganye nungo dzavho na nyambedzano dzavho kha mafhungo aya.

1.2  Khabinethe yo ṱanganedza u thomiwa ha yunivesithi ntswa mbili ngei Kapa Devhula na Mpumalanga, dzine dza vha dza u thoma u fhaṱwa u bva tshe demokirasi ya vha hone.

Yunivesithi ya Mpumalanga i ḓo vula nga Phando 2014. U bvelela uhu hu sumbedzisa ndeme ya muvhuso na maAfurika Tshipembe kha u ḓikwamanya na pfunzo sa tshipiḓa tsha mveledziso ya lushaka. Khamphasi ntswa i ḓo konisa matshudeni vha Mpumalanga uri vha gude tsini na hayani na u bveledza zwinzhi zwa ikonomi zwi shumiswaho nga yunivesithi na vhathu vhayo.

Afurika Tshipembe ḽo ita mvelaphanḓa khulwane vhukuma kha u ṋetshedza tshumelo na zwiimiswa zwiswa  kha zwitshavha zwi shayaho. U khwakhiswa ha zwiimiswa izwo ho humiselisa murahu zwinzhi zwe ra swikelela kha demokirasi yashu ntswa.

1.3. Khabinethe yo dovha ya ṱanganedza nḓivhadzo ya uri puḽanti dza muḓagasi wa  Eskom, Kusile na  Medupi dzi ḓo ṋetshedza muḓagasi nga 2014.

U bveledziswa ha muḓagasi ṅwakani nga puḽanti dza muḓagasi ya Kusile na Medupi zwi ḓo dovha zwa sumbedzisa u bvelela huhulwane ha shango na u bvelela ha shango kha u thoma mbekanyamushumo ya mveledziso ya themamveledziso khulwane ine ya khou fha ikonomi na tshitshavha tshivhumbeo tshavhuḓi.

Fulufulu ḽi bvelelaho ndi ḽa ndeme miṱani na kha khasiṱama dza nḓowetshumo na uri ḽi vula khonadzeo ntswa dza u alusa ikonomi– zwe zwa vha zwone zwo dzhielwaho nṱha kha vhuvhusi uvhu u bva nga 2009. Musi hu na thandela dzi fanaho na idzi dzire kati u mona na shango, Afurika Tshipembe ḽo dzhia maga a ndeme o vhalaho kha nḓila yaḽo ntswa ya u aluwa, na uri maAfurika Tshipembe vhoṱhe vha tea u pembelela zwe ra swikelela.

1.4. Khabinethe i khoḓa u sainiwa ha Mulanga wa Thandela ya Muḓagasi wa Maḓi wa Grand Inga, zwezwino kha Madalo a Tshiofisi nga Muphuresidennde Vho Jacob Zuma kha Riphabuḽiki ya Demokirasi ya Congo. Thandela iyi i ḓo engedza tswikelelo ya fulufulu Afurika ya ḓisa mbuelo khulwane kha zwa vhulimi, migodi na dziṅwe sekithara kha dzingu ḽa SADC.

Musi hezwi zwoṱhe zwo no fhela, thandela i ḓo ḓisa fulufulu ḽi vhuedzedzeaho kha   hafu ya dzhango ḽa Afurika. Thandela iyi i ḓo sika mishumo, mveledziso ya zwikili na u pfukiselwa ha thekhinoḽodzhi kha mashango o dzhenelelaho ya dovha ya vhuedza miṱa na vhashumisi vha nḓowetshumo. Izwi ndi zwa ndeme kha nyaluwo ya ikonomi na vhuṱaṱisani u mona na dzingu ḽa SADC.

1.5. Khabinethe i ṱuṱuwedza maAfurika Tshipembe vhoṱhe, nga mannḓa vhaswa na vhane vha khou voutha lwa u thoma, uri vha ite nḓowenḓowe ya pfanelo dzavho dza demokirasi nga u dzhia ḽiga ḽa u thoma ḽa u ḓiṅwalisela u voutha nga dzi 9 na dzi10 Ḽara 2013.

Nndwa yashu ya mbofholowo i tendela muAfurika Tshipembe muṅwe na muṅwe a tshi shela mulenzhe kha sisiṱeme yashu ya demokirasi. Ri vhidza maAfurika Tshipembe vhoṱhe uri vha hulise ḓivhazwakale yashu na u isa phanḓa na u khwaṱhisa vhungoho ha demokirasi yashu.

Khabinethe i vhidza vhavouthi vhoṱhe uri vha vhone uri khetho dza 2014 dzi vhe na tshivhalo tshihulwane tsha u voutha, sa zwe ra vhona kha khetho dzoṱhe u bva 1994. Ofisi dzoṱhe dza Muhasho wa zwa Muno dzi ḓo vha dzo vula nga maḓuvha a u ḓiṅwalisa u itela u tshimbidza maitele a u ḓiṅwalisa nga u vhekanya zwo teaho.
  
1.6. Khabinethe i khoḓa vhatheli vha fhiraho 3,6 miḽioni vho ḓadzaho khumelamurahu dza muthelo u swika zwino, zwi thusaho muvhuso u ṋetshedza tshumelo dzi ṱoḓeyesaho kha u khwinisa matshilo a maAfurika Tshipembe. Afurika Tshipembe ndi fhethu ha khwine ṋamusi u fhirisa zwe ḽa vha ḽi zwone nga 1994, zwi tshi khou itiswa nga u engedzea ha vhathu na khamphani dzine dzo ḓiṋekedzela na u kona u shela mulenzhe kha tshikwama tsha muvhuso tshine nga khatsho ha lambedzwa tshumelo na zwishumiswa.

Khabinethe i humbudza vhatheli vhane vha kha ḓi tea u ṋetshedza khumelamurahu dzavho dza muthelo uri dzi 22 Ḽara 2013 ḽine ḽa vha ḓuvha ḽa u vala kha faela ya eḽekiṱhironiki ya khumelamurahu dza muthelo wa iwe muṋe wa ṅwaha wa muthelo wa  2012/13 ḽi khou sendela nga u ṱavhanya.

1.7. Khabinethe i sathula gabelo ḽa khakhathi dza zwenezwino, u tshinyadzwa ha ndaka ya phuraivethe na ya muvhuso na u gevhengwa ha thundu ngei Gauteng, Kapa Vhubvaḓuvha na Kapa Vhukovhela. Khabinethe i khoḓa mapholisa kha u dzhenelela havho vha dzikisa nyimelo iyo.

U tshinyadzwa ha ndaka ya muvhuso zwi thivhela zwitshavha zwi ṱoḓesaho tshumelo ya muvhuso zwa ḓisa nyengedzedzo ya mutsiko kha zwishumiswa zwine zwa tea u vhuedzedzwa uri tshumelo ivhe hone.

Muvhuso a u nga tendeli zwigevhenga zwi tshi tshinyadza zwifhaṱo na zwishumiswa fhethu hune ho iswa tshumelo. Tshinyadzo a i thusi muthu, na uri a zwi pfali u tshinyadza tshumelo ngeno u tshi khou ṱoḓa iṅwe.

Zwitshavha zwi tea u lwa nga nḓila dzoṱhe kha zwiito zwa vhutshinyi u itela u tsireledza pfunzo ya vhana vhashu kha vha shushedzaho u thithisa milingo.
Ri tea u fhaṱa zwitshavha zwine ra ṱhonifhana na u thusana, hu si u ofhana.

1.8. Khabinethe yo sumbedzisa u kwamea nga tshitatamennde tsha tshitshavha tsha Majuta vha Afurika Tshipembe. Khabinethe yo limuwa ngaha pfanelo ya vhathu vha Paḽestina ya vhuḓiimiseli ha vhuṋe na pfanelo ya Isiraele ya u vha hone havho  kha shango ḽa Paḽestina. Vhuḓidini havho kha u wana thandululo ya nndwa yavho hu ḓo tea uya phanḓa.  Tshitshavha tsha Madzhuta a Afurika Tshipembe a vha ngo tea u ofha tshithu. Muvhuso a u ngo iledza u endela Isiralele kha vhaofisiri vha muvhuso.

1.9. Khabinethe yo saḽutha tswikelelo dza vhagidimi vha Afurika Tshipembe vho dzhenelelaho muṱaṱisano wa zwezwino ngei New York City Marathon, he Paralympian Vho Ernest van Dyk vha wina vhuimo ha vhuvhili kha khethekanyo ya wiḽitshee, ngeno Vho Lusapho April vho dzhia vhuimo ha vhuraru kha khethekanyo ya  ngwena dza vhanna. U engedza kha zwenezwo, Vho Don Oliver na Vho Abraham Mouton vho ṱhaphudza kha vha 60 vha nṱhesa kha khethekanyo ya hand cycle.

2. Nyambedzano na tsheo dza ndeme dza khabinethe

2.1 Khabinethe yo tendela uri Mannḓalanga a Vhufhufhi ha Afurika Tshipembe (SACAA) na Dzangano ḽa Vhufhufhi ha Dzitshaka (ICAO) zwi vhe ṋemuḓi wa Khonfarentsi ya u thoma ya Mishonga ya Nḓowetshumo ya mabupo a Afurika nga Luhuhi 2014. I ḓo ṱanganya mannḓalanga a ndango na Vhashumi vha Mutakalo kha mupo wa Nḓowetshumo ya mabupo, u amba ngaha khaedu dzi kwamaho sekithara iyo.

U vha ṋemuḓi wa vhuṱambo uvho zwi ḓo thusa kha u ḓivhisa na u ṱuṱuwedza mashango a Afurika uri vha kovhelane mafhungo a elanaho na kushumele kwa muyani, tsireledzo ya muyani na vhaṋameli vha mabupo khathihi na vhulangi ha tsireledzo.

3. U tholwa

Khabinethe yo tendela u tholwa ha vha tevhelaho:
3.1 Vho Kholofelo Glorious Sedibe kha poso ya Mufarisa Mulanguli Muhulwane: Nḓowedzo ya Vhurangaphanḓa na Vhulanguli kha Ofisi ya Khomishini ya Tshumelo dza Tshitshavha (OPSC).
