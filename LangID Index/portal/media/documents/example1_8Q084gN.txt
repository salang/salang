4:25 Amehlo akho makakhangele athi gca, Neenkophe zakho zithi nzo kokuphambi kwakho.

4:26 Lungelelanisa umkhondo wonyawo lwakho, Zonke iindlela zakho zizimaseke.

4:27 Musa ukuthi gu bucala ngasekunene nangasekhohlo; Susa unyawo lwakho entweni embi.

5:1 Nyana wam, bubazele indlebe ubulumko bam, Ingqondo yam uyithobele indlebe yakho;

5:2 Ukuze ugcine iminkqangiyelo, Ulondoloze ukwazi umlomo wakho.

5:3 Ngokuba ivuza incindi yobusi imilebe yomlomo womkamntu, Libuthelezi ngaphezu kweoli ikhuhlangubo lakhe;

5:4 Ke ukuphela kwakhe kukrakra njengomhlonyane, Kubukhali njengekrele elintlangothi mbini.

5:5 Iinyawo zakhe zihla ziye ekufeni, Amabanga akhe abambelele kwelabafileyo.

5:6 Akangeze wahamba ngomendo wobomi; Imikhondo yakhe iyajikajika, engazi.

5:7 Ke ngoku, bonyana, ndiveni, Ningemki entethweni yomlomo wam.

5:8 Mayibe kude kuye indlela yakho, Ungasondeli emnyango wendlu yakhe;

5:9 Hleze uyinike abanye intlahla yakho, Uyinike isijorha iminyaka yakho;

5:10 Hleze kuhluthe abasemzini ngamandla akho, Kungene endlwini yowolunye uhlanga ukubulaleka kwakho;
