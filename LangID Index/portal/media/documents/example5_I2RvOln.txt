3:10 Ozala amaqonga akho bubuninzi, Nemikhombe yakho yokukhongozela iphuphume iwayini entsha.

3:11 Uqeqesho lukaYehova, nyana wam, musa ukulucekisa, Ungakruquki sisohlwayo sakhe;

3:12 Kuba lo amthandayo uYehova uyamohlwaya, Njengoyise esohlwaya unyana akholwayo nguye.

3:13 Hayi, uyolo lomntu ofumene ubulumko, Lomntu ozuze ingqondo!

3:14 Kuba lulungile urhwebo lwabo ngaphezu korhwebo lwesilivere, Nongeniselo lwabo ngaphezu kwegolide embiweyo.

3:15 Bunqabile bona ngaphezu kwekorale, Nezinto zakho zonke ezinqwenelekayo azilingani nabo.

3:16 Imihla emide isesandleni sabo sokunene, Kwesokhohlo sabo bubutyebi nozuko.

3:17 Iindlela zabo ziindlela ezimnandi, Wonke umendo wabo uluxolo.

3:18 Bungumthi wobomi kwababambelela kubo, Unoyolo obubambayo.

3:19 UYehova waliseka ihlabathi ngobulumko, Walizinzisa izulu ngengqondo.

3:20 Agqobhoka amanzi anzongonzongo ngokwazi kwakhe, Savuza umbethe isibhakabhaka.

3:21 Nyana wam, ezi nto mazingemki emehlweni akho: Londoloza uzimaso nomnkqangiyelo.
