Ukubumba iHlangano yokuVikela umLilo

Okuphathelene | Okufanele ukwenze | Isikhathi esibekiweko | Kubiza malini | Amaforomo ekufanele uwazalise | Ungathintana nobani

Mayelana nokubumba iHlangano yokuVikela umLilo

Ungabumba iHlangano yokuVikela umLilo (i-FPA) ukubonela phambili, ukukhandela, ukulawula kanye nokucima imililo yommango  endawenakho nangabe:

umnikazi wendawo
umqatjhi
umPhathi  oPhezulu wesiphathiswa sendawo
ukhethwe ukobana alawule indawo yombuso (kufakwe hlangana indawo yamaSotja weliZwe weSewula Afrika)
ukulawula inarha ngaphasi kwemilayo yeKhotho ePhakemeko.
Kufanele uhlale endaweni:

eba nemililo eminengi  nanyana
eba sengozini yemililo yommango nanyana 
Enamahlathi nanyana iimila.
Angeze wakghona ukubumba ihlangano uwedwa. Kufanele nibe siqhema sabantu abawela ngaphakathi kwekambiso evezwa ngehla, esihlala endaweni eyodwa.

Imisebenzakho njengeHlangano eVikela umLilo kuzakuba:

kuthuthukisa nokusebenzisa amaqhinga wokuphatha imililo yommango endawenakho
kuqala ubujamo bemvelo obungabangela umlilo
amahlandla amanengi ukhulume  ngeenlinganiso zengozi yomlilo kumalunga endawenakho
kuhlela nokubandula amalunga endawenakho mayelana nokulwa, ukuphatha kanye nokukhandela umlilo
kunikela uNgqongqotjhe wezeLimo, amaHlathi kanye neenHlambi (i-DAFF) iimbalobalo ezimayelana nemililo yommango endawenakho
kunikela ilwazi elikhonjelwa nguNgqongqotjhe mayelana nokukhandela imililo yommango
Okufanele ukwenze

Biza umhlangano nalabo bantu  abakufaneleko abanekareko
Nangabe ukhona umsebenzi oqalene nomlilo, kufanele unikele inothisi etlolwe phasi kuhlangano leyo ofuna ukuyibumba okungenye eyihlangano yokuvikela umlilo bese umema isiPhathiswa sabo esiPhezulu ukobana sikhambele umhlangano.
Tlola amaminidi womhlangano, azakuveza lokhu:

Ukobana iye nanyana awa kube khona isekelo elibumbeneko mayelana nokubumba ihlangano yokuvikela umlilo
Inani lamavowudu  akhambisana nokubumba ihlangano
Inani lamavowudu elingakhambisaniko nokubunjwa kwehlangano; begodu
Inani labantu abagodle amavowudwabo mayelana nokubumba ihlangano.
Tlola bewutlikitle isivumelwano esitlolwe phasi esitjhoko bona uyingcenye yehlangano
Khetha usihlalo nesigungu sekomidi
Nikela ibizo lehlangano mayelana nokuvikela umlilo kanye nehlathululo yendawakho
Iba nerejista yabakhambele umhlanganio kanye nemininingwana abangathintwa kiyo kufakwe hlangana iimphande zeemposo zabo boke abantu
Khupha bese uthumela iForomo 1 ngaphasi kwamaforomo ukobana ulizalise, mayelana nokuphunyeleliswa yi-DAFF. Nangabe i-DAFF iphumelelisa isibawo sakho,  yeke kufanele ukhuphe bewuthumele iforomo 2 bese ufaka lokhu okulandelako:
amaminidi womhlangano
umthethosisekelo we-FPA begodu
Nomtlamo webhizinisi we-FPA, ofaka hlangana amaqhinga wokuphatha imililo yommango kanye nemithetho.
Isikhathi esibekiweko

Asikho isikhathi esibekwako

Kubiza malini

Umsebenzi usimahla

Amaforomo ekufanele uwazalise

(Amaforomo anikelwe ngefomede ye- PDF.  Ukuvula imitlolo ye- PDF, kuzakutlhogeka bona ube ne- Adobe Acrobat Reader 4  nanyana  engaphezulu efakwe kukhomphyuthakho.)

Iforomo 1: Application for registration of a fire protection association (Isibawo sokurejistara ihlangano yokuvikela umlilo)
Iforomo 2: Application for the registration of a fire protection association (Isibawo sokurejistara ihlangano yokuvikela umlilo)