1. Tincumo teKhabhinethi etindzabeni letigcamile kulesimo sanyalo

1.1 IKhabhinethi yemukele umbiko weGoldman Sachs, weNkhululeko Yeminyaka Lengemashumi Lamabili, lelandzisa kabanti ngemikhakha lelishumi iNingizimu Afrika leyente kuyo inchubekel’embili kusukela nga-1994 kanye nemikhakha lelishumi lelive lelifanele liyitsatsele tinyatselo.

Lombiko usiciniseko lesemukelekako seluhlolo lolwentiwe nguhulumende lucobo lwakhe lwendlela lelive lelisebente ngayo futsi icinisekisa tintfo letitawubekwa embili esikhatsini lesitako letibekwe kuLuhlelo Lwekutfutfukisa Lwavelonkhe.

Lombiko ungumtfombo welitsemba nenshisekelo futsi uligalelo lelibalulekile enkhulumeni yemmango esikhatsini lapho khona iNingizimu Afrika icale kugubha umkhosi weNkhululeko Yeminyaka lenge-20.
