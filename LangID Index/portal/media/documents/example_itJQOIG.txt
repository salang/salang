1. Isimo seKhabhinethi ezindabeni ezisemqoka esimweni samanje

1.1 IKhabhinethi yawamukela umbiko wabakwa-Goldman Sachs, Weminyaka Engamashumi Amabili Yenkululeko, okuwumbiko onikeza incazelo ebanzi futhi ejulile yemikhakha eyishumi lapho iNingizimu Afrika yenze khona inqubekela phambili enkulu kusukela ngowe-1994 kanye nemikhakha eyishumi lapho kudingeka khona ukuthi leli lizwe lithathe izinyathelo ezinqala.

Lo mbiko uhamba ezinyathelweni zokuhlola nokuhlaziya okwenziwe nguhulumeni ngokwakhe mayelana nendlela leli lizwe elisebenze ngayo futhi ugcizelela izinto ezibekwe eqhulwini okumele zenziwe esikhathini esizayo ezibekwe ngokucacile Ohlelweni Lokuthuthukiswa Kwezwe.

Lo mbiko ungumthombo wethemba nogqozi futhi unegalelo elibalulekile kwinkulumompikiswano yomphakathi esikhathini lapho seyiqalile iNingizimu Afrika ukugubha iMinyaka engama-20 Yenkululeko.

Uhulumeni unxusa abantu baseNingizimu Afrika ukuthi basebenzisane futhi bazikhandle ukwenza iNingizimu Afrika iphumelele kakhulu kunakuqala, bafake umfutho wabo kulezi zindaba futhi baxoxisane kabanzi ngazo.

1.2 IKhabhinethi iyakwamukela ukusungulwa kwamanyuvesi amabili eNyakatho Kapa kanye naseMpumalanga, okungamanyuvesi okuqala akhiwe selokhu kwafika ukubusa ngokwentando yeningi.

INyuvesi yaseMpumalanga izovulwa ngoMasingana 2014. Lo msebenzi ofezekisiwe wokwakhiwa kwale nyuvesi ukhombisa ukuthi uhulumeni kanye nabo bonke abantu baseNingizimu Afrika bayakubona ukubaluleka kwemfundo njengengxenye yokuthuthukiswa kwesizwe. Inyuvesi entsha izokwenza ukuthi abafundi baseMpumalanga bafunde eduze kwamakhaya abo futhi izosungula amathuba amaningi ezomnotho azovuleka ngenxa yesidingo sokuhlinzeka ngezinsiza kule nyuvesi nakubantu abayisebenzisayo.

INingizimu Afrika seyenze inqubekelaphambili enkulu ekuhlinzekeni izidingongqangi kanye nezikhungo ezintsha emiphakathini esemazingeni aphansi futhi entulayo. Isenzo sokucekelwa phansi kwalezi zikhungo yisenzo esibuyisela emuva izinto ezisemqoka ezizuzwe yithina sonke esikhathini sombuso wethu omusha wentando yeningi.

1.3  IKhabhinethi iyasamukela futhi nesimemezelo sokuthi iziphehlamandla zabwakwa-Eskom, iKusile kanye ne-Medupi, zizoqala ukukhiqiza ugesi ngowezi-2014.

Ukukhiqizwa kukagesi okuzokwenziwa yiziphehlamandla, iKusile kanye ne-Medupi, kuzoba ngesinye isenzo esiyingqophamlando kwintuthuko yaleli lizwe futhi kuzokhombisa impumelelo kahulumeni ekuqalisweni kohlelo olukhulukazi lokuthuthukiswa kwengqalasizinda oseluguqule isimo somnotho wethu kanye nomphakathi.

Ukutholakala kukagesi ngendlela esimeme kusemqoka kumakhasimende asebenzisa ugesi emakhaya kanye nalawo asebenzisa ugesi ezimbonini futhi kuvula amathuba amasha okukhulisa umnotho – okuyinto lo hulumeni ophethe ayibeke eqhulwini kusukela ngowezi-2009. Njengoba kunemiklamo eminingi yalolu hlobo eqhubekayo ezweni lonke, iNingizimu Afrika seyithathe izinyathelo eziningana ezisemqoka emgudwini omusha wokukhuliswa komnotho, futhi bonke abantu baseNingizimu Afrika kumele bazithokozele lezi zinto ezifezekisiwe.

1.4 IKhabhinethi ikushayela ihlombe ukusayindwa kweSivumelwano esiphathelene noMklamo Wokuphehlwa Kukagesi Wamanzi eDamini i-Grand Inga,  esasayindwa ngenkathi uMongameli Jacob Zuma esoHambweni Olusemthethweni lwe-Democratic Republic of Congo. Lo mklamo uzokwelula ukutholakala kukagesi e-Afrika futhi uzoletha imihlomulo emikhulukazi kwezolimo, kwezezimayini kanye nakweminye imikhakha esifundeni se-SADC.

Uma sekuphothulwe zonke izigaba zawo, lo mklamo uzoletha ugesi ovuselelekayo kwingxenye eyisigamu sezwekazi lase-Afrika. Lo mklamo uzogqugquzela ukuvulwa kwamathuba emisebenzi, ukuthuthukiswa kwamakhono kanye nokudluliswa kwezobuchwepheshe emazweni abambe iqhaza futhi kuzohlomulisa abasebenzisi bagesi emakhaya nasezimbonini. Lokhu kusemqoka kakhulu ekukhulisweni komnotho kanye nokuncintisana esifundeni se-SADC sonkana.

1.5 IKhabhinethi ikhuthaza bonke abantu baseNingizimu Afrika, ikakhulu intsha kanye nalabo abazobe bevota okokuqala, ukuthi basebenzise ilungelo labo lentando yeningi ngokuthi bathathe isinyathelo sokuqala, okuyisinyathelo sokubhalisela ukhetho mhla ziyisi-9 kanye namhla ziyi-10 Lwezi 2013.

Umzabalazo wethu wenkululeko walethela wonke umuntu waseNingizimu Afrika ilungelo lokubamba iqhaza elibonakalayo ohlelweni lwethu lwentando yeningi. Sihlaba ikhwelo eliqondiswe kubo bonke abantu baseNingizimu Afrika lokuthi bawuhloniphe umlando wethu futhi baqhubeke nokuqinisa igunya lethu lokubusa ngokwentando yeningi.

IKhabhinethi ihlaba ikhwelo eliqondiswe kubo bonke abavoti lokuthi baqinisekise ukuthi baphuma ngobuningi babo bavote okhethweni lowezi-2014, njengoba sibonile kwenzeka kusukela ngowe-1994. Wonke amahhovisi oMnyango Wezasekhaya azobe evuliwe ngezinsuku zokubhalisela ukhetho ukuze abantu abafuna ukubhalisela ukhetho bahlinzekwe ngezincwadi ezidingekayo.

1.6  IKhabhinethi iyabancoma abakhokhi bentela abangaphezu kwezigidi ezi-3,6 asebegcwalise futhi bathumela amafomu abo entela kuze kube manje, futhi ngalokho balekelela uhulumeni ukuthi akwazi ukuhlinzeka bonke abantu baseNingizimu Afrika ngezidingongqangi abazidinga kakhulu ukuze baphucule izimpilo zabo. Namhlanje iNingizimu Afrika iyizwe elingcono kakhulu uma kuqhathaniswa nesimo ebikusona ngowe-1994, ngenxa yesibalo esikhulayo sabantu nezinkampani ezizimisele futhi ezinawo amandla okufaka isandla emgodleni kahulumeni wokuxhasa ngezimali ukuhlinzekwa kwezidingongqangi kanye nezikhungo zomphakathi.

IKhabhinethi iyabakhumbuza abakhokhi bentela abangakawathumeli amafomu abo entela ukuthi akusensuku zatshwala ngaphambi kokufika kosuku lokugcina lokuthunyelwa kwamafomu entela omuntu ngamunye onyaka wentela wezi-2012/13 ngohleloxhumano, okungumhla zingama-22 Lwezi 2013.

1.7  IKhabhinethi iyazigxeka izigameko zodlame oluqubuke ezindaweni eziningi esikhathini esifushane esedlule, nokucekelwa phansi kwempahla yabahlali nekahulumeni kanye nokuphangwa kwempahla eGauteng, eMpumalanga Kapa kanye naseNtshonalanga Kapa. IKhabhinethi iyawancoma amaphoyisa ngokungenelela kwawo kanye nokuletha uzinzo nokubuyisela isimo kwesijwayelekile.

Ukucekelwa phansi kwempahla kahulumeni kuphuca imiphakathi izidingongqangi ezidingeka kakhulu futhi kuholela ekutheni kube nesidingo sokuthi uhulumeni athathe izimali kwezinye izinhlelo zikahulumeni ngenhloso yokuvuselela nokwakha kabusha izikhungo zikahulumeni ezicekelwe phansi futhi ukuze kuhlinzekwe ngezidingongqangi eziphazamisekile.

Uhulumeni ngeke neze avumele izigelekeqe ukuthi zicekele phansi izikhungo okuhlinzekwa kuzona izidingongqangi ezibalulekile. Ukucekelwa phansi kwempahla akusizi muntu, futhi akunangqondo ukucekela phansi impahla ethile ngenkathi ufuna ukuhlinzekwa ngenye.

Imiphakathi kumele ilwisane nayo yonke imizamo yezigelekeqe yokuphazamisa imfundo yezingane zethu ngokuthi zisabise ngokuphazamisa ukuhlolwa kokuphela konyaka.

Kumele sakhe imiphakathi lapho sihloniphana futhi sisizane khona, esikhundleni sokusabana sodwa.

1.8  IKhabhinethi izwakalise ukukhathazeka ngezitatimende ezenziwe emphakathini mayelana namaJuda aseNingizimu Afrika. IKhabhinethi iyalihlonipha ilungelo labantu base-Palestine likazimele geqe nokuziphatha ngokwabo kanye nelungelo lezwe lakwa-Israel lokuba likwazi ukuphilisana nombuso wase-Palestine. Kumele iqhubeke imizamo yalawa mazwe yokuthola isixazululo esingachemile kulokhu kungqubuzana. Umphakathi wamaJuda aseNingizimu Afrika kumele ungabi novalo. Asikho isinqumo esithathwe nguhulumeni sokuvimbela izikhulu zikahulumeni ukuthi zingavakasheli kwelakwa-Israel.

1.9 IKhabhinethi iwuthulela isigqoko umsebenzi oncomekayo owenziwe abagijimi baseNingizimu Afrika abebebambe iqhaza eMjahweni Webanga Elide Wedolobha lase-New York, lapho uMgijimi wama-Olimpiki Emkhakheni Wabakhubazekile u-Ernst van Dyk ethathe khona indawo yesibili emkhakheni wabahamba ngezitulo zabaphila nokukhubazeka, kanti uLusapho April ungene endaweni yesithathu emkhakheni ophezulu wabesilisa. Ngaphezu kwalokho, u-Don Oliver kanye no-Abraham Mouton babe yingxenye yabokuqala abangama-60 emkhakheni wezamabhayisikili ashovwa ngezandla.

2. Izingxoxo nezinqumo zeKhabhinethi eziqavile

2.1 IKhabhinethi yakwamukela ukuthi iNhlangano Ephathelene Nokulawulwa Kwamabhanoyi yaseNingizimu Afrika (i-SACAA) izosingatha ingqungquthela yoku-1 yase-Afrika yeZokwelapha Emkhakheni Wezokundiza Ngamabhanoyi ngokubambisana neNhlangano Yezamabhanoyi Yamazwe Ngamazwe (i-ICAO) ezobanjwa ngoNhlolanja 2014. Le ngqungquthela izoqoqela ndawoye iziphathimandla ezilawulayo kanye naBasebenzi Bezempilo abasebenza ngaphansi komkhakha wezokundiza ngamabhanoyi ukuze baxoxisane ngezinselelo lo mkhakha obhekene nazo.

Ukusingathwa kwale ngqungquthela kuzolekelela ngokugqugquzela ukuqwashiswa nokukhuthazwa kwamazwe ase-Afrika ukuthi abelane ngolwazi oluphathelene nezinga lomsebenzi owenziwa abasebenzi abasebenza ngaphakathi emabhanoyini, nokuphepha kwabasebenzi nabagibeli ngenkathi bendiza emoyeni futhi kwabelwane ngolwazi oluphathelene nokuphathwa kwezokuphepha.

3. Abaqashiwe

IKhabhinethi yakwamukela ukuqashwa kwalaba abalandelayo:

3.1 Nksz. Kholofelo Glorious Sedibe esikhundleni sokuba yiPhini likaMqondisi-Jikelele: Ophikweni Lwemisebenzi Yobuholi Nokuphatha eHhovisi Lekhomishana Yezemisebenzi Kahulumeni (i-OPSC).
