1:22 Kunini na, ziyathandini, nithanda ubuyatha, Abagxeki befuna ukugxeka, Izinyabi zithiya ukwazi?

1:23 Nithe nabuyela ekohlwayeni kwam, Niyabona, ndowumpompozisela kuni umoya wam, Ndonazisa amazwi am.

1:24 Ngokokuba ndinibizile, anavuma, Ndolule isandla sam, akwabakho ukhathalayo;

1:25 Nesuka naliyeka necebo lam lonke, Anavuma ndakunohlwaya.

1:26 Nam ke ndonihleka ekusindekeni kwenu; Ndogculela ekufikeni kokunkwantyiswa kwenu;

1:27 Ekufikeni kokunkwantyiswa kwenu njengesithonga, Ekuzeni kokusindeka kwenu njengesaqhwithi, Ekufikelweni kwenu yimbandezelo nenkcutheko.

1:28 Baya kwandula bandibize, ndingaphenduli, Bandifune kwakusasa, bangandifumani;

1:29 Ngethuba lokuba bakuthiyile ukwazi, Abakunyula ukoyika uYehova;

1:30 Abalivuma icebo lam, Basigiba sonke isohlwayo sam:

1:31 Baya kudla ke isiqhamo sendlela yabo, Bahluthe ngamacebo abo.

1:32 Kuba ukuphamba kweziyatha kuyazi bulala, Nobunqobo bezinyabi buyazitshabala lisa.

1:33 Ke yena ondivayo uhleli ekholosile, Wonwabile ekunkwantyiseni kobubi.

2:1 Nyana wam, ukuba uthe wazamkela iintetho zam, Wayiqwebela kuwe imithetho yam,

2:2 Ukuze uyibazele ubulumko indlebe yakho, Uyithobele ukuqonda intliziyo yakho;

2:3 Ewe, ukuba uthe wayibiza ingqondo, Walisa ekuqondeni ilizwi lakho;

2:4 Ukuba uthe wabufuna njengesilivere, Wabumba njengobutyebi obuselelweyo:

2:5 Uya kwandula ukukuqonda ukoyika uYehova, Ukufumane ukumazi uThixo,


