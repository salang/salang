“Morogakwa ke yo a botilego batho,
yo a botilego maatla a motho wa nama,
ke yo a mphuralelago, nna, Morena.
6O swana le mohlašana leganateng,
wo o melago lesehleng la lešoka,
tswaing, mo go sa dulego motho;
ga a na le mahlatse.
7“Go lehlogonolo yo a botilego nna, Morena,
yo a ithekgilego ka nna.
8  O bjalo ka mohlare wo o bjetšwego kgauswi le moela,
wo medu ya wona e nabelago meetseng.
Ga o tšhoge phišo,
gomme matlakala a wona a dula e le a matala;
le ka ngwaga wa komelelo ga o tshwenyege,
ga o lese go enywa.
9“Menagano ya motho e radia go feta tšohle,
ke ye kgopo; e ka kwešišwa ke mang?
10  Ke nna, Morena,
ke lekolago menagano,
ke hlahlobago dipelo tša batho,
gore ke putse motho yo mongwe le yo mongwe
ka se se mo lebanego,
go ya ka tšeo a di dirago.”
