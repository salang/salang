1. Maemo a kabinete ka ga merero ye e lego ditabeng ka seemong sa bjale

1.1 Kabinete e amogetše pego ya Goldman Sachs, Mengwagasome ye Mebedi ya Tokologo, yeo e hlalošago ka botlalo mafelo a 10 ao Afrika Borwa e gatetšego pele sebopegong sa ona go tloga ka 1994 le mafelo a 10 ao naga e swanelago go tšea magato a ka pela mo go ona.

Pego ke bohlatse bja kamogelo ya boitekolo bja naga bja mošomo wa mmušo ka noši gomme e tiiša dintlhakgolo tša ka moso tše di lego ka gare ga Leanotlhabollo la Bosetšhaba.

Pego ke mothopo wa tshepho le tlhohleletšo gomme ke kgato ye bohlokwa poledišanong ya setšhaba ka nako ya ge Afrika Borwa e thomile go keteka Mengwaga ye 20 ya Tokologo.

Mmušo o hlohleletša maAfrika Borwa go šomišana le go šoma ka maatla go dira gore Afrika Borwa e atlege le go feta le go lebiša maatla le dipoledišano tša bona go ditabana tše.

1.2 Kabinete e amogela go hlongwa ga diyunibesithi tše pedi tše diswa go la Kapa Leboa le Mpumalanga, tša mathomo tša go agwa go tloga mola re fihlelelago temokrasi.

Yunibesithi ya Mpumalanga e tla bula ka Pherekgong 2014. Katlego ye e laetša bohlokwa bjo mmušo le maAfrika Borwa ka moka ba bo beago thutong bjalo ka karolo ya tlhabollo ya setšhaba. Gape khamphase ye mpsha e tla kgontšha baithuti ba Mpumalanga go ithuta kgauswi le gae gomme e tla hlola bontši ba mediro ya ekonomi ye e tlogo šomela yunibesithi le setšhaba sa yona.

Afrika Borwa e gatetše pele kudu kabelong ya ditirelo le diinstitšhušene tše mpsha ditšhabeng tša go hloka le go phaelwa thoko. Tshenyo ya diinstitšhušene tšeo e bušetša morago dipoelo tše kgolo tše re di fihleletšego mehleng ya temokrasi ya rena ye mpsha.

1.3  Gape Kabinete e amogela tsebišo ya gore dipolante tša mohlagase tša Eskom Kusile le Medupi di tla aba mohlagase ka 2014.

Go fehla mohlagase ngwaga wo o tlago ka dipolante tša mohlagase tša Kusile le Medupi e tla ba katlego ye nngwe ye kgolo ya tlhabollo ya naga gomme e tla laetša katlego ya mmušo phethagatšong ya lenaneo la tlhabollo ya mananeokgoparara ka bontši yeo e bopagoleswa ekonomi le setšhaba sa rena.

Enetši ya go lotega e bohlokwa go badiriši ba intasteri le ba ka malapeng gomme e bula dikgonagalo tše diswa tša go godiša ekonomi – yeo e bego e ntše e le ntlhakgolo ya pušo ye go tloga ka 2009. Ka diprotšeke tše ntšintši tša mohuta wo tše di tšwelago pele go dikologa naga, Afrika Borwa e tšeere dikgato tša go balega tše bohlokwa gotee le mokgwa wa yona wo moswa wa kgolo, gomme maAfrika Borwa ka moka a swanela go keteka dikatlego tša rena.

1.4  Kabinete e tumiša go saenelwa ga Kwano ka ga Protšeke ya Mohlagase wa Meetse ya Grand Inga, ke Mopresidente Jacob Zuma malobanyana ge a be a le Ketelong ya Semmušo go la Repabliki ya Temokrasi ya Congo. Protšeke ye e tla oketša phihlelelo ya mohlagase mo Afrika ebile e na le mehola ye mentši go tša temo, meepo le makala a mangwe ka seleteng sa Dinaga tše di Hlabologago tša Borwa bja Afrika (SADC).

Ge dikgato ka moka di feleletše, protšeke e tla tliša mohlagase wa go tsošološwa go seripa sa kontinente ya Afrika. Protšeke ye e tla hlohleletša tlholo ya mešomo, tlhabollo ya mabokgoni le phetišetšo ya theknolotši dinageng tše di kgathago tema gomme e tla hola bašomiši ba intasteri le ba ka malapeng. Se se bohlokwa kgolong ya ekonomi le bophadišanong go kgabaganya selete sa SADC.

1.5  Kabinete e hlohleletša maAfrika Borwa ka moka, kudu ba bannyane le bakgethi ba go tlo thoma, go phethagatša tokelo ya bona ya temokrasi ka go tšea kgato ya mathomo ya go ingwadišetša dikgetho ka la 9 le 10 Dibatsela 2013.

Ntwa ya rena ya tokologo e šireleditše tokelo ya moAfrika Borwa o mongwe le o mongwe ya go ba bakgathatema ba mafolofolo ka temokrasing ya rena ye mpsha. Re ipiletša go maAfrika Borwa ka moka go hlompha histori ya rena le go tšwela pele go matlafatša bokgonthe bja temokrasi ya rena.

Kabinete e ipiletša go bakgethi ka moka go kgonthišiša gore dikgetho tša 2014 di fihlelela palo ya godimo ya bakgethi, bjalo ka ge re bone dikgethong tše dingwe le tše dingwe go tloga ka 1994. Dikantoro ka moka tša Kgoro ya Merero ya Selegae di tla bulwa ka matšatši a boingwadišo go sepetša tshepedišo ya boingwadišo ka ditokomane tša maleba.

1.6  Kabinete e leboga batšhedi ba motšhelo ba go feta dimilione tše 3,6 bao ba šetšego ba rometše dipego tša motšhelo go fihla ga bjale, go thuša mmušo go aba ditirelo tšeo di nyakegago kudu go kaonafatša maphelo a maAfrika Borwa ka moka. Afrika Borwa ke lefelo le le kaone kudu lehono go phala ka mo e bego e le ka gona ka 1994, ka lebaka la palo ya batho le dikhamphani tše di golago tšeo di ratago le go kgona go fa mmušo ditseka tša go thekga ditirelo le ditlabakelo ka ditšhelete.

Kabinete e gopotša batšhedi ba motšhelo bao ba sa šaletšego morago go romela dipego tša bona tša motšhelo gore la 22 Dibatsela 2013 ke letšatši la mafelelo la batho la go romela dipego tša motšhelo ka elektroniki gobane ngwaga wa motšhelo wa 2012/13 o batamela ka lebelo.

1.7  Kabinete e kgala ka bogale leuba la bjale la dikgaruru, tshenyo ya dithoto tša praebete le tša setšhaba le go thopa diphahlo go la Gauteng, Kapa Bohlabela le Kapa Bodikela. Kabinete e tumiša maphodisa ge a tsene bogare le go fediša dikgaruru.

Tshenyo ya thoto ya setšhaba e tima ditšhaba ditirelo tša setšhaba tše di nyakwago kudu gomme e hlola morwalo wa tlaleletšo wa go fapoša methopo o bušetša le go mpshafatša ditirelo tše.

Mmušo o ka se dumelele basenyi go senya meago le ditlabakelo fao ditirelo tša bohlokwa di abjago. Tshenyo ya ka boomo ga e thuše motho, gomme ga go kwagale go senya tirelo ye nngwe go kgopela ye nngwe.

Ditšhaba di swanetše go lwantšha maiteko a basenyi a go thibela thuto ya bana ba rena ka go tšhošetša go hlakahlakanya ditlhahlobo.

Re swanetše go aga ditšhaba fao re hlomphanago le go thušana, go ena le go tšhabana.

1.8  Kabinete e laeditše hlobaelo ka dipego setšhabeng mabapi le Majuda a Afrika Borwa. Kabinete e amogela tokelo ya batho ba Palestina ya boipušo le tokelo ya Israele ya go ba ka lehlakoreng la pušo ya Palestina. Maiteko a bona a go hwetša tharollo ya thulano yeo e tla bago ya toka a swanela go tšwela pele. Setšhaba sa Majuda sa Afrika Borwa ga se a swanela go tšhoga selo. Mmušo ga se o beye kiletšo maetong a go ya Israele ka bahlankedi ba mmušo.

1.9 Kabinete e amogela dikatlego tša baatlete ba Afrika Borwa bao ba bego ba phadišana malobanyana go Lebelo le le Telele la Toropokgolo ya New York, moo Moatlelete wa Boditšhabatšhaba wa Mogolofadi Ernst van Dyk a tšwilego maemong a bobedi ka karolong ya setulothethi sa bagolofadi, mola Lusapho April a tšwilego maemong a boraro ka legorong la lebelo la bao ba senago bogole la banna. Go tlaleletša, Don Oliver le Abraham Mouton ba feditše gare ga ba 60 ba mathomo ka karolong ya dibaesekele tša diatla.

2.  Dipoledišano le diphetho tša bohlokwa tša Kabinete

2.1 Kabinete e dumeletše Lekala la Bofofane la Badudi ba Afrika Borwa (SACAA) go šomišana le Mokgatlo wa Bofofane la Badudi ba Boditšhabatšhaba (ICAO) go swara Khonferentshe ya Mathomo ya Kalafo le Difofane tša Afrika ka Dibokwane 2014. E tla kopanya makala a taolo le Bahlankedi ba tša Maphelo ka tikologong ya diphofo go boledišana ka ditlhohlo tše di lebanego le lekala.

Go swara tiragalo go tla thuša go godiša temogo le go hlohleletša Mebušo ya Afrika go abelana tshedimošo ka ga mošomo wa bašomi ba ka gare ga difofane, polokego ya bašomi ba ka gare ga difofane le banamedi ka sefofaneg le yona taolo ya polokego.

3. Bao ba thwetšwego mošomong

Kabinete e amogetše go thwalwa mošomong ga batho ba ba latelago:

3.1 Mohumagadi Kholofelo Glorious Sedibe o thwetšwe mošomong wa maemo a Motlatšamolaodipharephare: Mešomong ya Bolaodi le Boetapele ka Kantorong ya Khomišene ya Tirelo ya Setšhaba (OPSC).
