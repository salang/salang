1. Seemo sa Kabinete mo mererong e e kwa setlhoeng mo maemong a ga jaanong

1.1 Kabinete e amogetse pegelo ya ga Goldman Sach, ya Tokologo ya Dingwagasome di le Pedi, e e leng pegelo e e nang le dintlha ka botlalo ka ga dikarolo di le 10 tseo Aforika Borwa e gatetseng pele go utlwagala mo go tsone fa e sale ka ngwaga wa 1994 mmogo le dikarolo di le 10 tseo naga e tshwanetseng go tsaya ditshweetso tse di botlhokwa mo go tsone.

Pegelo eo ke tlhomamiso ya pulamadibogo ya tlhatlhobo ya puso ka boyona ya kgatelopele ya naga mme e netefatsa dintlhapele tsa isago tse di tlhalositsweng mo Leano la Tlhabololo la Bosetšhaba.

Pegelo e ke motswedi wa tsholofelo le thotloetso ebile ke seabe se se botlhokwa mo dipuisanong tsa bosetšhaba mo nakong eo Aforika Borwa e setseng e simolotse go keteka Dingwaga di le 20 tsa Kgololesego.

Puso e rotloetsa maAforika Borwa go dirisana mmogo le go dira ka natla gore Aforika Borwa e atlege le go feta mmogo le go tsepamisa maikatlapelo a bona le dipuisano tsa bona mo dintlhaphisegong tse.

1.2 Kabinete e amogela go tlhongwa ga diyunibesithi tse pedi tse dintšhwa kwa Kapa Bokone le kwa Mpumalanga, e leng tsone tsa ntlha tsa go agwa fa e sale ka tshimologo ya temokerasi.

Yunibesithi ya Mpumalanga e tla bulwa ka Ferikgong 2014. Phitlhelelo e e bontsha botlhokwa jo puso le maAforika Borwa otlhe a bo golaganyang le thuto jaaka karolo ya tlhabololo le tsweletso ya setšhaba. Khamphase e ntšhwa e tla dira gore baithuti ba Mpumalanga ba kgone go ithutela gaufi le magae a bone mme gape e tla tlhola mefutafuta ya ditiro tse di ikaegileng mo ikonoming tse di ikaelelang go direla yunibesithi le batho ba yona.

Aforika Borwa e dirile kgatelopele e kgolo mo tlamelong ya ditirelo le ditheo tse dintšhwa mo baaging ba ba kgetholotsweng le ba ba tlhokang. Tshenyo ya ditheo tse e busetsa morago dipoelo tse di bonalang tse re di fitlheletseng mo temokeraseng ya rona e ntšhwa.

1.3  Kabinete e tswelela go amogela kitsiso ya gore mafelo a phetlhelomotlakase a Eskom e leng la Kusile le la Medupi a tla tlamela ka motlakase mo ngwageng wa 2014.

Phetlho ya motlakase mo ngwageng o o tlang ka mafelo a phetlhelomotlakase e leng la Kusile le la Medupi a tla nna kgato e nngwe e kgolo ya tlhabololo le tsweletso ya naga mme e tla bontsha katlego ya puso mo go tsenyeng tirisong lenaane le legolo la tlhabololo ya mafaratlhatlha le le bopang sešwa ikonomi le setšhaba sa borona.

Maatla a leruri a botlhokwa mo bareking ba fa gae le ba madirelo mme a bula ditšhono tse dintšhwa tsa kgodiso ya ikonomi – e leng ntlha e e saleng e ntse e le ntlhapele ya puso ya ga jaanong fa e sale go tloga ka 2009. Ka tswelelo ya diporojeke tse dintsi tsa mofuta o mo nageng yotlhe, Aforika Borwa e tshotse dikgato di le mmalwa tsa botlhokwa mo tselaneng ya yona e ntšhwa ya kgolo, mme maAforika Borwa otlhe a tshwanetse go keteka diphitlhelelo tsa rona.

1.4 Kabinete e itumeletse go saeniwa ga Tumelano ya Kagisano ya Porojeke ya Motlakase o o fetlhiwang ka metsi ya Grand Inga, ka nako ya Leeto la Sešweng la ga Moporesidente Jacob Zuma kwa nageng ya Rephaboliki ya Temokerasi ya Congo. Porojeke e e tla atolosa phitlhelelo ya maatla a motlakase mo Aforika le go tlhagisa mesola e megolo ya temothuo, meepo le maphata a mangwe mo kgaolong ya Dinaga tsa Mokgatlho wa Tlhabololo ya Borwa jwa Aforika (SADC).

Fa dikgato tsotlhe di weditswe, porojeke e tla tlisa ntšhwafatso ya motlakase go tlamela halofo ya kontinente ya Aforika ka motlakase. Porojeke e e tla itlhaganedisa go tlholwa ga ditiro, tsweletso ya bokgoni le diphetisetso tsa thekenoloji mo dinageng tse di tsayang karolo mme e tla tswela mosola badirisi ba fa gae le ba madirelo. Se se botlhokwa mo kgolong ya ikonomi le mowa wa kgaisano go ralala kgaolo ya SADC.

1.5 Kabinete e rotloetsa maAforika Borwa otlhe, segolo bogolo batlhophi ba bašwa le ba ba tla bong ba tlhopha lekgetlho la ntlha, go dirisa tshwanelo ya bona ya temokerasi ka go tsaya kgato ya ntlha ya go ikwadisetsa go tlhopha ka la 9 le 10 Ngwanaitseele 2013.

Kgaratlho ya rona ya go bona kgololesego e netefaditse tshwanelo ya moAforika Borwa mongwe le mongwe go nna batsayakarolo ba ba matlhagatlhaga mo tsamaisong ya temokerasi ya rona. Re kopa maAforika Borwa otlhe go tlotla hisetori ya rona le go tswelela go o tiisa boammaruri jwa temokerasi ya rona.

Kabinete e kopa batlhophi botlhe go netefatsa gore ditlhopho tsa 2014 di fitlhelela thologelo e kgolo ya batho ba ba tlhophang, fela jaaka re setse re bone mo ditlhophong dingwe le dingwe fa e sale go tloga ka 1994. Dikantoro tsotlhe tsa Lefapha la Merero ya Selegae le tla bula ka matlha otlhe a kwadiso go nolofatsa mokgwatsamaiso wa kwadiso ka ditokomane tsa botlhokwa.

1.6  Kabinete e akgola baduela lekgetho ba ba fetang dimilione di le 3,6 ba ba tladitseng le go busa diforomo tsa bona tsa lekgetho go fitlha ga jaanong, ka ba thusa puso go tlamela ditirelo tse di tlhokegang thata go tokafatsa matshelo a maAforika Borwa otlhe. Aforika Borwa ke lefelo le le botoka thata gompieno go feta jaaka le ne le ntse ka 1994, ka ntlha ya palo e e golang ya batho ka nosi le ditlamo tse di nang le maikemisetso le bokgoni jwa go abela kgetsana ya baagi e go abiwang matlole a ditirelo le tshegetso ya baagi go tswa mo go yone.

Kabinete e gakolola baduela lekgetho ba ba iseng ba romele diforomo tsa bona tsa lekgetho gore letlha la bofelo la 22 Ngwanaitseele 2013 la go busa diforomo tsa bona tsa motho ka nosi ka tsela ya ileketeroniki mo ngwageng wa lekgetho wa 2012/13 le atamela ka lebelo.

1.7  Kabinete e kgala palo e e kwa godimo ya sešweng ya tirisodikgoka, tshenyo ya dithoto tsa poraefete le tsa botlhe le go utswa dithoto kwa Gauteng, Kapa Botlhaba le Kapa Bophirima. Kabinete e akgola mapodisa ka go tsereganya le go lolamisa maemo.

Tshenyo ya dithoto tsa botlhe e tlhaeletsa baagi go fitlhelela ditirelo tse baagi ba di tlhokang thata mme gape e baka mathata a mangwe a mafaratlhatlha a a dirisiwang ka mokgwa yo o fapogileng ka maitlhomo a go busetsa le go a tsenya mo legatong la ditirelo tseo.

Puso ga e kitla e letla disenyi go senya dikago le ditlhokwa tseo go rebolwang ditirelo tse di botlhokwa mo go tsone. Tshenyo ya dithoto ga e thuse ope, e bile ga go na tlhaloganyo mo go senyeng tirelo nngwe ka nako ya go batla e nngwe.
Baagi ba tshwanetse go lwantsha maiteko otlhe a ditlhopha tsa bosenyi a go batla go tshwara kanaanyo thuto ya bana ba rona ka go tshosetsa le go kgoreletsa ditlhatlhobo.

Re tshwanetse go aga setšhaba se mo go sona re tlotlang le go thusa ba bangwe, go na le go tshabana.

1.8 Kabinete e supile go tshwenngwa ke dipeelo tsa mo phatlalatseng malebana le Bajuta ba mo Aforika Borwa. Kabinete e tlotla tshwanelo ya batho ba Palesetina ya kgololesego ya go bona boipuso le tshwanelo ya Iseraele ya go ikamanya le naga ya Palesetina. Maiteko a bona a go bona tharabololo e e siameng ya kgotlhang a tshwanetse go tswelela. Morafe wa Bajuta wa mono Aforika Borwa ga o tlhoke go tshaba sepe. Puso ga e a rebola kiletso mo maetong a batlhankedi ba puso go ya kwa Iseraele.

1.9 Kabinete e rolela hutshe diphitlhelelo tsa baatlelete ba Aforika Borwa ba ba neng ba gaisana sešweng mo kgaisanong ya mabelo ya kwa New York City, moo Ernst van Dyk a fentseng mo maemong a bobedi mo karolong ya dituloteti, fa Lusapho April a tswile mo maemong a boraro mo setlhopheng sa maemo a a kwa godimo sa banna. Mo godimo ga moo, Don Oliver le Abraham Mouton ba feditse mo go ba le 60 ba ntlha mo karolong ya dibaesekele tsa diatla.

2. Dipuisano le ditshweetso tse di botlhokwa tsa Kabinete

2.1 Kabinete e rebotse gore Bothati jwa Aforika Borwa jwa Difofane tsa Baagi (SACAA) bo tshware mmogo le Mokgatlho wa Boditšhabatšhaba wa Difofane tsa Baagi (ICAO) khonferense ya ntlha ya Aforika ya Boitekanelo le Difofane ka Tlhakole 2014. E tla kopanya bothati jwa taolo le Badiri ba Pholo mo karolong ya diphofo go buisana ka dikgwetlho tse di lebaneng lephata la bona.

Go tshwara tiragalo e go tla thusa go tsweletsa temoso le go rotloetsa Dinaga tsa Aforika go aroganya tshedimosetso malebana le tiro ya badiri ba difofane, pabalesego ya badiri ba sefofane le bapalami mo sefofaneng mmogo le taolo ya pabalesego.

3. Go thapiwa

Kabinete e rebotse go thapiwa gwa ba ba latelang:

3.1 Mme Kholofelo Glorious Sedibe mo tirong ya Motlatsamokaedikakaretso: Boeteledipele le Mekgwatiriso ya Botsamaisi mo Kantorong ya Khomišene ya Tirelo ya Setšhaba (OPSC).
