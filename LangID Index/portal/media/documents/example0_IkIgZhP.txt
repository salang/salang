“Ndisebenzisa iikawusi xa ndisexesheni” [1]
Dadewethu, uninzi lwethu bafundi abangamantombazana siphoswa zizifundo xa sisexesheni ngenxa yokungabinamali yokuthenga iiPads. Kuleminyaka miHlanu(5) igqithileyo, uMongameli wethu wathembisa ukunikezela ngeePads simahla kwabo bahlelelekileyo [2], sisalinde ukuzaliseka kweso sithembiso nangoku.

Singakutshintsha oku, thina bantu abangoomama abantsundu sibaninzi apha eMzantsi-Afrika, ukuba singabambisana, sime kunye, singaqinisekisa ukuba abaPhathiswa bamasebe ezeMfundo, iNtlalontle kunye nelezeMpilo, bayasifezekisa esi sithembiso senziwa nguMongameli uZuma.

Xa kuthelekelelwa, amantombazana afunda ibanga leShumi ngo 2015, aphoswa ziintsuku ezingama-300 zesikolo [3], oku kubangela abantu abangomama bangafumani mathuba emisebenzi kwaye bahleleleke ngakumbi.

Baphathiswa ababekekileyo Angie Motshekga, Aaron Motsoaledi kunye noBathabile Dlamini

Thina mantombazana alapha eMzantsi-Afrika kunye nabo basixhasayo, simemelela kuni ukuba nizalisekise isithembiso esenziwa nguMongameli wethu uZuma sokunikela ngeePads nezinye izinto ezisincedayo xa sisexesheni kuthi mantombazana afundayo angakwazi kufikelela kumaxabiso azo. Impilo yethu, imfundo, ingqesho kunye nesidima sethu zisezandleni zenu. Sicela ukuba niqinisekise ukuba xa kuqala iKota yezifundo yokugqibela enyakeni apha ku-2016, (October 10th), akukho ntombazana iyakuphoswa zizifundo okanye inyanzeleke ukuba isebenzise iikawusi kuba ingenamali yokuthenga iiPads.
