Mbombela – Owesilisa osolwa ngokudlwengula intombazanyana, 6, utshele inkantolo yaseMpumalanga ukuthi wafaka “induku” yakhe esithweni sangasese sengane bedlala imizi.

 “Sasidlala nje kuphela nale ngane emjondolo wami. Yona yayingumama mina ngingubaba ngesikhathi sidlala imizi,” kusho lo wesilisa oneminyaka engu-60 ubudala etshela inkantolo ngoLwesibili.

Ngeke likhishwe igama lakhe ukuze kuvikeleke umntwana.

“Angizange ngiyenze lutho, ngahlikihla ubuntu bami esithweni sayo sangasese kuphela sizidlalela,” kusho leli khehla.

OLUNYE UDABA: Kuboshwe udokotela ngelokudlwengula isiguli

Le ndoda zayikhalela ngoJulayi ngemuva kokuthi umndeni wengane uyivulele icala lokudlwengula amantombazane amabili.

Le ndoda ingeyokudabuka eMozambique kanti isihlale kuleli iminyaka emithathu.

Ithe ayazi lutho ngale enye ingane okusolakala ukuthi nayo yayidlwengula.

“Ngiyazi ukuthi engakwenza kuyicala elibomvu, ngangingaqondile lutho olubi. Ngiyalivuma icala lami. Le ngane ihlobene nonkosikazi wami,” esho.

Uzogqunywa esitokisini kuze kube ubuyela enkantolo futhi.

- News 24
