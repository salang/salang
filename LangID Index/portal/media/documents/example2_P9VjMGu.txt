4:12 Ekuhambeni kwakho akayi kuxinana amabanga akho; Nokuba uthe wagidima, akuyi kukhubeka.

4:13 Bambelela eluqeqeshweni, musa ukuluyeka; Lulondoloze, ngokuba lububomi bakho.

4:14 Musa ukungena emendweni wabangendawo, Unganyatheli endleleni yabanobubi.

4:15 Yiyeke; musa ukuya kugqitha kuyo; Yicezele, ugqithe.

4:16 Kuba abalali, ukuba abathanga benze ububi; Bemkelwa bubuthongo ukuba abathanga bakhubekise abanye.

4:17 Kuba badla isonka sokungendawo, Basele iwayini yogonyamelo.

4:18 Ke wona umendo wamalungisa unjengokukhanya komso, Okuya kuba mhlophe ngokuba mhlophe, kude kube semini enkulu.

4:19 Indlela yabangendawo injengesithokothoko; Abayazi into abakhubeka kuyo.

4:20 Nyana wam, wabazele indlebe amazwi am; Intetho yam yithobele indlebe yakho.

4:21 Mayingemki emehlweni akho; Yigcine entliziyweni yakho ngaphakathi.

4:22 Kuba ibubomi kwabayifumanayo, Nempiliso enyameni yabo yonke.

4:23 Nazintweni zonke zigcinwayo, londoloza intliziyo yakho; Kuba aphuma kuyo amathende obomi.

4:24 Kususe kuwe ukujibiliza komlomo, Nokuthi gu bucala komlomo kudedise kuwe.
