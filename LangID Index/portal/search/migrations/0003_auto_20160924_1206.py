# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-24 12:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('search', '0002_auto_20160922_0657'),
    ]

    operations = [
        migrations.AlterField(
            model_name='search',
            name='data',
            field=models.TextField(blank=True, null=True),
        ),
    ]
