from urllib2 import *
import urllib2
import simplejson #sudo easy_install simplejson
import json
import sys
import string
import datetime



def updateIndex(s_fileName, collection):
    # UPDATE SOLR INDEX WITH UPDATED RESULT JSON FILE#
    try:
        jsonfile = open(s_fileName, 'r')
        entry = jsonfile.readlines()
        req = urllib2.Request('http://192.168.0.8:8983/solr/' + collection + '/update/json/docs')
        #req = urllib2.Request('http://localhost:8983/solr/' + collection + '/update/json/docs')
        req.add_header('Content-Type', 'application/json')
        urllib2.urlopen(req, string.join(entry))
        jsonfile.close()
    except Exception as inst:
        log_file = open('solrUpdate_error_log.txt', 'wr')
        log_file.write(str(type(inst)) + '\n')
        log_file.write(str(inst.args) + '\n')
        log_file.write(str(inst) + '\n')
        log_file.close()
        pass

def increment(collection, id):
    reload(sys)
    sys.setdefaultencoding('utf8')
    #GET RESULT INFORMATION FROM SOLR USING SOLR QUERY#
    connection = urlopen('http://192.168.0.8:8983/solr/' + collection + '/select?q="' + id +'"&wt=json')
    #connection = urlopen('http://localhost:8983/solr/' + collection + '/select?q="' + id +'"&wt=json')
    response = simplejson.load(connection)
    #CREATE JSON FILE WITH UPDATED RESULT INFORMATION#
    i_randNum = random.randrange(9999999999)
    s_fileName = str(response['response']['docs'][0]['_version_']) + '_' + str(i_randNum) + '.json'
    f_tempFile = open(s_fileName, 'wr')
    f_tempFile.write('{\n')
    f_tempFile.write('\t"tstamp":' + str('["' + response['response']['docs'][0]['tstamp'][0] + '"],\n'))
    f_tempFile.write('\t"segment":' + str('[' + str(response['response']['docs'][0]['segment'][0]) + '],\n'))
    f_tempFile.write('\t"digest":' + str('["' + response['response']['docs'][0]['digest'][0] + '"],\n'))
    f_tempFile.write('\t"host":' + str('["' + response['response']['docs'][0]['host'][0] + '"],\n'))
    f_tempFile.write('\t"boost":' + str('[' + str(response['response']['docs'][0]['boost'][0]) + '],\n'))
    f_tempFile.write('\t"id":"' + str(response['response']['docs'][0]['id']) + '",\n')
    f_tempFile.write('\t"title":' + str('["' + response['response']['docs'][0]['title'][0] + '"],\n'))
    f_tempFile.write('\t"url":' + str('["' + response['response']['docs'][0]['url'][0] + '"],\n'))
    f_tempFile.write('\t"content":' + str('["' + response['response']['docs'][0]['content'][0].encode('utf-8') + '"],\n'))
    if 'visits' in response['response']['docs'][0]:
        f_tempFile.write('\t"visits":' + str((response['response']['docs'][0]['visits']+1)) + ',\n')
    else:
        f_tempFile.write('\t"visits":' + str(1) + ',\n')
    f_tempFile.write('\t"_version_":' + str(response['response']['docs'][0]['_version_']) + '\n')
    f_tempFile.write('}')
    f_tempFile.close()
    updateIndex(s_fileName, collection)
    #DELETE THE JSON FILE#
    os.remove(s_fileName)

def createJson(data):
    fileName = "tempFile.json"
    f = open(fileName, "w")
    date = data['tstamp'].strftime('%Y-%m-%d %H:%M:%S')
    f.write('{\n')
    f.write('\t"tstamp":' + str('["' + date + '"],\n'))
    f.write('\t"host":' + str('["' + data['host'] + '"],\n'))
    f.write('\t"id":"' + str(data['id']) + '",\n')
    f.write('\t"title":' + str('["' + data['title'] + '"],\n'))
    f.write('\t"url":' + str('["' + data['url'] + '"],\n'))
    f.write('\t"content":' + str('["'+ data['content'] + '"],\n'))
    f.write('\t"description":' + str('["' + data['description'] + '"],\n'))
    f.write('\t"visits":' + str(data['visits']) + ',\n')
    f.write("}")
    f.close()
    # mybb should be the name of the language
    updateIndex(fileName,"mybb")
    os.remove(fileName)