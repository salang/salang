import sys
import string
import datetime
import math
from search.models import Search
from authentication.models import Account

#Store the relevance result into one string which will be processed when subitted to file.
def relevanceVote(vote, pos, id_):
    record = Account.objects.get(id=id_)
    record.alonData = str(record.alonData)+ pos + ':' + vote + '#'
    record.save()

#Store the reported result in a string which will be written to file when subitted.
def reportLanguage(url, collection, id_):
    record = Account.objects.get(id=id_)
    record.osherData = str(record.osherData) + '\n' + url + ' - ' + collection
    record.save()

#Clear the relevance data from the backend.
def clearData(id_):
    record = Account.objects.get(id=id_)
    record.alonData = ''
    record.osherData = ''
    record.save()

#Remove the last # charater from the relevance string.
def removeLastChar(id_):
    record = Account.objects.get(id=id_)
    record.alonData = str(record.alonData)[:-1]
    record.save()

#Turn the string with relevance results into an array for relevance calculations.
def getVotesArray(id_):
    votesArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    record = Account.objects.get(id=id_)
    votes = record.alonData.split('#')
    for vote in votes:
        part = vote.split(':')
        votesArray[int(part[0])] = int(part[1])
    return votesArray

#Calculate Mean Average Precision of results.
def calcMAP(votes):
    relevantCount = 0.0
    totCount = 0.0
    total = 0.0
    for vote in votes:
        if vote == 1.0:
            relevantCount+=1.0
        totCount+=1.0
        total+=float(relevantCount/totCount)
    return float(total/10)

#Calculate Discounted cumulative Gain of results.
def calcDCG(votes):
    DCG = 0.0
    posCount = 0
    for vote in votes:
        posCount+=1
        DCG += (vote/(math.log(posCount + 1)))
    return DCG

#Calculate Normalized Discounted Cumulative Gain of results.
def calcNDCG(votes, DCG):
    votes.sort(reverse = True)
    IDCG = calcDCG(votes)
    if IDCG != 0:     
        return float(DCG/IDCG)
    else:
        return 0

#Submit/Write the results from the search query to file.
def submitEvaluation(query, languages, primaryLanguage, testMethod, id_):
    removeLastChar(id_)
    votes = getVotesArray(id_)
    dt = str(datetime.datetime.now())
    f = open(dt+'.txt', 'w')
    f.write('User query: ' + query + '\n' + 'Primary Language: ' + primaryLanguage + '\n' 
    + 'Result Languages: ' + languages + '\n' + 'Test method ' + testMethod + '\n')
    f.write(str(Account.objects.get(id=id_).alonData) + '\n')
    f.write(str(votes) + '\n') 
    f.write('MAP = ' + str(calcMAP(votes)) + '\n')
    DCG = calcDCG(votes)
    f.write('DCG = ' + str(DCG) + '\n')
    f.write('NDCG = ' + str(calcNDCG(votes, DCG)) + '\n')
    f.write(str('\n-------------------------------------\n' + Account.objects.get(id=id_).osherData) + '\n')
    f.close()
    clearData(id_)


