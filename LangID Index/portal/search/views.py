from rest_framework.response import Response
from uploads.models import Upload
from uploads.serializers import UploadSerializer
from rest_framework.decorators import api_view
from search import incrementIndexSolr, relevanceEvaluation
from rest_framework import permissions, viewsets, status

@api_view(['GET', ])
def increment_counter(request):
    collection = request.GET.get('collection')
    id_ = request.GET.get('id')
    incrementIndexSolr.increment(collection, id_)
    return Response({
            'status': 'Accepted Request',
            'message': 'Successful'
        }, status=status.HTTP_202_ACCEPTED)

@api_view(['GET', ])
def relevance_Vote(request):
    vote = request.GET.get('vote')
    pos = request.GET.get('pos')
    id_ = request.GET.get('id')
    relevanceEvaluation.relevanceVote(vote, pos, id_)
    return Response({
            'status': 'Accepted Request',
            'message': 'Successful'
        }, status=status.HTTP_202_ACCEPTED)

@api_view(['GET', ])
def report_Language(request):
    url = request.GET.get('url')
    collection = request.GET.get('collection')
    id_ = request.GET.get('id')
    relevanceEvaluation.reportLanguage(url, collection, id_)
    return Response({
            'status': 'Accepted Request',
            'message': 'Successful'
        }, status=status.HTTP_202_ACCEPTED)

@api_view(['GET', ])
def submit_Evaluation(request):
    query = request.GET.get('query')
    languages = request.GET.get('languages')
    primaryLanguage = request.GET.get('primaryLanguage')
    testMethod = request.GET.get('testMethod')
    id_ = request.GET.get('id')
    relevanceEvaluation.submitEvaluation(query, languages, primaryLanguage, testMethod, id_)
    return Response({
            'status': 'Accepted Request',
            'message': 'Successful'
        }, status=status.HTTP_202_ACCEPTED)

@api_view(['GET', ])
def clear_Data(request):
    id_ = request.GET.get('id')
    relevanceEvaluation.clearData(id_)
    return Response({
            'status': 'Accepted Request',
            'message': 'Successful'
        }, status=status.HTTP_202_ACCEPTED)


