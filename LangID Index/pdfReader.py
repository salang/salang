import textract
import sys
f2 = open("test.txt", "w")
f2.write("here\n")
f2.write(str(sys.argv[1]) + '\n')
f2.write(str(sys.argv[2]) + '\n')
f2.write(str(sys.argv[3]) + '\n')

if "True" in str(sys.argv[3]):
    f2.write("if 1")
    text = textract.process(sys.argv[1] + sys.argv[2])
    fileName = sys.argv[2][:-4]
    f = open('../PDFs/' + fileName + '.txt', 'w')
    f.write(text)
    f.close()
else:
    f2.write("if 2")
    text = textract.process(sys.argv[1] + sys.argv[2])
    fileName = sys.argv[2][:-4]
    f = open('PDFs/' + fileName + '.txt', 'w')
    f.write(text)
    f.close()
f2.close()