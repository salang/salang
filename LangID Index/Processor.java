import java.lang.Process;
import java.io.*;
public class Processor
{

    public Processor ()
    {

    }

    //process unix command and return output
	//Parameters:
	//	cmd - the command line string
	public String processCMD(String cmd)
	{
        //process to run unix command
		Process p = null;
		String output = "";

		//try catch for process and buffered reader
		try
		{
		 	//run unix command
			p = Runtime.getRuntime().exec(cmd);
			p.waitFor();

			//get output of command
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = "";
			while ((line = reader.readLine()) != null)
			{
				output = output + "\n"+ line ;
			}

			//System.out.println("Command done");
			reader.close();
		}

		catch (Exception ee)
		{
			System.out.println("Error in Processor: " + ee);
			output = "Error";
		}

		finally
		{
			p.destroyForcibly();
		}
		return output;
	}
}
