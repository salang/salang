import java.io.*;
public class CleanFile
{
    public CleanFile()
    {

    }

    public void clean(String fileName)
    {
        try
        {
            String result = "";
            BufferedReader reader = new BufferedReader (new FileReader (fileName));
            String line = reader.readLine();
            while (line != null)
            {
                String s [] = line.split ("\t");
                if(s.length > 1)
                {
                    if(!s[1].equals(" "))
                    {
                        result = result + s[1] + "\n";
                    }
                }

                line = reader.readLine();
            }

            reader.close();
            String s [] = fileName.split("\\.");
            BufferedWriter writer = new BufferedWriter (new FileWriter(s[0] + "_clean.txt"));
            writer.write(result);
            writer.close();
        }

        catch (Exception ee)
        {
            System.out.println("Error: " + ee);
        }
    }

    public static void main (String [] args)
    {
        CleanFile c = new CleanFile();
        c.clean(args[0]);
    }
}
