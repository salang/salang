####################################
# Name:		BasicLIDWrapper.pl
# Date:		2016-07-13
# Version:	1.0.1
# Project:	NCHLT-Text II
# OS:		DOS\Windows (Unix capable)
# Description:
# Author:	Justin Hocking
# Additional info:
# 	PERL wrapper to SALId module using sockets

use strict;
use Term::ANSIColor;
use Cwd qw( abs_path );
use File::Find;
use utf8;
use IO::Socket::INET;
if ($^O =~ /MSWin/g)
{
require Win32::Console::ANSI;
}

# For use when iterating through languages
#my %codes = ( 'af'=>'afrikaans', 'nr'=>'isindebele', 'nso'=>'sepedi', 'ss'=>'siswati', 'st'=>'sesotho', 'tn'=>'setswana', 'ts'=>'xitsonga', 've'=>'tshivenda', 'xh'=>'isixhosa', 'zu'=>'isizulu' );
#my %languages = ( 'afrikaans'=>'af', 'isindebele'=>'nr', 'sepedi'=>'nso', 'siswati'=>'ss', 'sesotho'=>'st', 'setswana'=>'tn', 'xitsonga'=>'ts', 'tshivenda'=>'ve', 'isixhosa'=>'xh', 'isizulu'=>'zu' );

my $filextension = "txt";

my $input;
my $output;

my $openTag = "{ \"text\" : \"";
my $closeTag = "\" , \"benchmark\" : 0 }";

my $port = 7770;

my $server = "laptop409";  # Host IP running the server
#my $server = "osher-pc";  # Host IP running the server

my $socket;

sub SetupSock
{
	# create the socket, connect to the port
	eval
	{
		$socket = new IO::Socket::INET (
			PeerHost => $server,
			PeerPort => $port,
			Proto => 'tcp',
			) or die "ERROR in Socket Creation : $!\n";
	};
	if ($@)
	{
		print color("red"), "Socket Error: $@\n", color("reset");
		print color("red"), "Attempting to start module!\n", color("reset");

		# Assuming it is in this dir
		if (-f "salid.exe" or -f "salid")
		{
			my $pid = fork();
			if ($pid == 0)
			{
				print color("blue"), "Sleeping for 10 seconds\n", color("reset");
				sleep(10);
				if (fork)
				{
					system("perl Script.CTexT.BasicLIDWrapper.1.0.1.JH.2016-07-13.pl $input $output $server $port");
					exit(0);
				}
			}
			else
			{
				exec("salid server -p $port");
			}
		}
	}
}

sub Main
{
	if (@ARGV > 3)
	{
		$input = $ARGV[0];
		$output = $ARGV[1];
		$server = $ARGV[2];
		$port = $ARGV[3];
	}
	elsif (@ARGV > 1 and @ARGV < 3)
	{
		$input = $ARGV[0];
		$output = $ARGV[1];
	}
	else
	{
		print color("blue"), "Input file/directory: ", color("reset");
		$input = <STDIN>;
		chomp($input);

		print color("blue"), "Output file: ", color("reset");
		$output = <STDIN>;
		chomp($output);

		print color("blue"), "Server IP: ", color("reset");
		$server = <STDIN>;
		chomp($server);

		print color("blue"), "Port: ", color("reset");
		$port = <STDIN>;
		chomp($port);
	}

	SetupSock;

	my @files;

	$input = abs_path($input);

	if (-d $input)
	{
		find (
			sub {
				my $file_name = $_;
				if ( -f $file_name and $file_name =~ /(.+)\.$filextension/) {
					push @files, $File::Find::name;
				}
			}, $input);
	}
	elsif (-f $input)
	{
		push @files, $input;
	}

	for my $file (@files)
	{
		open IN, "<:utf8", $file;
		open OUT, ">:utf8", $output;

		while (my $line = <IN>)
		{
			chomp($line);

			# Seeing as the text is already JSON serialized
			#my $request = $openTag . $line . $closeTag;
			my $request = $line;

			utf8::encode($request);

			$socket->send($request);

			my $response;
			$socket->recv($response,1024);

			print OUT "$file\t$response\n";
		}

		close IN;
		close OUT;
	}

}

Main;

$socket->close();
