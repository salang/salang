import java.io.*;
import java.util.LinkedList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Base64;

import org.json.JSONArray;
import org.json.JSONObject;
/**
 * Created by osher on 2016/08/22.
 */

public class SeedGenerator
{
    private LinkedList<String> wordList;
    private LinkedList<String> seedList;

    public SeedGenerator (String [] fileNames)
    {
        wordList = new LinkedList<>();
        seedList = new LinkedList<>();

        generateQueries(fileNames);
        System.out.println("Word List size = " + wordList.size());
        int count = 1;

        for(String word : wordList)
        {
            queryBing(word);
            System.out.println("COUNT = " + count);
            count++;
        }
        generateSeedList();

    }

    //generates queries by reading words from text file and places them into a list.
    public void generateQueries (String [] fileNames)
    {
        BufferedReader reader;
        for (int i = 0; i < fileNames.length; i++)
        {
            try
            {
                reader = new BufferedReader(new FileReader(fileNames[i]));
                String line ;
                //read line by line until no more lines
                while ((line = reader.readLine()) != null)
                {
                    wordList.add(line);
                }
                reader.close();

            }

            catch(Exception ee)
            {
                System.out.println("Error in generateQueries: " + ee);
            }
        }

    }

    public void generateSeedList()
    {
        System.out.println("Seedlist size: " + seedList.size());
        try
        {
            BufferedWriter writer = new BufferedWriter(new FileWriter("SeedList.txt", true));
            for (String url : seedList)
            {
                writer.write(url);
                writer.newLine();
            }
            writer.close();
        }

        catch (Exception ee)
        {
            System.out.println("Error in generateSeedList: " + ee);
        }
    }

    public void queryBing(String word)
    {
        try
        {
            String accountKey = "KeBurc4zt8a1YD5qSTr0hZt8Aev8acAjxNtbQHwDp4g";
            String bingUrlPattern = "https://api.datamarket.azure.com/Bing/Search/v1/Web?Query=%%27%s%%27&$format=JSON";

            String query = URLEncoder.encode(word, Charset.defaultCharset().name());
            String bingUrl = String.format(bingUrlPattern, query);

            String accountKeyEnc = Base64.getEncoder().encodeToString((accountKey + ":" + accountKey).getBytes());

            URL url = new URL(bingUrl);
            URLConnection connection = url.openConnection();
            connection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);


                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null)
                {
                    response.append(inputLine);
                }
                in.close();
                JSONObject json = new JSONObject(response.toString());
                JSONObject d = json.getJSONObject("d");
                JSONArray results = d.getJSONArray("results");
                int resultsLength = results.length();
                for (int i = 0; i < resultsLength; i++)
                {
                    JSONObject aResult = results.getJSONObject(i);
                    System.out.println(aResult.get("Url"));
                    seedList.add(aResult.get("Url") + "");
                }

        }

        catch (Exception ee)
        {
            System.out.println("Error in queryBing: " + ee);
        }
    }

    //args = file names
    public static void main (String [] args)
    {
        SeedGenerator seedGenerator = new SeedGenerator(args);
    }
}
