This is the readme for the SALANG IR Honours Project

Project Members:
Andreas von Holy
Osher Shuman
Alon Bresler

=============================================================================
=============================================================================

Running AngularJS (install is Windows specific):
-------------------------------------------------
1) Install node.js and NPM
node.js 5.x.x -> the latest one (6) apparently has bugs and I couldn't get it to work
NPM will install at the same time.
https://nodejs.org/dist/v5.9.1/node-v5.9.1-x64.msi

2) In the directory on the AngularJS website (i.e. salang/Web Portal), shift+rightClick and open command line.

3) In command line --> npm install (you will get an error that bower couldn't install)

4) In command line --> npm install -g bower

5) open git bash in the Web Portal directory and type bower install

6) In command line --> npm install (now bower will install properly)

7) To start the local server in command line --> npm start

8) Open chrome and type localhost:8000 in the url


Running AngularJS on Ubuntu:
-----------------------------
supde apt-get install nodejs (if you don't already have it)
sudo apt install npm (if you don't already have it)

In the directory of the Web page you want to run:
1) npm install (if not installed in the directory yet)
2) npm start
